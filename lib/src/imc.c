/*******************************************************************************
 * \file    imc.c
 *
 * \brief   IMC - Inter Module Communication
 *
 * \author  Mariusz Midor
 *          https://bitbucket.org/marmidr/imc
 *
 * \date
 *
 *      2024-09-28
 *          - new project structure
 *          - CMake configuration
 *
 *      2024-09-25
 *          - trace macros not used directly in headers
 *
 *      2018-02-14
 *          - fixed bug causing broken ACK frame transmitted
 *          - refactoring
 *
 *      2016-07-28
 *          - CRC calculation bug fixed
 *          - received DATA when expecting ACK: case ignored
 *
 *      2016-05-27
 *          - redesigned support of pending control frame
 *
 *      2016-05-16
 *          - code reorganized, minor bug fixes
 *
 *      2016-05-08
 *          - documentation updated, cosmetic changes
 *
 *      2013-05-20
 *          - fixed error in IMC_ProcessEvent (related to last change)
 *
 *      2013-05-08
 *          - variable IMC_TestRetrans
 *
 *      2013-04-15
 *          - when receiving data, recognizing duplicates if the frame
 *              is already on the receive queue
 *
 *      2013-01-27
 *          - multimaster flag moved to Context
 *          - frame sync byte changed from '—' to '•'
 *
 *      2013-01-19
 *          - elliminated usage of malloc/free
 *
 *      2012-12-11
 *          - multiinstance
 *
 *      2012-08-20
 *          - datasize extended to 4095 B
 *          - header structure changed
 *
 *      2012-07-26
 *          - 4-bit frame tag added, incremented after each transmision
 *          - 4-bit frame chcecksum added for quick recognizing of invalid header
 *
 *      2010-09-18
 *          - frame structure changed - added DataType field
 *          - simple checksum is now 16-bit
 *          - CRC16 functions moved to external file
 *
 *      2010-03-08
 *          - macros for byte representation ($xx and #nnn)
 *
 *      2010-02-17
 *          - Read(), Write(), StateMachine() may be interrupted by ProcessEvent()
 *              or StateMachine())
 *
 *      2010-02-05
 *          - retransmission on ACK timeout
 *          - frame buffers may be static memory or malloc() memory
 *
 *      2010-02-01
 *          - receive timeout
 *          - token give back routine
 *          - separated buffer for control frames
 *
 *      2010-01-30
 *          - improved receive routine speed
 *
 *      2010-01-20
 *          - frame format changed
 *          - CRC16 support
 *          - MultMaster support
 *          - MultiCast support
 *          - data from 0 to 1023 bytes, depending on configuration
 *
 *      2009-12-09
 *
 ******************************************************************************/

/*******************************************************************************
* Copyright (C) 2009-2018 Mariusz Midor: mmidor(at)gmail_com.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, and to permit persons to whom the Software is furnished
* to do so, subject to the following conditions:
*
* >
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* >
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
* >
* If some bug has been found or modifications made - please, let me know.
*
*******************************************************************************/



////////////////////////////////////////////////////////////////////////////////
/// includes

#include "imc.h"
#include <string.h>

////////////////////////////////////////////////////////////////////////////////
/// preprocessor definitions

// frame type
enum IMC_FrameType
{
    IMC_FT_DUMMY =          (0 << 4), // dummy, reserved
    IMC_FT_ECHO  =          (1 << 4), // send ACK and return
    IMC_FT_ACK =            (2 << 4), // data/echo ACK
    IMC_FT_DATA =           (3 << 4), // data frame
    IMC_FT_TOKEN =          (4 << 4), // pass token to recipent
    IMC_FT_ACK_ECHO_SLAVE = (5 << 4), // echo ACK - device is slave only
    IMC_FT_R1 =             (6 << 4), // rsvd
    IMC_FT_R2 =             (7 << 4)  // rsvd
};

// frames to acknowledge: IMC_FT_ECHO, IMC_FT_DATA, IMC_FT_TOKEN

/// frame type mask in Flags field
#define IMC_FLAGS_FT_MASK               (0x07 << 4)

/// return frame type
#define IMC_FRAMETYPE(p_frame)          ((p_frame)->Flags & IMC_FLAGS_FT_MASK)

/// Flags masks
#define IMC_FLAGS_LEN_MASK              0x0F
/// multicast flag
#define IMC_FLAGS_MULTICAST_MASK        0x80

/// return data size in frame
#define IMC_DATASIZE(p_frame)           ((p_frame)->DataSize + (((p_frame)->Flags & IMC_FLAGS_LEN_MASK) * 256))
/// check if addres is a multicast address
#define IMC_IS_MULTICAST(p_frame)       ((p_frame)->Flags & IMC_FLAGS_MULTICAST_MASK)

/// 4-bit frame tag, incremented on each transmission
#define IMC_FRAMETAG_MASK               0x0F
#define IMC_FRAMETAG(p_frame)           ((p_frame)->Flags2 & IMC_FRAMETAG_MASK)

/// 4-bit header checksum
#define IMC_HDRCHSUM(p_frame)           ((p_frame)->Flags2 & 0xF0)
#define IMC_CALCHDRCHSUM(p_frame)       ((((p_frame)->Dst + (p_frame)->Src + (p_frame)->Flags) & 0x0F) << 4)
#define IMC_HDRCHSUM_NBYTES             6 // Sync..Flags2

/// frame header byte value
#define IMC_SYNCBYTE                    (uint8_t)149 // '•'

/// constants

#define IMC_FRAME_HDR_SIZE              10  // Sync..DataType

// max token time
// should be big enough to suport big data frames
// 1->empty frame; 101->frames with 1000 bytes of data
// because max data is 4095 bytes -> 409, and tokentime may be 15, so 409+15+some reserve...
#define IMC_MAX_TOKEN_TIME              430

// define checksum size
#define IMC_CHSUM_SIZE                  2

// Context header
#define IMC_CTX_HDR                     0xAACCDDCC

// multimaster configuration (controller required)
#define IMC_CONFIG_USE_MULTIMASTER      false

#if IMC_CONFIG_USE_EVENTS
# define IMC_EVENT(evnt, ft, src, dst, pdata, datasize, ftag, dt)    IMC_Event(evnt, ft, src, dst, pdata, datasize, ftag, dt)
#else
# define IMC_EVENT(evnt, ft, src, dst, pdata, datasize, ftag, dt)
#endif

#ifndef MIN
# define MIN(x, y)                      (((x) < (y)) ? (x) : (y))
#endif

#ifndef STATIC_ASSERT
# define STATIC_ASSERT(expr, errmsg)    typedef char __statassert[(expr) ? 1 : -1]
#endif

////////////////////////////////////////////////////////////////////////////////
///                    Configuration check                                    //
////////////////////////////////////////////////////////////////////////////////

// define only on destination platform when file is delivered in form of static library
#ifndef IMC_OMIT_CHECKS

#if (IMC_CONFIG_RX_BUFF_COUNT < 1) || (IMC_CONFIG_RX_BUFF_COUNT > 255)
# error IMC_CONFIG_RXFRAMES_BUFF_COUNT must be in range 1.. 255
#endif

#if IMC_CONFIG_DATA_SIZE > IMC_DATASIZE_MAX
# error IMC_CONFIG_DATA_SIZE must be <= IMC_DATASIZE_MAX
#endif

/// ack timeout in IMC frames: small values for device-to-device cable connection,
/// must be 4+, bigger values for radio transmission
#if (IMC_CONFIG_ACK_TIMEOUT < 4) || (IMC_CONFIG_ACK_TIMEOUT > 255)
# error IMC_CONFIG_WAIT4ACK_TIMEOUT must be in range 4..255
#endif

// if the time for entire ACK frame is IMC_CONFIG_ACK_TIMEOUT, single byte timeout should be much smaller, right?
#if (IMC_CONFIG_BYTE_TIMEOUT < 1) || (IMC_CONFIG_BYTE_TIMEOUT > (IMC_CONFIG_ACK_TIMEOUT/2))
# error IMC_CONFIG_BYTE_TIMEOUT must be in range 1 .. IMC_CONFIG_ACK_TIMEOUT/2
#endif

#if IMC_CONFIG_USE_MULTIMASTER
# if !IMC_CONFIG_USE_CRC
#  error Multi-master mode works only with CRC16 checksum
# endif

# if !IMC_CONFIG_TOKENTIME
#  error IMC_CONFIG_TOKENTIME must be in range 1..15
# endif

# if !INRANGE(IMC_CONFIG_TOKENTIME, 1, 15)
#  error IMC_CONFIG_TOKENTIME must be in range 1..15
# endif
#endif

#endif // IMC_OMIT_CHECKS

///

////////////////////////////////////////////////////////////////////////////////
/// types


/**
 * Structure of IMC frame:
 * HHRSFFrrLTDC
 *
 * H - Header, Sync. bytes
 * R - Recipent address
 * S - Sender address
 * F - Flags
 * F - Flags
 * r
 * r
 * L - Data length
 * T - data type
 * D - Data(0..4095 Bytes)
 * C - Checksum
 *
 */

#if !defined __GNUC__ && !defined __BORLANDC__
# error Unknown compiler - set proper structure packing options for TIMC_Frame
#endif

#ifdef __BORLANDC__
# pragma pack(push, 1)
#endif

// actual structure of IMC frame
#define IMC_FRAME       \
    uint8_t Sync[2];    \
    uint8_t Dst;        \
    uint8_t Src;        \
    uint8_t Flags;      \
    uint8_t Flags2;     \
    uint8_t rsvd1;      \
    uint8_t rsvd2;      \
    uint8_t DataSize;   \
    uint8_t DataType;   \
    uint8_t Data[];


/// data frame buffer
typedef union
{
    struct ATTR_PACKED
    {
        IMC_FRAME
    };
    uint8_t Buffer[IMC_FRAME_HDR_SIZE + IMC_CONFIG_DATA_SIZE + IMC_CHSUM_SIZE];
} T_IMC_DataFrame;

/// control frame buffer - no data
typedef union
{
    struct ATTR_PACKED
    {
        IMC_FRAME
    };
    uint8_t Buffer[IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE];
} T_IMC_CtrlFrame;

#ifdef __BORLANDC__
# pragma pack(pop)
#endif

/// internal ringbuffer for incoming frames
typedef struct
{
    uint8_t         Tail;   // read index
    uint8_t         Head;   // write index
    uint8_t         Rsvd[2];// align
    T_IMC_DataFrame Frames[IMC_CONFIG_RX_BUFF_COUNT];
} T_IMC_RxBuff;

/// statemachine control block
typedef struct
{
    volatile uint8_t  Wait4Ack_Ctr;     // waiting for ACK after last Tx frame - downcounter
    volatile uint8_t  Wait4Ack_Tag;     // if DATA frame arrives, this field allow accept it as an ACK
    volatile uint8_t  Wait4Ack_Adr;     // if DATA frame arrives, this field allow accept it as an ACK
    volatile uint8_t  HasToken_Ctr;     // indicate that module has a token - count downto 0
    volatile uint8_t  Receiving_Ctr;    // receiving is in progress
    volatile uint8_t  TxRetrans_Ctr;    // downcounter: try to deliver data
    volatile bool     RxReady;          // there is received frame in the buffer
    volatile bool     SendingData;      // data transmission is in progress
    volatile bool     SendingData_BcMc; // boardcast or multicast data sending
    volatile bool     SendingControl;   // sending control frame
    volatile bool     SendingToken;     //
    volatile bool     Wait4Token;       // tx buffer waiting for token before sent
    volatile bool     TxAcknowledged;   // last Tx frame has been confirmed (ACK received)
    volatile bool     DropRxFrame;      // frame data size too big - drop when receiving finished
    volatile bool     ProcessingOnRx;   //
} T_IMC_CtrlBlock;

typedef struct
{
    /** struct header */
    uint32_t            Hdr;
    /** number of received bytes */
    uint16_t            Received;
    /** statistics */
    T_IMC_Stats         Stats;
    /** state machine control block */
    T_IMC_CtrlBlock     CB;
    /** buffer for control frame */
    T_IMC_CtrlFrame     CtrlFrame;
    /** last control frame was'n sent because another transmission was in progress */
    bool                PostponedCtrlFrame;
    /** using multimaster */
    bool                MultiMaster;
    /** tag for tx frames */
    uint8_t             TxFrameTag;
    /** our address */
    uint8_t             OurAddr;
    /** 0-terminated list of accepted multicast addresses */
    const uint8_t      *pMulticastAddrList;
    /** ring buffer for configured number of receive frames */
    T_IMC_RxBuff        RxFrames;
    /** pointer to actual RxFrame */
    T_IMC_DataFrame    *pRxFrame;
#if !IMC_CONFIG_RECEIVE_ONLY
    /** transmit frame buffer */
    T_IMC_DataFrame     TxFrame;
    /** size of data added to TxFrame */
    uint16_t            TxDataSize;
#endif
    /** struct footer */
    uint32_t            Ftr;
} T_IMC_Context;

////////////////////////////////////////////////////////////////////////////////
/// constants, variables

/** send 3 of 4 acks */
#if IMC_CONFIG_TEST_RETRANS
bool IMC_TestRetrans;
#endif

/** current context */
static T_IMC_Context   *IMC_pCtx;

////////////////////////////////////////////////////////////////////////////////
/// implementation

// assertions
STATIC_ASSERT(sizeof(T_IMC_Ctx) >= sizeof(T_IMC_Context), "Size of T_IMC_Ctx is too small");
STATIC_ASSERT(sizeof(T_IMC_DataFrame) == (IMC_FRAME_HDR_SIZE + IMC_CONFIG_DATA_SIZE + IMC_CHSUM_SIZE), "Invalid size of T_IMC_DataFrame");
STATIC_ASSERT(sizeof(T_IMC_CtrlFrame) == (IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE), "Invalid size of T_IMC_CtrlFrame");

//------------------------------------------------------------------------------

void IMC_Init(T_IMC_Ctx *pContext)
{
    IMC_ASSERT(pContext != NULL);

    // initialize structure
    T_IMC_Context *p_ctx = (T_IMC_Context*)pContext;
    size_t sz = sizeof(T_IMC_Context);
    memset(p_ctx, 0, sz);
    p_ctx->Hdr = IMC_CTX_HDR;
    p_ctx->Ftr = IMC_CTX_HDR;
}

//------------------------------------------------------------------------------

void IMC_Free(T_IMC_Ctx *pContext)
{
    IMC_ASSERT(pContext != NULL);

    T_IMC_Context* p_ctx = (T_IMC_Context*)pContext;
    IMC_ASSERT(p_ctx->Hdr == IMC_CTX_HDR && p_ctx->Ftr == IMC_CTX_HDR);

    p_ctx->Hdr = p_ctx->Ftr = 0;
    IMC_pCtx = NULL;
}

//------------------------------------------------------------------------------

T_IMC_Ctx * IMC_Select(T_IMC_Ctx *pContext)
{
    T_IMC_Ctx     *p_old = (T_IMC_Ctx*)IMC_pCtx;
    T_IMC_Context *p_new = (T_IMC_Context*)pContext;

    if (!p_new)
        return p_old;

    if (p_new->Hdr != IMC_CTX_HDR || p_new->Ftr != IMC_CTX_HDR)
    {
        IMC_Trace("-E- IMC_Select() - invalid instance pointer");
        return p_old;
    }

    IMC_pCtx = p_new;
    return p_old;
}

//------------------------------------------------------------------------------

const T_IMC_Stats * IMC_GetStats(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    return &IMC_pCtx->Stats;
}

//------------------------------------------------------------------------------

void IMC_ResetStats(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    memset(&IMC_pCtx->Stats, 0, sizeof(T_IMC_Stats));
}

//------------------------------------------------------------------------------

bool IMC_SetAddress(uint8_t DeviceAddr)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    if ((DeviceAddr == IMC_ADDR_NETCONTROLLER) || (DeviceAddr == IMC_ADDR_BROADCAST))
    {
        IMC_Trace("-E- IMC_SetAddress() Device address of %d not allowed", DeviceAddr);
        return false;
    }

    IMC_pCtx->OurAddr = DeviceAddr;
    return true;
}

//------------------------------------------------------------------------------

bool IMC_SetMulticastAddrList(const uint8_t *pMulticastAddrList)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    uint16_t n = 0;
    if (pMulticastAddrList)
        for (n = 0; (pMulticastAddrList[n] > 0) && (n < 256); n++)
            ;
    if (n > 254)
        return false;
    IMC_pCtx->pMulticastAddrList = pMulticastAddrList;
    return true;
}

//------------------------------------------------------------------------------

void IMC_GetAddress(uint8_t *pDeviceAddr, const uint8_t **ppMulticastAddrList)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    if (pDeviceAddr)
        *pDeviceAddr = IMC_pCtx->OurAddr;

    if (ppMulticastAddrList)
        *ppMulticastAddrList = IMC_pCtx->pMulticastAddrList;
}

//------------------------------------------------------------------------------

#if IMC_CONFIG_USE_MULTIMASTER
void IMC_SetMultimaster(bool8 Enabled)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    IMC_pCtx->MultiMaster = Enabled;
}
#endif

//------------------------------------------------------------------------------

#if IMC_CONFIG_USE_MULTIMASTER
bool8 IMC_GetMultimaster(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    return IMC_pCtx->MultiMaster;
}
#endif

//------------------------------------------------------------------------------

#if !IMC_CONFIG_RECEIVE_ONLY
T_IMC_WriteRes IMC_Write(uint8_t Addr, bool Multicast, T_IMC_DataType DataType,
    const void *pData, size_t DataSize, int8_t *pTag)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    T_IMC_DataFrame *p_txframe = &IMC_pCtx->TxFrame;
    int8_t _tag = -1;

    if (!pTag)
        pTag = &_tag;

#if IMC_CONFIG_USE_MULTIMASTER
    if (IMC_pCtx->MultiMaster && IMC_pCtx->CB.Wait4Token)
        return IMC_WRITE_BUSY;

    // Receiving status is ignored here - device may receiving Token at the moment
    if (IMC_pCtx->CB.SendingData || (IMC_pCtx->CB.Wait4Ack_Ctr > 1))
        return IMC_WRITE_BUSY;
#else
    if (IMC_pCtx->CB.Receiving_Ctr  ||
        IMC_pCtx->CB.SendingData    ||
        IMC_pCtx->CB.SendingControl ||
        IMC_pCtx->CB.SendingToken   ||
        IMC_pCtx->CB.ProcessingOnRx ||
       (IMC_pCtx->CB.Wait4Ack_Ctr > 1))
    {
        return IMC_WRITE_BUSY;
    }
#endif

    if (Multicast && (Addr == IMC_ADDR_BROADCAST))
        return IMC_WRITE_ADDR_ERROR;


    if (pData == NULL)
    {
        // data added to buffer by IMC_AddTxData() or IMC_AddTxString()
        DataSize = IMC_pCtx->TxDataSize;
    }
    else
    {
        if (DataSize == 0) // pData is a string
            DataSize = strlen((char*)pData) + 1;

        if (DataSize > IMC_CONFIG_DATA_SIZE)
            return IMC_WRITE_DATATOOLONG;

        memcpy(IMC_pCtx->TxFrame.Data, (uint8_t*)pData, DataSize);
    }

    p_txframe->Sync[0] = p_txframe->Sync[1] = IMC_SYNCBYTE;
    p_txframe->Dst = Addr;
    p_txframe->Src = IMC_pCtx->OurAddr;
    p_txframe->DataType = DataType;
    p_txframe->Flags = IMC_FT_DATA;
    p_txframe->Flags |= (DataSize >> 8) & IMC_FLAGS_LEN_MASK;
    p_txframe->Flags |= Multicast ? IMC_FLAGS_MULTICAST_MASK : 0;
    p_txframe->DataSize = DataSize;

    if (*pTag < 0)
    {
        // automatic internal tag
        p_txframe->Flags2 = *pTag = IMC_pCtx->TxFrameTag;
        IMC_pCtx->TxFrameTag++;
        IMC_pCtx->TxFrameTag &= IMC_FRAMETAG_MASK;
    }
    else
    {
        // use given Tag value
        p_txframe->Flags2 = *pTag & IMC_FRAMETAG_MASK;
    }

    p_txframe->Flags2 |= IMC_CALCHDRCHSUM(&IMC_pCtx->TxFrame);
    p_txframe->rsvd1 = p_txframe->rsvd2 = 0;

#if IMC_CONFIG_USE_CRC
    uint16_t crc16 = 0;
    IMC_CalcCRC16(&crc16, p_txframe->Buffer, IMC_DATASIZE(&IMC_pCtx->TxFrame) + IMC_FRAME_HDR_SIZE);
    p_txframe->Data[IMC_DATASIZE(&IMC_pCtx->TxFrame)+0] = crc16;
    p_txframe->Data[IMC_DATASIZE(&IMC_pCtx->TxFrame)+1] = crc16 >> 8;
#else
    int16_t chksum = 0;
    for (int16_t i = 0; i < IMC_DATASIZE(&IMC_pCtx->TxFrame) + IMC_FRAME_HDR_SIZE; i++)
        chksum += p_txframe->Buffer[i];
    chksum = -chksum;
    p_txframe->Data[IMC_DATASIZE(&IMC_pCtx->TxFrame)+0] = chksum;
    p_txframe->Data[IMC_DATASIZE(&IMC_pCtx->TxFrame)+1] = chksum >> 8;
#endif

    IMC_pCtx->CB.Wait4Ack_Tag = *pTag;
    IMC_pCtx->CB.Wait4Ack_Adr = Addr;
    IMC_pCtx->CB.TxRetrans_Ctr = IMC_CONFIG_TX_RETRANS;
    IMC_pCtx->CB.SendingData_BcMc = (Addr == IMC_ADDR_BROADCAST) || Multicast;

#if IMC_CONFIG_USE_MULTIMASTER
    if ((IMC_pCtx->MultiMaster && !IMC_pCtx->CB.HasToken_Ctr) ||
        IMC_pCtx->CB.SendingControl)
    {
        IMC_pCtx->CB.Wait4Token = true;
        return IMC_WRITE_WAIT4TOKEN;
    }
#endif

    IMC_pCtx->CB.SendingData = true;
    IMC_pCtx->CB.SendingControl = false;
    IMC_pCtx->CB.TxAcknowledged = false;

    IMC_WriteBuffer(p_txframe->Buffer,
        IMC_DATASIZE(&IMC_pCtx->TxFrame) + IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE);

    IMC_pCtx->Stats.TxFrames++;

    IMC_EVENT(IMC_EVNT_TX_FRAME, IMC_EVNT_FT_DATA,
        p_txframe->Src, p_txframe->Dst,
        p_txframe->Data, IMC_DATASIZE(&IMC_pCtx->TxFrame),
        IMC_FRAMETAG(&IMC_pCtx->TxFrame), (T_IMC_DataType)p_txframe->DataType);

    return IMC_WRITE_OK;
}
#endif

//------------------------------------------------------------------------------

#if !IMC_CONFIG_RECEIVE_ONLY
T_IMC_WriteRes IMC_AddTxData(const void *pData, size_t DataSize)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    if (DataSize > (size_t)(IMC_CONFIG_DATA_SIZE - IMC_pCtx->TxDataSize))
        return IMC_WRITE_DATATOOLONG;

    memcpy(&IMC_pCtx->TxFrame.Data[IMC_pCtx->TxDataSize], (uint8_t*)pData, DataSize);
    IMC_pCtx->TxDataSize += DataSize;
    return IMC_WRITE_OK;
}
#endif

//------------------------------------------------------------------------------

#if !IMC_CONFIG_RECEIVE_ONLY
T_IMC_WriteRes IMC_AddTxString(const char *Text)
{
    size_t sl = strlen(Text)+1;
    // jeśli już jest tekst w buforze, cofamy wskaźnik na ostatni '\0'
    if (IMC_pCtx->TxDataSize)
        IMC_pCtx->TxDataSize--;
    return IMC_AddTxData(Text, sl);
}
#endif

//------------------------------------------------------------------------------

#if !IMC_CONFIG_RECEIVE_ONLY
uint16_t IMC_GetTxSize(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    return IMC_pCtx->TxDataSize;
}
#endif

//------------------------------------------------------------------------------

#if !IMC_CONFIG_RECEIVE_ONLY
void IMC_SetTxSize(uint16_t TxSize)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    IMC_pCtx->TxDataSize = MIN(TxSize, IMC_CONFIG_DATA_SIZE);
}
#endif

//------------------------------------------------------------------------------

#if !IMC_CONFIG_RECEIVE_ONLY
const char *IMC_GetWriteRes_Str(T_IMC_WriteRes Res)
{
    const char *wr_res[] =
    {
        "OK",
        "WAIT4TOKEN",
        "DATATOOLONG",
        "BUSY",
        "ADDR_ERROR",
        "DATA_ERROR",
        "?"
    };

    if (Res <= IMC_WRITE_DATA_ERROR)
        return wr_res[Res];
    else
        return "?";
};
#endif

//------------------------------------------------------------------------------

#if !IMC_CONFIG_RECEIVE_ONLY
void IMC_WriteAbort(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    IMC_pCtx->CB.Wait4Token = false;
    IMC_pCtx->CB.SendingData = false;
    IMC_pCtx->CB.SendingControl = false;
    IMC_pCtx->CB.SendingToken = false;
    IMC_pCtx->Stats.TxNack += IMC_pCtx->CB.Wait4Ack_Ctr > 2;
    IMC_pCtx->CB.Wait4Ack_Ctr = 0;
}
#endif

//------------------------------------------------------------------------------

T_IMC_State IMC_GetState(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    if (IMC_pCtx->CB.SendingData || IMC_pCtx->CB.SendingControl)
        return IMC_STAT_SENDING;

    if (IMC_pCtx->CB.Receiving_Ctr || IMC_pCtx->CB.ProcessingOnRx)
        return IMC_STAT_RECEIVING;

    if (IMC_pCtx->CB.Wait4Token)
        return IMC_STAT_WAIT4TOKEN;

    if (IMC_pCtx->CB.Wait4Ack_Ctr > 2)
        return IMC_STAT_WAIT4ACK;

    if (IMC_pCtx->CB.Wait4Ack_Ctr == 1)
        return IMC_STAT_SENT_NACK;

    if (IMC_pCtx->CB.TxAcknowledged)
        return IMC_STAT_SENT_ACK;

    return IMC_STAT_IDLE;
}

//------------------------------------------------------------------------------

const char *IMC_GetState_Str(T_IMC_State Stat)
{
    const char *stat[] =
    {
        "SENT_ACK",
        "SENT_NACK",
        "IDLE",
        "SENDING",
        "RECEIVING",
        "WAIT4ACK",
        "WAIT4TOKEN",
        "?"
    };

    if (Stat <= IMC_STAT_WAIT4TOKEN)
        return stat[Stat];
    else
        return "?";
};

//------------------------------------------------------------------------------

bool IMC_GetRxReady(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    return IMC_pCtx->CB.RxReady;
}

//------------------------------------------------------------------------------

void * IMC_Read(uint8_t *pSrcAddr, T_IMC_DataType *pDataType, uint16_t *pDataSize, int8_t *pTag)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    if (IMC_pCtx->CB.RxReady)
    {
        T_IMC_DataFrame *p_frame = &IMC_pCtx->RxFrames.Frames[IMC_pCtx->RxFrames.Tail];

        if (pSrcAddr)
            *pSrcAddr = p_frame->Src;

        if (pDataType)
            *pDataType = (T_IMC_DataType)p_frame->DataType;

        if (pDataSize)
            *pDataSize = IMC_DATASIZE(p_frame);

        if (pTag)
            *pTag = IMC_FRAMETAG(p_frame);

        return p_frame->Data;
    }

    return NULL;
}

//------------------------------------------------------------------------------

bool IMC_Read2(T_IMC_Rd *pRd)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    IMC_ASSERT(pRd != NULL);
    pRd->pData = IMC_Read(&pRd->Addr, &pRd->DataType, &pRd->DataSize, &pRd->FrameTag);
    return pRd->pData != NULL;
}

//------------------------------------------------------------------------------

void IMC_ReadEnd(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);
    T_IMC_RxBuff *p_rxframes = &IMC_pCtx->RxFrames;
    T_IMC_DataFrame *p_rxframe = &p_rxframes->Frames[p_rxframes->Tail];
    p_rxframe->Flags = p_rxframe->Flags2 = 0;

    if (++p_rxframes->Tail >= IMC_CONFIG_RX_BUFF_COUNT)
        p_rxframes->Tail = 0;

    if (p_rxframes->Tail == p_rxframes->Head)
        IMC_pCtx->CB.RxReady = false;
}

//------------------------------------------------------------------------------

/**
 * @brief Sends control frame (ACK, Token)
 * @param Addr      Destination address
 * @param FrameType Type of control frame
 * @param FrameTag  Frame tag 0x0..0xF
 */
static void IMC_SendControl(uint8_t Addr, uint8_t FrameType, uint8_t FrameTag)
{
    T_IMC_CtrlFrame *p_ctrlframe = &IMC_pCtx->CtrlFrame;

    if (IMC_pCtx->CB.SendingControl || IMC_pCtx->CB.SendingData || IMC_pCtx->CB.SendingToken)
    {
        // some transmission is still in progress -> remember control frame to send it later
        if (IMC_pCtx->PostponedCtrlFrame)
        {
            IMC_EVENT(IMC_EVNT_MSG, IMC_EVNT_FT_DUMMY, 0, 0,
                (uint8_t*)"Pending control frame conflict", 0, 0, IMC_DT_NONE);
        }
        else
        {
            IMC_EVENT(IMC_EVNT_MSG, IMC_EVNT_FT_DUMMY, 0, 0,
                (uint8_t*)"Add pending control frame", 0, 0, IMC_DT_NONE);

            // this will be processed in IMC_ProcessStateMachine
            IMC_pCtx->PostponedCtrlFrame = true;
            p_ctrlframe->Dst     = Addr;
            p_ctrlframe->Flags   = FrameType;
            p_ctrlframe->Flags2  = FrameTag;
        }
        return;
    }

    IMC_pCtx->CB.SendingControl = true;
    IMC_pCtx->CB.TxRetrans_Ctr = 0;

    p_ctrlframe->Sync[0] = p_ctrlframe->Sync[1] = IMC_SYNCBYTE;
    p_ctrlframe->Dst     = Addr;
    p_ctrlframe->Src     = IMC_pCtx->OurAddr;
    p_ctrlframe->Flags   = FrameType;
    p_ctrlframe->Flags2  = FrameTag & IMC_FRAMETAG_MASK;
    p_ctrlframe->Flags2 |= IMC_CALCHDRCHSUM(p_ctrlframe);
    p_ctrlframe->DataSize = 0;
    p_ctrlframe->DataType = IMC_DT_NONE;
    p_ctrlframe->rsvd1 = p_ctrlframe->rsvd2 = 0;

#if IMC_CONFIG_USE_CRC
    uint16_t crc16 = 0;
    IMC_CalcCRC16(&crc16, p_ctrlframe->Buffer, IMC_FRAME_HDR_SIZE);
    p_ctrlframe->Data[0] = crc16;
    p_ctrlframe->Data[1] = crc16 >> 8;
#else
    int16_t chksum = 0;
    for (int16_t i = 0; i < IMC_FRAME_HDR_SIZE; i++)
        chksum += p_ctrlframe->Buffer[i];
    chksum = -chksum;
    p_ctrlframe->Data[0] = chksum;
    p_ctrlframe->Data[1] = chksum >> 8;
#endif

    IMC_WriteBuffer(p_ctrlframe->Buffer, IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE);

    IMC_EVENT(IMC_EVNT_TX_FRAME, (T_IMC_EvFrameType)FrameType, p_ctrlframe->Src, p_ctrlframe->Dst,
        NULL, 0, FrameTag, (T_IMC_DataType)p_ctrlframe->DataType);
}

//------------------------------------------------------------------------------

static bool IMC_CheckMulticastAddr(uint8_t McAddr)
{
    const uint8_t *p_mc_addr = IMC_pCtx->pMulticastAddrList;
    if (p_mc_addr == NULL)
        return false;

    for (uint16_t i = 0; i < 255; i++)
    {
        if (p_mc_addr[i] == 0) // terminator
            return false;
        if (p_mc_addr[i] == McAddr)
            return true;
    }

    return false;
}

//------------------------------------------------------------------------------

static __inline void IMC_CheckIfHeaderCorrectOrResync(void)
{
    uint8_t hdr_chsum_actual   = IMC_HDRCHSUM(IMC_pCtx->pRxFrame);
    uint8_t hdr_chsum_expected = IMC_CALCHDRCHSUM(IMC_pCtx->pRxFrame);

    if (hdr_chsum_actual != hdr_chsum_expected)
    {
        // header checksum error - searching for another frame header bytes within rx buffer

        IMC_EVENT(IMC_EVNT_MSG, IMC_EVNT_FT_DUMMY, 0, 0,
            (uint8_t*)"Header checksum err", 0, 0, IMC_DT_NONE);

        bool sync_found = false;
        uint8_t *p_buff = IMC_pCtx->pRxFrame->Buffer;

        for (uint8_t i = 1; i < IMC_HDRCHSUM_NBYTES-1; i++)
        {
            if ((p_buff[i+0] == IMC_SYNCBYTE) &&
                (p_buff[i+1] == IMC_SYNCBYTE))
            {
                IMC_EVENT(IMC_EVNT_MSG, IMC_EVNT_FT_DUMMY, 0, 0,
                    (uint8_t*)"New header found", 0, 0, IMC_DT_NONE);

                // found - shift bytes to the beginning of the buffer
                for (uint8_t j = 0; j < IMC_HDRCHSUM_NBYTES; j++)
                    p_buff[j] = p_buff[i+j];

                IMC_pCtx->Received = IMC_HDRCHSUM_NBYTES - i;
                sync_found = true;
                break;
            }
        }

        if (!sync_found)
        {
            IMC_pCtx->Received = 0;
            IMC_pCtx->CB.Receiving_Ctr = 0;
        }
    }

/*
    #if (IMC_CONFIG_RX_BUFF_COUNT > 1)
    // check header if such a frame already exists on the rx frames buff
    if (IMC_pCtx->RxFrames.Tail != IMC_pCtx->RxFrames.Head)
    {
        T_IMC_Frame *p_last_frame = &IMC_pCtx->RxFrames.Frames[IMC_pCtx->RxFrames.Tail];
        if (p_last_frame->Flags == IMC_pCtx->pRxFrame->Flags &&
            p_last_frame->Flags2 == IMC_pCtx->pRxFrame->Flags2 &&
            p_last_frame->Src == IMC_pCtx->pRxFrame->Src)
        {
            IMC_pCtx->Status.SkipRxFrame = true;
            IMC_EVENT(IMC_EVNT_MSG, 0, 0, 0,
                (uint8*)"Duplicated frame", 0, 0, IMC_DT_NONE);
        }
    }
    #endif
*/
}

//------------------------------------------------------------------------------

static __inline void IMC_AckReceived(void)
{
    IMC_pCtx->CB.TxAcknowledged = true;
    IMC_pCtx->CB.Wait4Ack_Ctr = 0;

    if (!IMC_pCtx->CB.SendingToken)
        IMC_pCtx->Stats.TxAck++;
    IMC_pCtx->CB.SendingToken = false;
}

//------------------------------------------------------------------------------

bool IMC_OnRxData(uint8_t Data)
{
    #define RETURN()    { IMC_pCtx->CB.ProcessingOnRx = false; return true; }

    IMC_ASSERT(IMC_pCtx != NULL);
    IMC_pCtx->CB.ProcessingOnRx = true;

    // start of frame sync.
    if (IMC_pCtx->Received < 2)
    {
        if (Data != IMC_SYNCBYTE)
        {
            IMC_pCtx->Received = 0;
            RETURN();
        }

        // two sync bytes received - initialize rx frame header
        if (++IMC_pCtx->Received == 2)
        {
            // buffers are full
            if (IMC_pCtx->CB.RxReady && (IMC_pCtx->RxFrames.Tail == IMC_pCtx->RxFrames.Head))
            {
                IMC_pCtx->Stats.RxOverruns++;
                IMC_pCtx->Received = 0;
                IMC_pCtx->CB.ProcessingOnRx = false;
                return false;
            }

            IMC_pCtx->pRxFrame = &IMC_pCtx->RxFrames.Frames[IMC_pCtx->RxFrames.Head];
            IMC_pCtx->pRxFrame->Buffer[0] = IMC_SYNCBYTE;
            IMC_pCtx->pRxFrame->Buffer[1] = IMC_SYNCBYTE;
            IMC_pCtx->pRxFrame->DataSize = 0;
            IMC_pCtx->pRxFrame->Flags = 0;
            IMC_pCtx->CB.DropRxFrame = false;
        }

        RETURN();
    }

    T_IMC_DataFrame *p_rxframe = IMC_pCtx->pRxFrame;
    // do not save Data if IMC_Received >= IMC_CONFIG_DATA_SIZE + IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE
    if (IMC_pCtx->Received < (IMC_CONFIG_DATA_SIZE + IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE))
        p_rxframe->Buffer[IMC_pCtx->Received] = Data;
    else
        IMC_pCtx->CB.DropRxFrame = true;

    IMC_pCtx->Received++;
    IMC_pCtx->CB.Receiving_Ctr = IMC_CONFIG_BYTE_TIMEOUT; // renew next byte timeout

    // header complete ? check if it is valid header and locate next sync bytes if not
    if (IMC_pCtx->Received == IMC_HDRCHSUM_NBYTES)
    {
        IMC_CheckIfHeaderCorrectOrResync();
    }
    // frame complete?
    else if (IMC_pCtx->Received == (IMC_DATASIZE(p_rxframe) + IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE))
    {
        uint16_t rx_chksum = 0;

        #if IMC_CONFIG_USE_CRC
            // calc entire frame CRC
            IMC_CalcCRC16(&rx_chksum, p_rxframe->Buffer, IMC_pCtx->Received);
        #else
            // sum up bytes
            for (uint16_t i = 0; i < IMC_pCtx->Received-1; i++)
                rx_chksum += p_rxframe->Buffer[i];

            // add hi-byte of 16bit checksum
            rx_chksum += (uint16_t)Data << 8;
        #endif

        IMC_pCtx->CB.Receiving_Ctr = 0;
        IMC_pCtx->Received = 0;

        if (IMC_pCtx->CB.DropRxFrame)
        {
            IMC_EVENT(IMC_EVNT_RX_DROPPED,
                (T_IMC_EvFrameType)IMC_FRAMETYPE(p_rxframe),
                p_rxframe->Src,
                p_rxframe->Dst,
                p_rxframe->Data,
                IMC_CONFIG_DATA_SIZE + IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE,
                IMC_FRAMETAG(p_rxframe),
                (T_IMC_DataType)p_rxframe->DataType);

            IMC_pCtx->CB.DropRxFrame = false;
            RETURN();
        }
        else
        {
            IMC_EVENT(rx_chksum ? IMC_EVNT_RX_CHSUM_ERROR : IMC_EVNT_RX_FRAME,
                (T_IMC_EvFrameType)IMC_FRAMETYPE(p_rxframe),
                p_rxframe->Src,
                p_rxframe->Dst,
                p_rxframe->Data,
                IMC_DATASIZE(p_rxframe),
                IMC_FRAMETAG(p_rxframe),
                (T_IMC_DataType)p_rxframe->DataType);
        }

        if (rx_chksum != 0)
        {
            IMC_pCtx->Stats.RxErrors++;
            RETURN();
        }

        // check if dest address is our self address or broadcast address or multicast addr
        uint8_t dstaddr = p_rxframe->Dst;

        if (IMC_IS_MULTICAST(p_rxframe))
        {
            if (!IMC_CheckMulticastAddr(dstaddr))
                RETURN();
        }
        else if (dstaddr != IMC_ADDR_BROADCAST)
        {
            if (dstaddr != IMC_pCtx->OurAddr)
                RETURN();
        }

        switch (IMC_FRAMETYPE(p_rxframe))
        {
            case IMC_FT_ACK:
                if (IMC_pCtx->CB.Wait4Ack_Ctr > 2) // TODO: is this condition needed?
                    IMC_AckReceived();
                break;

            case IMC_FT_DATA:
                /*
                if ((IMC_pCtx->CB.Wait4Ack_Ctr > 2) &&
                    (IMC_pCtx->CB.Wait4Ack_Adr == p_rxframe->Src) &&
                    (IMC_pCtx->CB.Wait4Ack_Tag == IMC_FRAMETAG(p_rxframe)))
                {
                    // so far we didn't received ACK frame, but this frame looks to be an answer to the frame
                    // we waiting ACK - let's assume if there is an answar, last tx frame has been received.
                    IMC_AckReceived();
                    IMC_EVENT(IMC_EVNT_MSG, IMC_EVNT_FT_DUMMY, 0, 0,
                        (uint8_t*)"Expected ACK but got DATA", 0, 0, IMC_DT_NONE);
                }
                */

                // if not a broadcast or multicast address, send frame ack
                if ((dstaddr != IMC_ADDR_BROADCAST) && !IMC_IS_MULTICAST(p_rxframe))
                {
                #if IMC_CONFIG_TEST_RETRANS
                    if (IMC_TestRetrans)
                    {
                        bool ack = true;
                        // send only 3 of 4 expected acks
                        static uint8_t ack_ctr = 0;
                        ack_ctr++;
                        ack = (ack_ctr & 0x03) > 0;

                        if (!ack)
                            RETURN();
                    }
                #endif

                    IMC_SendControl(p_rxframe->Src,
                        IMC_FT_ACK, IMC_FRAMETAG(p_rxframe));
                }
                break;

            #if IMC_CONFIG_USE_MULTIMASTER
            case IMC_FT_TOKEN:
                if (dstaddr != IMC_ADDR_BROADCAST)
                    IMC_pCtx->CB.HasToken_Ctr = IMC_CONFIG_TOKENTIME;
            #endif

            #if IMC_CONFIG_USE_MULTIMASTER
            case IMC_FT_ECHO:
                // if not a broadcast or multicast address, send frame ack
                if (dstaddr != IMC_ADDR_BROADCAST)
                {
                    #if IMC_CONFIG_RECEIVE_ONLY
                    IMC_SendControl(p_rxframe->Src,
                        IMC_FT_ACK_ECHO_SLAVE, IMC_FRAMETAG(p_rxframe));
                    #else
                    IMC_SendControl(p_rxframe->Src,
                        IMC_FT_ACK, IMC_FRAMETAG(p_rxframe));
                    #endif
                }
                break;
            #endif
        }

        if (IMC_FRAMETYPE(p_rxframe) == IMC_FT_DATA)
        {
            if (++IMC_pCtx->RxFrames.Head >= IMC_CONFIG_RX_BUFF_COUNT)
                IMC_pCtx->RxFrames.Head = 0;

            IMC_pCtx->Stats.RxFrames++;
            IMC_pCtx->CB.RxReady = true;
        }
    }

    RETURN();
}

//------------------------------------------------------------------------------

void IMC_OnRxError(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    IMC_pCtx->Stats.RxErrors++;
    IMC_EVENT(IMC_EVNT_RX_ERROR, IMC_EVNT_FT_DUMMY, 0, 0,
        IMC_pCtx->pRxFrame->Buffer, IMC_pCtx->Received, 0, IMC_DT_NONE);
    IMC_pCtx->Received = 0;
    IMC_pCtx->CB.Receiving_Ctr = 0;
}

//------------------------------------------------------------------------------

void IMC_OnDataSent(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    if (IMC_pCtx->CB.SendingControl)
    {
        IMC_pCtx->CB.SendingControl = false;
        IMC_pCtx->CB.Wait4Ack_Ctr = 0;
    }

    #if !IMC_CONFIG_RECEIVE_ONLY
    if (IMC_pCtx->CB.SendingData)
    {
        IMC_pCtx->CB.SendingData = false;

        if (IMC_pCtx->CB.SendingData_BcMc)
        {
            // multicast transmission? do not expect ACK
            IMC_pCtx->CB.SendingData_BcMc = false;
            IMC_pCtx->CB.Wait4Ack_Ctr = 0;
            // little cheat for the statistics looking good
            IMC_pCtx->Stats.TxAck++;
        }
        else
        {
            // set ACK timeout counter
            IMC_pCtx->CB.Wait4Ack_Ctr = IMC_CONFIG_ACK_TIMEOUT;
        }
    }

    IMC_pCtx->CB.SendingToken = false;
    #endif

    // control frame pending because of transmission? now we are free to send it
    if (IMC_pCtx->PostponedCtrlFrame)
    {
        IMC_EVENT(IMC_EVNT_MSG, IMC_EVNT_FT_DUMMY, 0, 0,
            (uint8_t*)"Send pending control frame", 0, 0, IMC_DT_NONE);

        IMC_pCtx->PostponedCtrlFrame = false;
        IMC_SendControl(IMC_pCtx->CtrlFrame.Dst, IMC_pCtx->CtrlFrame.Flags, IMC_pCtx->CtrlFrame.Flags2);
    }
}

//------------------------------------------------------------------------------

void IMC_ProcessStateMachine(void)
{
    IMC_ASSERT(IMC_pCtx != NULL);

    // IMC_OnRxData() may be called not only from an ISR
    if (IMC_pCtx->CB.ProcessingOnRx)
        return;

    if (IMC_pCtx->CB.Receiving_Ctr)
    {
        // check byte-timeout
        if (!--IMC_pCtx->CB.Receiving_Ctr)
        {
            IMC_EVENT(IMC_EVNT_RX_INCOMPLETE, IMC_EVNT_FT_DUMMY, 0, 0,
                IMC_pCtx->pRxFrame->Buffer, IMC_pCtx->Received, 0, IMC_DT_NONE);

            IMC_pCtx->Received = 0;
        }
    }

#if !IMC_CONFIG_RECEIVE_ONLY
    if (IMC_pCtx->CB.Wait4Ack_Ctr > 2)
    {
        // check ACK-timeout
        IMC_pCtx->CB.Wait4Ack_Ctr--;
        #if IMC_CONFIG_USE_MULTIMASTER
        if (IMC_pCtx->CB.Wait4Ack_Ctr == 2)
            IMC_pCtx->CB.SendingToken = false;
        #endif
    }

    #if IMC_CONFIG_TX_RETRANS > 0
    if (IMC_pCtx->CB.Wait4Ack_Ctr == 2)
    {
        // Wait4Ack == 2 --> ACK timeout
        if (IMC_pCtx->CB.TxRetrans_Ctr > 0)
        {
            // let's try again
            #if IMC_CONFIG_USE_MULTIMASTER
            if (IMC_pCtx->MultiMaster)
                IMC_pCtx->CB.Wait4Token = true;
            else
            #endif
            {
                IMC_pCtx->CB.SendingData = true;
                IMC_pCtx->CB.TxAcknowledged = false;
                IMC_pCtx->CB.Wait4Token = false;
                IMC_pCtx->CB.Wait4Ack_Ctr = IMC_CONFIG_ACK_TIMEOUT;
                // TxRetrans is incremented only once for every data frame to be transmitted
                IMC_pCtx->Stats.TxRetrans += IMC_pCtx->CB.TxRetrans_Ctr == IMC_CONFIG_TX_RETRANS;
                IMC_pCtx->CB.TxRetrans_Ctr--;

                // send frame
                IMC_WriteBuffer(IMC_pCtx->TxFrame.Buffer,
                    IMC_DATASIZE(&IMC_pCtx->TxFrame) + IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE);

                IMC_EVENT(IMC_EVNT_TX_RETRANSMISSION,
                    (T_IMC_EvFrameType)IMC_FRAMETYPE(&IMC_pCtx->TxFrame),
                    IMC_pCtx->TxFrame.Src, IMC_pCtx->TxFrame.Dst,
                    IMC_pCtx->TxFrame.Data, IMC_DATASIZE(&IMC_pCtx->TxFrame),
                    IMC_FRAMETAG(&IMC_pCtx->TxFrame), (T_IMC_DataType)IMC_pCtx->TxFrame.DataType);
            }
        }
        else
        {
            // '1' -> sending failed
            IMC_pCtx->CB.Wait4Ack_Ctr = 1;
            IMC_pCtx->Stats.TxNack++;

            IMC_EVENT(IMC_EVNT_TX_NACK,
                (T_IMC_EvFrameType)IMC_FRAMETYPE(&IMC_pCtx->TxFrame),
                IMC_pCtx->TxFrame.Src, IMC_pCtx->TxFrame.Dst,
                IMC_pCtx->TxFrame.Data, IMC_DATASIZE(&IMC_pCtx->TxFrame),
                IMC_FRAMETAG(&IMC_pCtx->TxFrame), (T_IMC_DataType)IMC_pCtx->TxFrame.DataType);
        }
    }
    #else
    // retransmission mechanism is not used
    if (IMC_pCtx->CB.Wait4Ack_Ctr == 2)
    {
        IMC_pCtx->CB.Wait4Ack_Ctr = 1; // sending failed
        IMC_pCtx->Stats.TxNack++;

        IMC_EVENT(IMC_EVNT_TX_NACK,
            (T_IMC_EvFrameType)IMC_FRAMETYPE(&IMC_pCtx->TxFrame),
            IMC_pCtx->TxFrame.Src, IMC_pCtx->TxFrame.Dst,
            IMC_pCtx->TxFrame.Data, IMC_DATASIZE(&IMC_pCtx->TxFrame),
            IMC_FRAMETAG(&IMC_pCtx->TxFrame), (T_IMC_DataType)IMC_pCtx->TxFrame.DataType);
    }
    #endif // IMC_CONFIG_TX_RETRANS

    #if IMC_CONFIG_USE_MULTIMASTER
    if (!IMC_pCtx->MultiMaster)
        return;

    if (IMC_pCtx->CB.HasToken_Ctr)
    {
        if (IMC_pCtx->CB.Wait4Token)
        {
            // send frame
            IMC_pCtx->CB.SendingData = true;
            IMC_pCtx->CB.TxRetrans_Ctr--;
            IMC_pCtx->CB.TxAck = false;
            IMC_pCtx->CB.Wait4Ack_Ctr = IMC_MAX_TOKEN_TIME; // required for IMC_Status() correct work
            IMC_pCtx->CB.Wait4Token = false;
            IMC_pCtx->Stats.TxFrames++;

            IMC_WriteBuffer(IMC_pCtx->TxFrame.Buffer,
                IMC_DATASIZE(&IMC_pCtx->TxFrame) + IMC_FRAME_HDR_SIZE + IMC_CHSUM_SIZE);

            IMC_EVENT(IMC_pCtx->CB.TxRetrans_Ctr < IMC_CONFIG_TX_RETRANS ?
                IMC_EVNT_TX_RETRANSMISSION : IMC_EVNT_TX_FRAME,
                IMC_FRAMETYPE(&IMC_pCtx->TxFrame), IMC_pCtx->TxFrame.Src, IMC_pCtx->TxFrame.Dst,
                IMC_pCtx->TxFrame.Data, IMC_DATASIZE(&IMC_pCtx->TxFrame),
                IMC_FRAMETAG(&IMC_pCtx->TxFrame), IMC_pCtx->TxFrame.DataType);

            return;
        }

        if (IMC_pCtx->CB.SendingData || IMC_pCtx->CB.SendingControl)
            return;

        if (IMC_pCtx->CB.Wait4Ack_Ctr < 2)
            IMC_pCtx->CB.HasToken_Ctr--;

        // give token back
        if (!IMC_pCtx->CB.HasToken_Ctr)
        {
            IMC_pCtx->CB.SendingToken = true;
            IMC_pCtx->TxFrameTag++;
            IMC_pCtx->TxFrameTag &= IMC_FRAMETAG_MASK;

            IMC_SendControl(IMC_ADDR_NETCONTROLLER, IMC_FT_TOKEN, IMC_pCtx->TxFrameTag);
        }
    }
    #endif
#endif // !IMC_CONFIG_RECEIVE_ONLY
}

//------------------------------------------------------------------------------
