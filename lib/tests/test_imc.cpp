/***************************************************************************************************
 * \file    test_imc.cpp
 *
 * \date    2024-09-28
 *
 **************************************************************************************************/

#include "imc.h"
#include "crc16.h"

#include <CppUTest/TestHarness.h>
#include <CppUTestExt/MockSupport.h>

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

// -----------------------------------------------------------------------------

// configuration required by the following tests
static_assert(IMC_CONFIG_RX_BUFF_COUNT == 2, "");
static_assert(IMC_CONFIG_RECEIVE_ONLY == false, "");
static_assert(IMC_CONFIG_DATA_SIZE == 2500, "");

// -----------------------------------------------------------------------------

TEST_GROUP(IMC)
{
    T_IMC_Ctx  imc_master;
    T_IMC_Ctx  imc_slave;

    static const
    uint8_t    addr_master = 10;
    static const
    uint8_t    addr_slave = 11;

    void setup()
    {
        IMC_Init(&imc_slave);
        IMC_Select(&imc_slave);
        IMC_SetAddress(addr_slave);

        IMC_Init(&imc_master);
        IMC_Select(&imc_master);
        IMC_SetAddress(addr_master);

        IMC_TestRetrans = false;
        mock().ignoreOtherCalls();
    }

    void teardown()
    {
        mock().clear();
        // this is not necessary.
        IMC_Free(&imc_master);
        IMC_Free(&imc_slave);
    }

};

// -----------------------------------------------------------------------------

static uint8_t  imc_framebuff[IMC_DATASIZE_MAX + 20];
static uint16_t imc_framebuff_datasize;

static void write_framebuff_to_receiver(void)
{
    uint16_t datasize = imc_framebuff_datasize;
    imc_framebuff_datasize = 0;

    // during the call IMC_OnRxData() IMC may call IMC_WriteBuffer() to send ACK
    for (uint16_t i = 0; i < datasize; i++)
        IMC_OnRxData(imc_framebuff[i]);
}

extern "C" void IMC_WriteBuffer(uint8_t *pData, uint16_t DataSize)
{
    memcpy(imc_framebuff, pData, DataSize);
    imc_framebuff_datasize = DataSize;
}

extern "C" void IMC_Event(T_IMC_Events Event, T_IMC_EvFrameType FrameType,
    uint8_t Src, uint8_t Dst, const uint8_t *pData, uint16_t DataSize,
    uint8_t FrameTag, T_IMC_DataType Dt)
{
    mock().actualCall("IMC_Event").withIntParameter("event", Event);
}

extern "C" void IMC_CalcCRC16(uint16_t *pCRC16, const void *pData, uint32_t DataSize)
{
    CRC16_Compute(pCRC16, pData, DataSize);
}

extern "C" void IMC_AssertFailed(const char *expression, const char *file, unsigned int line)
{
    fprintf(stderr, "Assert failed: %s @ %s:%u", expression, file, line);
    abort();
}

extern "C" void IMC_Trace(const char *Fmt, ...)
{
}

// -----------------------------------------------------------------------------

TEST(IMC, PendingControlFrame)
{
    IMC_Select(&imc_slave);
    IMC_SetTxSize(0);
    IMC_Write(addr_master, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    // slave is now waiting to finish sending data frame

    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_WriteAbort(); // don't wait for ack

    // write data frame from master to slave while it is in sending state
    IMC_Select(&imc_slave);
    mock().clear();
    mock().expectOneCall("IMC_Event").withIntParameter("event", IMC_EVNT_MSG);
    mock().expectOneCall("IMC_Event").withIntParameter("event", IMC_EVNT_RX_FRAME);
    write_framebuff_to_receiver(); // master -> slave
    // now CtrlFrame.Sheduled == true
    mock().checkExpectations();

    mock().clear();
    mock().expectOneCall("IMC_Event").withIntParameter("event", IMC_EVNT_TX_FRAME);
    mock().expectOneCall("IMC_Event").withIntParameter("event", IMC_EVNT_MSG);
    IMC_OnDataSent(); // send pending control frame
    mock().checkExpectations();
}

// -----------------------------------------------------------------------------

TEST(IMC, Stats)
{
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_AddTxString("Nothing motivates like bankruptcy");
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->TxFrames);
    LONGS_EQUAL(0, p_stats->TxAck);
    LONGS_EQUAL(0, p_stats->TxNack);
    LONGS_EQUAL(0, p_stats->RxFrames);

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent();

    p_stats = IMC_GetStats();
    LONGS_EQUAL(0, p_stats->TxFrames);
    LONGS_EQUAL(0, p_stats->TxAck);
    LONGS_EQUAL(0, p_stats->TxNack);
    LONGS_EQUAL(1, p_stats->RxFrames);

    IMC_Select(&imc_master);
    write_framebuff_to_receiver(); // slave -> master
    p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->TxFrames);
    LONGS_EQUAL(1, p_stats->TxAck);
    LONGS_EQUAL(0, p_stats->TxNack);
    LONGS_EQUAL(0, p_stats->RxFrames);

    IMC_ResetStats();
    p_stats = IMC_GetStats();
    LONGS_EQUAL(0, p_stats->TxFrames);
    LONGS_EQUAL(0, p_stats->TxAck);
    LONGS_EQUAL(0, p_stats->TxNack);
    LONGS_EQUAL(0, p_stats->RxFrames);
}

// -----------------------------------------------------------------------------

TEST(IMC, ByteTimeout)
{
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_AddTxString("one funny word: Qwzgmrg");
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    mock().expectOneCall("IMC_Event").withIntParameter("event", IMC_EVNT_RX_INCOMPLETE);
    mock().ignoreOtherCalls();

    uint8_t *pdata = imc_framebuff;

     // master -> slave
    IMC_Select(&imc_slave);
    // send part of the data
    for (uint16_t i = 0; i < 10; i++, imc_framebuff_datasize--)
    {
        IMC_OnRxData(*pdata++);
        IMC_ProcessStateMachine();
    }
    // simulate time passing calling state machine
    for (uint16_t i = 0; i < IMC_CONFIG_BYTE_TIMEOUT; i++)
        IMC_ProcessStateMachine();
    // send remaining bytes
    while (imc_framebuff_datasize--)
        IMC_OnRxData(*pdata++);

    // because of the gap, the rx buffer should be empty
    CHECK_FALSE(IMC_GetRxReady());
    mock().checkExpectations();
}

// -----------------------------------------------------------------------------

TEST(IMC, HdrChecksumError)
{
    const uint8_t imc_sync_value = (uint8_t)149; // '•'

    IMC_Select(&imc_master);
    // send real frame
    IMC_SetTxSize(0);
    IMC_AddTxString("false-header-is-not-a-problem");
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    IMC_Select(&imc_slave);
    // first, send several false header syncbytes
    for (uint8_t i = 0; i < 10; i++)
        IMC_OnRxData(imc_sync_value);
    for (uint8_t i = 0; i < 6; i++)
        IMC_OnRxData('+');
    for (uint8_t i = 0; i < 3; i++)
        IMC_OnRxData(imc_sync_value);
    // now send the right frame keeped in the buffer
    write_framebuff_to_receiver();  // master -> slave
    IMC_OnDataSent();

    T_IMC_Rd imcrd;
    CHECK_TRUE(IMC_Read2(&imcrd));
    CHECK(imcrd.DataType == IMC_DT_TERMINAL);
    STRCMP_EQUAL("false-header-is-not-a-problem", (char*)imcrd.pData);
    IMC_ReadEnd();
}

// -----------------------------------------------------------------------------

TEST(IMC, UartByteParityError)
{
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    IMC_Select(&imc_slave);
    // master -> slave
    for (uint16_t i = 0; i < imc_framebuff_datasize; i++)
    {
        if (i == 8) // insert one wrong byte
        {
            CHECK_EQUAL(IMC_STAT_RECEIVING, IMC_GetState());
            IMC_OnRxError();
            CHECK_EQUAL(IMC_STAT_IDLE, IMC_GetState());
        }
        else
        {
            IMC_OnRxData(imc_framebuff[i]);
        }
    }
    IMC_OnDataSent();

    CHECK_FALSE(IMC_GetRxReady());
    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(0, p_stats->RxFrames);
    LONGS_EQUAL(1, p_stats->RxErrors);
}

// -----------------------------------------------------------------------------

TEST(IMC, RxFrameBufferOverrun)
{
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);

    for (int i = 0; i <= IMC_CONFIG_RX_BUFF_COUNT; i++)
    {
        IMC_Select(&imc_master);
        IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
        IMC_WriteAbort(); // don't wait for ack

        IMC_Select(&imc_slave);
        write_framebuff_to_receiver(); // master -> slave
        IMC_OnDataSent();
    }

    IMC_Select(&imc_slave);
    CHECK(IMC_GetState() == IMC_STAT_IDLE);
    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(IMC_CONFIG_RX_BUFF_COUNT, p_stats->RxFrames);
    LONGS_EQUAL(1, p_stats->RxOverruns);

    // read all received frames
    CHECK_TRUE(IMC_GetRxReady());
    IMC_ReadEnd();
    CHECK_TRUE(IMC_GetRxReady());
    IMC_ReadEnd();
    CHECK_FALSE(IMC_GetRxReady());
}

// -----------------------------------------------------------------------------

TEST(IMC, WriteTextAndWaitForAck)
{
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_AddTxString("help");
    IMC_AddTxString("?\n");

    T_IMC_WriteRes rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    CHECK(rc == IMC_WRITE_OK);
    CHECK(IMC_GetState() == IMC_STAT_SENDING);
    IMC_OnDataSent();
    CHECK(IMC_GetState() == IMC_STAT_WAIT4ACK);

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver();  // master -> slave
    CHECK(IMC_GetState() == IMC_STAT_SENDING);
    IMC_OnDataSent();
    CHECK(IMC_GetState() == IMC_STAT_IDLE);

    // check slave buffer
    CHECK_TRUE(IMC_GetRxReady());
    T_IMC_Rd imcrd;
    CHECK_TRUE(IMC_Read2(&imcrd));
    CHECK(imcrd.DataType == IMC_DT_TERMINAL);
    STRCMP_EQUAL("help?\n", (char*)imcrd.pData);
    IMC_ReadEnd();
    CHECK_FALSE(IMC_GetRxReady());

    IMC_Select(&imc_master);
    write_framebuff_to_receiver();  // slave -> master
    CHECK(IMC_GetState() == IMC_STAT_SENT_ACK);
}

// -----------------------------------------------------------------------------

TEST(IMC, Broadcast)
{
    // write broadcast frame
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(IMC_ADDR_BROADCAST, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver();  // master -> slave
    LONGS_EQUAL(0, imc_framebuff_datasize); // for broadcast ACK should not be sent
    CHECK_TRUE(IMC_GetRxReady());

    // the statistisc should say all tx packets has been acknowledged
    IMC_Select(&imc_master);
    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->TxFrames);
    LONGS_EQUAL(1, p_stats->TxAck);
    LONGS_EQUAL(0, p_stats->TxNack);
    LONGS_EQUAL(0, p_stats->RxFrames);
}

// -----------------------------------------------------------------------------

TEST(IMC, BadMulticastList)
{
    uint8_t multicast_addr_list[260];
    memset(multicast_addr_list, 1, sizeof(multicast_addr_list));
    multicast_addr_list[sizeof(multicast_addr_list)-1] = 0;
    IMC_Select(&imc_slave);
    CHECK_FALSE(IMC_SetMulticastAddrList(multicast_addr_list));
}

// -----------------------------------------------------------------------------

TEST(IMC, Multicast)
{
    const uint8_t multicast_addr_list[] = { 101, 123, 214, 0, 33, 0 };
    uint8_t multicast_addr_list_long[254];
    memset(multicast_addr_list_long, 1, sizeof(multicast_addr_list_long));
    multicast_addr_list_long[sizeof(multicast_addr_list_long)-1] = 0;

    IMC_Select(&imc_slave);
    CHECK_TRUE(IMC_SetMulticastAddrList(multicast_addr_list));

    // write first multicast frame
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    CHECK(IMC_WRITE_ADDR_ERROR == IMC_Write(IMC_ADDR_BROADCAST, true, IMC_DT_TERMINAL, NULL, 0, NULL));
    CHECK(IMC_WRITE_OK == IMC_Write(101, true, IMC_DT_TERMINAL, NULL, 0, NULL));
    IMC_OnDataSent(); // data sent

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent();
    CHECK_TRUE(IMC_GetRxReady());
    IMC_ReadEnd();

    // write second multicast frame
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(214, true, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent(); // data sent

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent(); // ACK sent
    CHECK_TRUE(IMC_GetRxReady());
    IMC_ReadEnd();

    // write invalid multicast frame
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(33, true, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent(); // data sent

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent(); // ACK sent
    CHECK_FALSE(IMC_GetRxReady());
    IMC_ReadEnd();

    // remove multicast addr list
    IMC_Select(&imc_slave);
    CHECK_TRUE(IMC_SetMulticastAddrList(NULL));
    // write second multicast frame
    IMC_Select(&imc_master);
    IMC_SetTxSize(0); // data sent
    IMC_Write(214, true, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent(); // ACK sent
    CHECK_FALSE(IMC_GetRxReady());
    IMC_ReadEnd();
}

// -----------------------------------------------------------------------------

TEST(IMC, AckCounting1)
{
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_WriteAbort();

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent(); // ACK sent

    IMC_Select(&imc_master);
    // write ack to the master - should be ignored
    write_framebuff_to_receiver(); // slave -> master

    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->TxFrames);
    LONGS_EQUAL(0, p_stats->TxAck);
}

// -----------------------------------------------------------------------------

TEST(IMC, AckCounting2)
{
    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent(); // ACK sent

    int framesize = imc_framebuff_datasize;
    IMC_Select(&imc_master);
    // write ack to the master
    write_framebuff_to_receiver(); // slave -> master
    // write it once again - should be ignored
    imc_framebuff_datasize = framesize;
    write_framebuff_to_receiver(); // slave -> master

    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->TxFrames);
    LONGS_EQUAL(1, p_stats->TxAck);
}

// -----------------------------------------------------------------------------

TEST(IMC, RetransmissionFailed)
{
    IMC_TestRetrans = true;

    IMC_Select(&imc_master);
    IMC_SetTxSize(0);
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    IMC_OnDataSent();

    for (int j = 0; j <= IMC_CONFIG_ACK_TIMEOUT * (IMC_CONFIG_TX_RETRANS+1); j++)
    {
        // here, when ack timeout pass, IMC will send frame again
        IMC_ProcessStateMachine();
        IMC_OnDataSent();
    }

    CHECK(IMC_STAT_SENT_NACK == IMC_GetState());

    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->TxFrames);
    LONGS_EQUAL(0, p_stats->TxAck);
    LONGS_EQUAL(1, p_stats->TxNack);
    LONGS_EQUAL(1, p_stats->TxRetrans);
}

// -----------------------------------------------------------------------------

TEST(IMC, RetransmissionOk)
{
    IMC_TestRetrans = true;
    T_IMC_WriteRes rc;

    for (int i = 0; i < 100; i++)
    {
        IMC_Select(&imc_master);
        IMC_SetTxSize(0);
        rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
        CHECK(rc == IMC_WRITE_OK);
        IMC_OnDataSent();

        IMC_Select(&imc_slave);
        write_framebuff_to_receiver(); // master -> slave
        IMC_OnDataSent();
        IMC_ReadEnd();

        IMC_Select(&imc_master);
        write_framebuff_to_receiver(); // slave -> master
        bool sent_ack = IMC_GetState() == IMC_STAT_SENT_ACK;

        for (int j = 0; j <= IMC_CONFIG_ACK_TIMEOUT; j++)
        {
            // here, when ack timeout pass, IMC will send frame again
            IMC_ProcessStateMachine();
            IMC_OnDataSent();
        }

        if (!sent_ack)
        {
            // write retransmited frame to slave
            IMC_Select(&imc_slave);
            write_framebuff_to_receiver(); // master -> slave
            IMC_OnDataSent();
            IMC_ReadEnd();

            IMC_Select(&imc_master);
            write_framebuff_to_receiver(); // slave -> master
            sent_ack = IMC_GetState() == IMC_STAT_SENT_ACK;
        }

        CHECK_TRUE(sent_ack);
    }

    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(100, p_stats->TxFrames);
    LONGS_EQUAL(100, p_stats->TxAck);
    CHECK(p_stats->TxRetrans > 20); // 1 of 4 ack missed -> ~25
}

// -----------------------------------------------------------------------------

TEST(IMC, WriteAndAbort)
{
    IMC_Select(&imc_master);
    IMC_NewTxPacket();
    LONGS_EQUAL(0, IMC_GetTxSize());
    IMC_AddTxString("help?");
    LONGS_EQUAL(6, IMC_GetTxSize());
    T_IMC_WriteRes rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    CHECK(rc == IMC_WRITE_OK);
    STRCMP_EQUAL("OK", IMC_GetWriteRes_Str(rc));
    STRCMP_EQUAL("?",  IMC_GetWriteRes_Str(T_IMC_WriteRes(IMC_WRITE_DATA_ERROR+20)));

    rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    CHECK(rc == IMC_WRITE_BUSY);
    CHECK(IMC_GetState() == IMC_STAT_SENDING);
    STRCMP_EQUAL("SENDING", IMC_GetState_Str(IMC_GetState()));
    STRCMP_EQUAL("?", IMC_GetState_Str(T_IMC_State(IMC_GetState()+20)));
    IMC_WriteAbort();
    CHECK(IMC_GetState() == IMC_STAT_IDLE);
}

// -----------------------------------------------------------------------------

TEST(IMC, ChecksumError)
{
    T_IMC_WriteRes wrc;

    IMC_Select(&imc_master);
    IMC_NewTxPacket();
    wrc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    LONGS_EQUAL(IMC_WRITE_OK, wrc);

    IMC_Select(&imc_slave);
    // modify last byte to introduce crc checksum error
    imc_framebuff[imc_framebuff_datasize-1] += 1;
    write_framebuff_to_receiver(); // master -> slave

    CHECK_FALSE(IMC_GetRxReady());
    CHECK(IMC_GetState() == IMC_STAT_IDLE);
    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(0, p_stats->RxFrames);
    LONGS_EQUAL(1, p_stats->RxErrors);

    // send once again, without error to find out if IMC will recover from error state
    IMC_Select(&imc_master);
    IMC_WriteAbort(); // cancel ACK wait state
    IMC_NewTxPacket();

    wrc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);
    LONGS_EQUAL(IMC_WRITE_OK, wrc);
    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave

    CHECK_TRUE(IMC_GetRxReady());
    CHECK(IMC_GetState() == IMC_STAT_SENDING);
    p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->RxFrames);
    LONGS_EQUAL(1, p_stats->RxErrors);
}

// -----------------------------------------------------------------------------

/*
TEST(IMC, DropLargeIncomingFrame)
{
    IMC_Select(&imc_master);
    IMC_NewTxPacket();
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, NULL, 0, NULL);

    // modify 4 LSB of Flags to simulate big data frame
    imc_framebuff[4] |= 0x03; // -> DataSize = 3*256
    imc_framebuff_datasize += 3*256;

    // correct frame hdr checksum
    imc_framebuff[5] &= 0x0F;
    imc_framebuff[5] |= ((imc_framebuff[2] + imc_framebuff[3] + imc_framebuff[4]) & 0x0F) << 4;

    mock().expectOneCall("IMC_Event").withIntParameter("event", IMC_EVNT_RX_DROPPED);
    mock().ignoreOtherCalls();
    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    mock().checkExpectations();

    CHECK_FALSE(IMC_GetRxReady());
    POINTERS_EQUAL(NULL, IMC_Read(NULL, NULL, NULL, NULL));
    CHECK(IMC_GetState() == IMC_STAT_IDLE);
}
*/

// -----------------------------------------------------------------------------

/*
TEST(IMC, MissingACK_UseResponse)
{
    int8_t tag_tx = -1;
    IMC_Select(&imc_master);
    IMC_NewTxPacket();
    IMC_Write(addr_slave, false, IMC_DT_TERMINAL, "request", 0, &tag_tx);
    IMC_OnDataSent(); // data sent -> wait for ack

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    IMC_OnDataSent(); // mark sent, but really ACK has been not sent

    // send response with the same frame tag value as in rx frame
    int8_t tag_rx = -1;
    IMC_Read(NULL, NULL, NULL, &tag_rx);
    LONGS_EQUAL(tag_tx, tag_rx);
    IMC_Write(addr_master, false, IMC_DT_TERMINAL, "response", 0, &tag_rx);

    IMC_Select(&imc_master);
    write_framebuff_to_receiver(); // slave -> master
    IMC_Read(NULL, NULL, NULL, &tag_rx);
    LONGS_EQUAL(tag_tx, tag_rx);

    const T_IMC_Stats *p_stats = IMC_GetStats();
    LONGS_EQUAL(1, p_stats->TxFrames);
    LONGS_EQUAL(1, p_stats->TxAck);
    LONGS_EQUAL(1, p_stats->RxFrames);
    LONGS_EQUAL(0, p_stats->RxErrors);
}
*/

// -----------------------------------------------------------------------------

TEST(IMC, WrongSlaveAddr)
{
    IMC_Select(&imc_master);
    IMC_NewTxPacket();
    IMC_Write(addr_slave+10, false, IMC_DT_TERMINAL, NULL, 0, NULL);

    IMC_Select(&imc_slave);
    write_framebuff_to_receiver(); // master -> slave
    CHECK_FALSE(IMC_GetRxReady());
    CHECK(IMC_GetState() == IMC_STAT_IDLE);
}

// -----------------------------------------------------------------------------

TEST(IMC, WriteTooMuch)
{
    uint8_t data[IMC_CONFIG_DATA_SIZE+2];
    int8_t tag = 1;

    memset(data, '+', sizeof(data));
    data[sizeof(data)-1] = '\0';

    IMC_Select(&imc_master);
    IMC_NewTxPacket();
    T_IMC_WriteRes rc = IMC_AddTxData(data, sizeof(data));
    CHECK(rc == IMC_WRITE_DATATOOLONG);

    rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, "short text", 0, &tag);
    CHECK(rc == IMC_WRITE_OK);
    IMC_WriteAbort();

    // write binary data
    IMC_NewTxPacket();
    rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, data, sizeof(data), NULL);
    CHECK(rc == IMC_WRITE_DATATOOLONG);
    IMC_WriteAbort();

    // write binary data
    IMC_NewTxPacket();
    rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, data, sizeof(data)/2, NULL);
    CHECK(rc == IMC_WRITE_OK);
    IMC_WriteAbort();

    // write null-terminaded string
    IMC_NewTxPacket();
    rc = IMC_Write(addr_slave, false, IMC_DT_TERMINAL, data, 0, NULL);
    CHECK(rc == IMC_WRITE_DATATOOLONG);
    IMC_WriteAbort();
}

// -----------------------------------------------------------------------------

TEST(IMC, GetAddress)
{
    const uint8_t multicast_addr_list[] = { 101, 123, 214, 0 };
    IMC_Select(&imc_slave);
    CHECK_TRUE(IMC_SetMulticastAddrList(multicast_addr_list));

    uint8_t addr = 0;
    const uint8_t *p_multicastadresslist = NULL;
    IMC_GetAddress(&addr, &p_multicastadresslist);
    LONGS_EQUAL(addr_slave, addr);
    POINTERS_EQUAL(multicast_addr_list, p_multicastadresslist);

    CHECK_TRUE(IMC_SetMulticastAddrList(NULL));
    IMC_GetAddress(&addr, &p_multicastadresslist);
    LONGS_EQUAL(addr_slave, addr);
    POINTERS_EQUAL(NULL, p_multicastadresslist);
}

// -----------------------------------------------------------------------------

TEST(IMC, GetCurrentContextPointer)
{
    IMC_Select(&imc_master);
    T_IMC_Ctx *p_ctx = IMC_Select(NULL);
    POINTERS_EQUAL(&imc_master, p_ctx);
}

// -----------------------------------------------------------------------------
