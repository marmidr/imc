/*******************************************************************************
 * \file    imc_cfg.h
 *
 * \brief   IMC configuration file
 *
 * \author  Mariusz Midor
 *          https://bitbucket.org/marmidr/imc
 *
 ******************************************************************************/

#ifndef _IMC_CFG_H_
#define _IMC_CFG_H_

////////////////////////////////////////////////////////////////////////////////
/// includes

#include <stdint.h>
#include <stdbool.h>

////////////////////////////////////////////////////////////////////////////////
/// defines, macros

#ifndef ATTR_PACKED
# ifdef __GNUC__
#  define ATTR_PACKED           __attribute__ ((packed))
# else
#  define ATTR_PACKED
# endif
#endif

/*******************************************************************************
 * IMC configuration
 * more information in imc.h "Configuration Guide"
 */

// number of receive buffers for complete frames, minimum 1
#define IMC_CONFIG_RX_BUFF_COUNT                2

// max data bytes in each frame, must be less or eq IMC_DATASIZE_MAX
#define IMC_CONFIG_DATA_SIZE                    2500

// acknowledge timeout for sent packet, in SM intervals (see IMC_SM_INTERVAL() macro)
# define IMC_CONFIG_ACK_TIMEOUT                 250

// next byte of frame timeout, in SM intervals
#define IMC_CONFIG_BYTE_TIMEOUT                 30

// use IMC_CRC16 macro to calculate frame checksum value
#ifndef IMC_CONFIG_USE_CRC
# define IMC_CONFIG_USE_CRC                     true
#endif

// configure as receive-only - transmit functions unavailable, so entire code will be much smaller
#ifndef IMC_CONFIG_RECEIVE_ONLY
# define IMC_CONFIG_RECEIVE_ONLY                false
#endif

// number of retransmissions if case of acknowledge timeout detected
#ifndef IMC_CONFIG_TX_RETRANS
# define IMC_CONFIG_TX_RETRANS                  2
#endif

// call IMC_Event() for debug purposes
#ifndef IMC_CONFIG_USE_EVENTS
# define IMC_CONFIG_USE_EVENTS                  true
#endif

#ifndef IMC_CONFIG_TEST_RETRANS
# define IMC_CONFIG_TEST_RETRANS                false
#endif

// assertion: alternative solution using user implemented handler to avoid program abort
#ifndef IMC_ASSERT
# define IMC_ASSERT(expr)                       if (!(expr)) IMC_AssertFailed(#expr, __FILE__, __LINE__)
#endif

//------------------------------------------------------------------------------

#endif
