/*******************************************************************************
 *  \file   imc.h
 *
 *  \brief  IMC - Inter Module Communication protocol
 *
 *  \author Mariusz Midor
 *          https://bitbucket.org/marmidr/imc
 *
 ******************************************************************************/

#ifndef _IMC_H_
#define _IMC_H_

////////////////////////////////////////////////////////////////////////////////
/// includes

#include "imc_cfg.h"

#include <stdint.h>
#include <stddef.h>

/*******************************************************************************
 *  CONFIGURATION GUIDE
 *
 *  IMC is widely configurable. Options must be defined in "imc_cfg.h"
 *  The following options are possible:
 *
 *  @IMC_CONFIG_RX_BUFF_COUNT
 *  defines how many frame buffers will be used. the minimum is 2, if the device
 *  can not read received data immediatelly - more buffers may be required
 *
 *  @IMC_CONFIG_DATA_SIZE
 *  you can specify maximum data size that will be sent using IMC.
 *  the value may be from 0 up to 4095. bigger data size - more memory used
 *  for the receive buffers!
 *
 *  @IMC_CONFIG_WAIT4ACK_TIMEOUT
 *  each data frame is acknowledged. if devices are not connected directly
 *  to one wire, ack delay may be longer.
 *  timeout value may be from 5 to 255
 *
 *  @IMC_CONFIG_USE_CRC
 *  IMC uses simple inverted sum of bytes as checksum, or CRC16 alternatively.
 *  define this constant to use CRC checksum.
 *
 *  @IMC_CONFIG_RECEIVE_ONLY
 *  specify this constant if the device only receives data. this may save a lot
 *  of memory space by cutting of the unused code.
 *
 *  @IMC_CONFIG_TX_RETRANS
 *  if no ACK is received, IMC will try sending the data again specified number of times
 *
 *  @IMC_CONFIG_USE_EVENTS
 *  use an event handler function to examine all the frames being passed through the network.
 *  if receive buffers are full or frame data size is bigger
 *  than IMC_CONFIG_DATA_SIZE - it fails on receive
 *
 *  All timeout calculations are based on transmission time of control frame,
 *  which is 12 bytes long.
 *  Transfer time is calculated from baudrate (bits per second, assuming
 *  the biggest, 12-bit RS-232 frame)
 *
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
/// preprocessor definitions

typedef enum
{
    /** frame received */
    IMC_EVNT_RX_FRAME,
    /** frame sent */
    IMC_EVNT_TX_FRAME,
    /** rx timeout -> frame dropped */
    IMC_EVNT_RX_INCOMPLETE,
    /** IMC_OnRxError called -> frame dropped */
    IMC_EVNT_RX_ERROR,
    /** frame complete, but invalid CRC */
    IMC_EVNT_RX_CHSUM_ERROR,
    /** ACK timeout - send again */
    IMC_EVNT_TX_RETRANSMISSION,
    /** delivery failed */
    IMC_EVNT_TX_NACK,
    /** text message */
    IMC_EVNT_MSG,
    /** rx frame data too big -> frame dropped */
    IMC_EVNT_RX_DROPPED
} T_IMC_Events;

typedef enum
{
    IMC_EVNT_FT_DUMMY =             (0 << 4), // dummy, reserved
    IMC_EVNT_FT_ECHO =              (1 << 4), // sent ACK and return
    IMC_EVNT_FT_ACK =               (2 << 4), // data/echo ACK
    IMC_EVNT_FT_DATA =              (3 << 4), // data frame
    IMC_EVNT_FT_TOKEN =             (4 << 4), // pass token to recipent
    IMC_EVNT_FT_ACK_ECHO_SLAVE =    (5 << 4), // echo ACK - device is slave only
    IMC_EVNT_FT_R1 =                (6 << 4), // rsvd
    IMC_EVNT_FT_R2 =                (7 << 4)  // rsvd
} T_IMC_EvFrameType;

// data types
typedef enum
{
    IMC_DT_NONE = 0,
    /** CPU firmware */
    IMC_DT_FIRMWARE = 1,
    /** low level disc access */
    IMC_DT_DISK_IO = 2,
    /** high level disc access */
    IMC_DT_FILE_IO = 3,
    /** hmi registers access  */
    IMC_DT_REG_IO = 4,
    /** */
    IMC_DT_RSVD = 5,
    /** text terminal over IMC */
    IMC_DT_TERMINAL = 6,
    /** ask for device status */
    IMC_DT_STATUS = 7,
    /** debug channel for Lua */
    IMC_DT_DEBUG = 8,
    // following are the user defined data types/commands
    IMC_DT_USER_0 = 10
} T_IMC_DataType;

/** broadcast address - constant */
#define IMC_ADDR_BROADCAST          0
/** network controller address - constant */
#define IMC_ADDR_NETCONTROLLER      255
/** IMC state machine interval in ms; it is time needed to send IMC control frame like ACK */
#define IMC_SM_INTERVAL(baudrate)   ((12L * 10000L / baudrate) + 1)
/** maximum data size IMC frame can transport; do not change this value! */
#define IMC_DATASIZE_MAX            4095

////////////////////////////////////////////////////////////////////////////////
/// types

/** communication statistics */
typedef struct
{
    /** number of received frames */
    uint32_t    RxFrames;
    /** number of rx data errors or checksum errors while receiving */
    uint16_t    RxErrors;
    /** how many times frame to be received cannot be stored because of lack of the free bufer */
    uint16_t    RxOverruns;
    /** number of frames written to the output */
    uint32_t    TxFrames;
    /** number of frames send and acknowledged, should be equal to TxFrames */
    uint32_t    TxAck;
    /** number of retransmited frames due to ack timeout;
     *  incremented only once for every data frame to be transmitted,
     *  no matter how big the IMC_CONFIG_TX_RETRANS value is */
    uint32_t    TxRetrans;
    /** number of sent frames (after retransmissions) without ack */
    uint32_t    TxNack;
} T_IMC_Stats;

/** result of Write function */
typedef enum
{
    /** data has been written to the output */
    IMC_WRITE_OK,
    /** IMC is waiting for token (multimaster mode only) */
    IMC_WRITE_WAIT4TOKEN,
    /** data size is to big for defined frame datasize */
    IMC_WRITE_DATATOOLONG,
    /** IMC statemachine is busy (sending/waiting for ack) */
    IMC_WRITE_BUSY,
    /** invalid destination address */
    IMC_WRITE_ADDR_ERROR,
    /** invalid data */
    IMC_WRITE_DATA_ERROR,
} T_IMC_WriteRes;

/** current IMC state machine state */
typedef enum
{
    // stares below are treated as 'free to send'
    /** data sent and acknowledged */
    IMC_STAT_SENT_ACK,
    /** data sent, but no acknowledged has been received */
    IMC_STAT_SENT_NACK,
    /** */
    IMC_STAT_IDLE,
    // states below are treated as 'busy' states
    /** IMC is currently sending a data */
    IMC_STAT_SENDING,
    /** IMC is currently receiving data */
    IMC_STAT_RECEIVING,
    /** frame was sent, now waiting for acknowledge */
    IMC_STAT_WAIT4ACK,
    /** waiting for a token (multimaster mode only) */
    IMC_STAT_WAIT4TOKEN
} T_IMC_State;

/** structure with all incoming frame parameters */
typedef struct
{
    /** address of sender device */
    uint8_t         Addr;
    /** pointer to data buffer */
    void *          pData;
    /** size of data buffer */
    uint16_t        DataSize;
    /** type of data in buffer */
    T_IMC_DataType  DataType;
    /** 4-bit frame tag */
    int8_t          FrameTag;
} T_IMC_Rd;

/** IMC context buffer; defined in that way to prevent manual modifications */
typedef uint32_t    T_IMC_Ctx[(88+((12+12+IMC_CONFIG_DATA_SIZE) * (IMC_CONFIG_RX_BUFF_COUNT + (!IMC_CONFIG_RECEIVE_ONLY))))/4];

////////////////////////////////////////////////////////////////////////////////
/// constants, variables

////////////////////////////////////////////////////////////////////////////////
/// prototypes
#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * @brief   Initialize pointed structure
 * @param   pContext - pointer to IMC context structure
 */
void IMC_Init(T_IMC_Ctx *pContext);

/*******************************************************************************
 * @brief   Free memory allocated for pointed context; function reset pointer
 *          to current context so any IMC call will be invalid, until next Select()
 * @param   pContext - pointer to previously initialized IMC context structure
 */
void IMC_Free(T_IMC_Ctx *pContext);

/*******************************************************************************
 * @brief   Select context the module will be working on
 * @param   pContext - pointer to previously initialized IMC context structure or NULL
 * @return  Prevoius context pointer on success,
 *          current if pContext == NULL
 */
T_IMC_Ctx * IMC_Select(T_IMC_Ctx *pContext);

/*******************************************************************************
 * @brief   Returns constant pointer to IMC stats structure
 * @return  Pointer
 */
const T_IMC_Stats * IMC_GetStats(void);

/*******************************************************************************
 * @brief   Reset statistic counters
 */
void IMC_ResetStats(void);

/*******************************************************************************
 * @brief   Set basic device address
 * @param   DeviceAddr - Device address (1..254)
 * @return  true if value is correct
 */
bool IMC_SetAddress(uint8_t DeviceAddr);

/*******************************************************************************
 * @brief   Set multicast address list
 * @param   pMulticastAddrList - pointer to 0-terminated list, max 254 elements,
 *          of accepted multicast device addresses (1..254)
 *          or NULL to remove adddress list
 * @return  true if values are correct
 */
bool IMC_SetMulticastAddrList(const uint8_t *pMulticastAddrList);

/*******************************************************************************
 * @brief   Get local and multicast IMC addresses
 * @param   pDeviceAddr - pointer to store device address or NULL
 * @param   ppMulticastAddrList - pointer to store pointer to 0-terminated multicast
 *          address list or NULL
 */
void IMC_GetAddress(uint8_t *pDeviceAddr, const uint8_t **ppMulticastAddrList);

/*******************************************************************************
 * @brief   Sends data to Addr device
 * @param   Addr - Destination device address
 * @param   Multicast - Is Addr a multicast address
 * @param   DataType - Type of data to be transported in the frame, see IMC_DT_xxx
 * @param   pData - Data or string pointer
 * @param   DataSize - pData size or 0 if pData is NULL
 * @param   pTag - pointer to FrameTag value (if *pTag >= 0);
 *          if *pTag < 0 -> pointer will receive automatic internal frame tag value;
 *          if NULL, internal frame tag value will be used
 * @return  Result code
 */
#if !IMC_CONFIG_RECEIVE_ONLY
T_IMC_WriteRes IMC_Write(uint8_t Addr, bool Multicast, T_IMC_DataType DataType, const void *pData, size_t DataSize, int8_t *pTag);
#endif

/*******************************************************************************
 * @brief   Compose tx packet from individual bytes.
 *          Before the first call, call the IMC_SetTxSize(0) to reset tx buffer size counter.
 *          Use this function only for binary data.
 *          When tx packet is ready, call IMC_Write(addr, false, dtype, NULL, 0, NULL)
 * @param   pData - Pointer to binary data to add to tx buffer
 * @param   Size of pointed data
 * @return  The return values are the same as for IMC_Write()
 */
#if !IMC_CONFIG_RECEIVE_ONLY
T_IMC_WriteRes IMC_AddTxData(const void *pData, size_t Size);
#endif

/*******************************************************************************
 * @brief   Compose tx packet from strings
 *          Before the first call, call the IMC_SetTxSize(0) to reset tx buffer size counter.
 *          This function concatenate strings, overwriting the last character '\0' in tx buffer
 *          and appending '\0' at the end
 *          Do not use this function together with IMC_AddTxData() because it will corrupt
 *          existing data if IMC_SetTxSize(IMC_GetTxSize()+1) has not been used.
 *          When tx packet is ready, call IMC_Write(addr, false, dtype, NULL, 0, NULL)
 * @param   Text - Pointer to string to add to tx buffer
 * @return  The return values are the same as for IMC_Write()
 */
#if !IMC_CONFIG_RECEIVE_ONLY
T_IMC_WriteRes IMC_AddTxString(const char *Text);
#endif

/*******************************************************************************
 * @brief   Return data size in Tx buffer added by IMC_AddTxData() or IMC_AddTxString()
 */
#if !IMC_CONFIG_RECEIVE_ONLY
uint16_t IMC_GetTxSize(void);
#endif

/*******************************************************************************
 * @brief   Set new value of tx buff size counter
 */
#if !IMC_CONFIG_RECEIVE_ONLY
void IMC_SetTxSize(uint16_t TxSize);

/**
 * @brief   Clear the Tx buffer size counter before new transmission
 */
static __inline void IMC_NewTxPacket(void) { IMC_SetTxSize(0); }

#endif

/*******************************************************************************
 * @brief   Returns string representation of WriteResult
 */
#if !IMC_CONFIG_RECEIVE_ONLY
const char * IMC_GetWriteRes_Str(T_IMC_WriteRes Res);
#endif

/*******************************************************************************
 * @brief   Abort any pending write process (sending DATA or ACK)
 */
#if !IMC_CONFIG_RECEIVE_ONLY
void IMC_WriteAbort(void);
#endif

/*******************************************************************************
 * @brief   Check if any received frame is in the buffer
 */
bool IMC_GetRxReady(void);

/*******************************************************************************
 * @brief   Read data if any in buffer
 * @param   pSrcAddr - Pointer to variable receiving sender device address or NULL
 * @param   pDataType - Pointer to variable receiving type of data or NULL
 * @param   pDataSize - Pointer to variable receiving data size or NULL
 * @param   pTag - Pointer to variable receiving frame Tag or NULL
 * @return  Pointer to data buffer or NULL if there is no data in Rx buffer
 */
void *IMC_Read(uint8_t *pSrcAddr, T_IMC_DataType *pDataType, uint16_t *pDataSize, int8_t *pTag);

/*******************************************************************************
 * @brief   More convienient data read
 * @param   pIO - pointer to structure receiving all informations about received frame
 * @return  true if pIO has been initialized with existing rx frame
 */
bool IMC_Read2(T_IMC_Rd *pRd);

/*******************************************************************************
 * @brief   Must be called each time after IMC_Read, to release rx data buffer
 */
void IMC_ReadEnd(void);

/*******************************************************************************
 * @return  Return current IMC state machine status
 */
T_IMC_State IMC_GetState(void);

/*******************************************************************************
 * @return  Return string representation of IMC State
 */
const char *IMC_GetState_Str(T_IMC_State Stat);

/*******************************************************************************
 * @brief   Periodically called IMC state machine routine
 *          Period [ms] is calculated by macro IMC_SM_INTERVAL(baudrate)
 *          but always must be > 0 (so 0.1ms may be OK for high baudrates)
 */
void IMC_ProcessStateMachine(void);

/*******************************************************************************
 * @brief   Push received data to the IMC
 * @param   Data - Received byte
 * @return  false if buffers are full, true otherwise
 */
bool IMC_OnRxData(uint8_t Data);

/*******************************************************************************
 * @brief   Signal RX error (parity/frame/stop) to IMC.
 *          Previously received data will be rejected
 */
void IMC_OnRxError(void);

/*******************************************************************************
 * @brief   Tell IMC that tx frame has been sent -
 *          from now it can wait for ACK
 */
void IMC_OnDataSent(void);


/******************************************************************************/
/******************************************************************************/
/******************************************************************************/


/*******************************************************************************
 * @brief   Called when data is ready to send,
 *          must be implemented in user code
 * @param   pData - pointer to data to send
 * @param   DataSize - size of the pData to send
 */
extern void IMC_WriteBuffer(uint8_t *pData, uint16_t DataSize);

/*******************************************************************************
 * @brief   Events callback function, must be implemented in user code
 * @param   Event - event type
 * @param   FrameType - frame type; n/a if IMC_EVNT_RX_INCOMPLETE, IMC_EVNT_RX_ERROR
 * @param   Src - source address; n/a if IMC_EVNT_RX_INCOMPLETE, IMC_EVNT_RX_ERROR
 * @param   Dst - dest address; n/a if IMC_EVNT_RX_INCOMPLETE, IMC_EVNT_RX_ERROR
 * @param   pData - data pointer; n/a if IMC_EVNT_RX_INCOMPLETE, IMC_EVNT_RX_ERROR;
 *          string pointer if IMC_EVNT_MSG
 * @param   DataSize - size of data; n/a if IMC_EVNT_RX_INCOMPLETE, IMC_EVNT_RX_ERROR
 * @param   FrameTag - tag number 0..15, incremented for each TxFrame
 * @param   Dt - DataType if FrameType==IMC_EVNT_FT_DATA
 */
#if IMC_CONFIG_USE_EVENTS
extern void IMC_Event(T_IMC_Events Event, T_IMC_EvFrameType FrameType,
    uint8_t Src, uint8_t Dst, const uint8_t *pData, uint16_t DataSize, uint8_t FrameTag, T_IMC_DataType Dt);
#endif

/*******************************************************************************
 * @brief   Prints message to terminal,
 *          must be implemented in user code
 * @param   Fmt - printf() format arguments
 */
extern void IMC_Trace(const char *Fmt, ...);

/*******************************************************************************
 * @brief   Calc the CRC16 from given chunk of data
 *          must be implemented in user code
 * @param   pCRC16 - Internal variable that stores CRC value; initialy zeroed
 * @param   pData - pointer to data
 * @param   DataSize - pData number of bytes
 */
extern void IMC_CalcCRC16(uint16_t *pCRC16, const void *pData, uint32_t DataSize);

/*******************************************************************************
 * @brief   Called on runtime assertion failure
 */
extern void IMC_AssertFailed(const char *expression, const char *file, unsigned int line);

#ifdef __cplusplus
}
#endif

////////////////////////////////////////////////////////////////////////////////
/// global variables

// configuration

/** send 3 of 4 acks if true */
#if IMC_CONFIG_TEST_RETRANS
extern bool IMC_TestRetrans;
#endif

//------------------------------------------------------------------------------

#endif
