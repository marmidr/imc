/*******************************************************************************
 * \file    imc_packet.h
 *
 * \brief   Data packets definitions for IMC
 *
 * \author  Mariusz Midor
 *          https://bitbucket.org/marmidr/imc
 *
 ******************************************************************************/

#ifndef _IMC_PACKET_H_
#define _IMC_PACKET_H_

////////////////////////////////////////////////////////////////////////////////
/// includes

#include "imc_cfg.h"

////////////////////////////////////////////////////////////////////////////////
/// preprocessor definitions

// firmware flags
#define IFW_FLAG_ACK            BIT(0)
#define IFW_FLAG_TOOBIG         BIT(1)
#define IFW_FLAG_DATAERR        BIT(2)
#define IFW_FLAG_ABORTED        BIT(3)

#define IMC_DP_DATASIZE         (2048)      // data
#define IMC_DP_BUFFSIZE         (2048+64)   // data+spare

////////////////////////////////////////////////////////////////////////////////
/// types

typedef enum
{
    ICMD_FW_NONE,
    ICMD_FW_INFO,
    ICMD_FW_DATA,
    ICMD_FW_ABORT
} ATTR_PACKED T_IMC_FwCmd;


#ifdef __BORLANDC__
    #pragma pack(push, 1)
#endif

typedef struct ATTR_PACKED
{
    struct
    {
        T_IMC_FwCmd Cmd;
        uint8_t     Result;
        uint8_t     Flag;
        uint8_t     rsvd;
    } Hdr;

    union
    {
        struct ATTR_PACKED
        {
            uint32_t  Size;
            uint32_t  CRC32;
            uint32_t  FileDateTime; //zakodowane jak w fat
            uint32_t  Rsvd32[3];
            char    FileName[256];
        } FwInfo;

        struct ATTR_PACKED
        {
            uint32_t  PacketNo;
            uint32_t  PacketCount;
            uint16_t  DataSize; // 10bits used
            uint16_t  Rsvd16;
            char    Data[512];
        } FwData;
    };
} T_IMC_FwPacket;

#ifdef __BORLANDC__
    #pragma pack(pop)
#endif

//-------

typedef enum
{
    ICMD_DISK_NONE,
    /** wyłączny dostęp do napędu  */
    ICMD_DISK_EXCLUSIVE_ENTER,
    ICMD_DISK_EXCLUSIVE_EXIT,
    /** maska z listą napędów */
    ICMD_DISK_GET_DISKS,
    /** geometria pamięci: rozmiar sektora, ilość sektorów */
    ICMD_DISK_GET_INFO,
    // FAT
    /** odczyt sektora */
    ICMD_DISK_SECTOR_READ,
    /** zapis sektora */
    ICMD_DISK_SECTOR_WRITE,
    // YAFFS
    /** odczyt strony */
    ICMD_DISK_PAGE_READ,
    /** zapis strony */
    ICMD_DISK_PAGE_WRITE,
    /** kasowanie bloku */
    ICMD_DISK_BLOCK_ERASE,
    /** pobranie spare ze stron całego bloku */
    ICMD_DISK_BLOCK_GETBRIEF,
} ATTR_PACKED T_IMC_DiskCmd;

typedef enum
{
    ICMD_RSLT_UNKNOWN,
    ICMD_RSLT_OK,
    ICMD_RSLT_ERROR,
    ICMD_RSLT_BLOCK_BAD,
    ICMD_RSLT_INVALID_SESSION,
} ATTR_PACKED T_IMC_DiskRslt;

#ifdef __BORLANDC__
    #pragma pack(push, 1)
#endif

typedef struct ATTR_PACKED
{
    struct ATTR_PACKED
    {
        T_IMC_DiskCmd   Cmd;
        T_IMC_DiskRslt  Result;
        uint8_t         DiskNo;
        uint8_t         rsv1;
        uint16_t        SessionID;
        uint16_t        rsvd2;

        union
        {
            struct ATTR_PACKED
            {
                /** numer sektora */
                uint32_t    Sector;
                /** rozmiar danych */
                uint32_t    DataSize:   14;
                /** czy dane skompresowane */
                uint32_t    Compressed: 1;
                /** ilość sektorów w paczce danych */
                uint32_t    NSectors:   3;
                /** subsektor (gdy rozmiar sektora > IMC_DP_DATASIZE) */
                uint32_t    Subsect:    3;
                /** ilość subsektorów -1 */
                uint32_t    NSubsec:    3;
            } FAT;

            struct ATTR_PACKED
            {
                /** numer bloku */
                uint32_t    Block:      16;
                /** numer strony */
                uint32_t    Page:       16;
                /** rozmiar pagedata */
                uint32_t    DataSize:   14;
                /** rozmiar sparedata */
                uint32_t    SpareSize:  8;
                /** czy pagedata skompresowane */
                uint32_t    Compressed: 1;
                /** sprawdzenie/ustawienie znacznika badblock */
                uint32_t    BadBlock:   1;
            } YAFFS;

            uint32_t    Buff[4];
        };
    } Hdr;

    union
    {
        uint16_t    Disks;

        struct ATTR_PACKED
        {
            /***** FAT */
            /** rozmiar sektora w bajtach */
            uint16_t  SectorSize;
            uint16_t  rsvd1;
            /** ilość sektorów */
            uint32_t  SectorsCount;

            /***** YAFFS */
            /** rozmiar strony flash */
            uint16_t  PageDataSize;
            /** ilość bajtów dodatkowych na stronę */
            uint8_t   PageSpareSize;
            /** offset bajtu statusu strony w obszarze Spare */
            uint8_t   BlockStatusOffs;
            /** ilość stron w bloku */
            uint32_t  PagesInBlock;
            /** ilość bloków */
            uint32_t  BlockCount;
            uint16_t  rsvd2[8];
            /** nazwa systemu plików, np. FAT16 */
            char      FileSystemName[10];
        } DiskInfo;

        /** dane sektora/sektorów/strony/spare */
        uint8_t Data[IMC_DP_BUFFSIZE];
    };
} T_IMC_DiskPacket;

typedef enum
{
    ICMD_DBG_NONE,
    /** włączenie/wyłączenie debugowania (gdy włączony, skrypt działa dużo wolniej) */
    ICMD_DBG_ACTIVATE,
    ICMD_DBG_DEACTIVATE,
    /** polecenia sterujące */
    ICMD_DBG_PAUSE,
    ICMD_DBG_CONTINUE,
    ICMD_DBG_STEPOVER,
    ICMD_DBG_STEPINTO,
    ICMD_DBG_STEPOUT,
    ICMD_DBG_RUNTO,
    /** dodawanie/usuwanie breakpointów */
    ICMD_DBG_BREAKPOINT_ADD,
    ICMD_DBG_BREAKPOINT_DEL,
    ICMD_DBG_BREAKPOINT_REPLACEALL,
    ICMD_DBG_BREAKPOINT_GETALL,
    ICMD_DBG_BREAKPOINT_CLEAR,
    /** handler linii */
    ICMD_DBG_LINEHANDLER,
    /** inspekcja zmiennej (składnik po składniku jesli pole tablicy)
     * szuka kolejno w lokal, _ENV i na końcu w _G */
    ICMD_DBG_EVALUATE,
    /** dodanie zmiennych do stalej inspekcji, zwracane obok locaklnych z każdym eventem */
    ICMD_DBG_WATCH_ADD,
    ICMD_DBG_WATCH_DEL,
    ICMD_DBG_WATCH_CLEAR,
} ATTR_PACKED T_IMC_DebugCmd;

typedef enum
{
    ICMD_DBGRSLT_UNKNOWN,
    ICMD_DBGRSLT_OK,
    ICMD_DBGRSLT_ERROR,
} ATTR_PACKED T_IMC_DebugRslt;

typedef struct
{
    struct ATTR_PACKED
    {
        T_IMC_DebugCmd  Cmd;
        T_IMC_DebugRslt Result;
        uint8_t         rsv0;
        uint8_t         rsv1;
        uint16_t        SessionID;
        uint16_t        rsvd2;
    } Hdr;

    union
    {
        /** lista wszystkich breakpointów: plik:linia!oneshoot;plik:linia!oneshoot; */
        struct ATTR_PACKED
        {
            char        Data[1000];
        } Breakpoint;

        struct ATTR_PACKED
        {
            uint16_t    Line;
            uint16_t    rsvd[6];
            char        FileName[60];
        } RunTo;

        struct ATTR_PACKED
        {
            // statystyki w momencie zdarzenia
            struct ATTR_PACKED
            {
                // w kB
                uint16_t    MemUsed;
                uint16_t    MemUsedMax;
                uint16_t    MemTotal;
                uint16_t    MemLua;
                uint16_t    MemObjects;
                uint16_t    MemString;
                uint16_t    rsvd1[4];

                uint32_t    ObjectsCount;
                uint32_t    ObjectsCountMax;
                uint16_t    IQue;
                uint16_t    OQue;
                uint16_t    rsvd2[4+12];
            } Stat; // razem 64B

            /** linia skryptu */
            struct ATTR_PACKED
            {
                uint16_t    CurrentLine;
                bool        IsReturnEvent;
                /** numery aktywnych linii wykonywanej funkcji */
                uint8_t     ActiveLinesCount;
                uint16_t    ActiveLines[100];
                uint8_t     rsvd[8];
                /** nazwa skryptu, bez rozszerzenia */
                char        FileName[60];
                /** nazwa:typ!zakres=wartość\fnazwa:typ!zakres=wartość\f
                 typ: LUA_TNUMBER (jako string)
                 zakres: l(local)/g(global)
                 wartość: tekst */
            } Info;

            char Variables[1800];
        } LineHandler;

        struct ATTR_PACKED
        {
            /** modules[modules.index].Value */
            char Expression[120];

            struct ATTR_PACKED
            {
                /** typ zmiennej (gdy zwracana) LUA_TNUMBER */
                uint8_t Type;
                /** */
                uint8_t rsvd[3+4];
                /** wartość (gdy zwracana) */
                char    Value[2000];
            } Response;
        } Evaluate;
    };
} ATTR_PACKED T_IMC_DebugPacket;

#ifdef __BORLANDC__
    #pragma pack(pop)
#endif

////////////////////////////////////////////////////////////////////////////////
/// constants, variables

////////////////////////////////////////////////////////////////////////////////
/// prototypes

////////////////////////////////////////////////////////////////////////////////
/// global variables

//------------------------------------------------------------------------------

#endif
