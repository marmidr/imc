#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/CppUTest/CommandLineArguments.o \
	${OBJECTDIR}/src/CppUTest/CommandLineTestRunner.o \
	${OBJECTDIR}/src/CppUTest/JUnitTestOutput.o \
	${OBJECTDIR}/src/CppUTest/MemoryLeakDetector.o \
	${OBJECTDIR}/src/CppUTest/MemoryLeakWarningPlugin.o \
	${OBJECTDIR}/src/CppUTest/SimpleMutex.o \
	${OBJECTDIR}/src/CppUTest/SimpleString.o \
	${OBJECTDIR}/src/CppUTest/TestFailure.o \
	${OBJECTDIR}/src/CppUTest/TestFilter.o \
	${OBJECTDIR}/src/CppUTest/TestHarness_c.o \
	${OBJECTDIR}/src/CppUTest/TestMemoryAllocator.o \
	${OBJECTDIR}/src/CppUTest/TestOutput.o \
	${OBJECTDIR}/src/CppUTest/TestPlugin.o \
	${OBJECTDIR}/src/CppUTest/TestRegistry.o \
	${OBJECTDIR}/src/CppUTest/TestResult.o \
	${OBJECTDIR}/src/CppUTest/Utest.o \
	${OBJECTDIR}/src/CppUTestExt/CodeMemoryReportFormatter.o \
	${OBJECTDIR}/src/CppUTestExt/MemoryReportAllocator.o \
	${OBJECTDIR}/src/CppUTestExt/MemoryReportFormatter.o \
	${OBJECTDIR}/src/CppUTestExt/MemoryReporterPlugin.o \
	${OBJECTDIR}/src/CppUTestExt/MockActualCall.o \
	${OBJECTDIR}/src/CppUTestExt/MockExpectedCall.o \
	${OBJECTDIR}/src/CppUTestExt/MockExpectedCallsList.o \
	${OBJECTDIR}/src/CppUTestExt/MockFailure.o \
	${OBJECTDIR}/src/CppUTestExt/MockNamedValue.o \
	${OBJECTDIR}/src/CppUTestExt/MockSupport.o \
	${OBJECTDIR}/src/CppUTestExt/MockSupportPlugin.o \
	${OBJECTDIR}/src/CppUTestExt/MockSupport_c.o \
	${OBJECTDIR}/src/CppUTestExt/OrderedTest.o \
	${OBJECTDIR}/src/Platforms/Gcc/UtestPlatform.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/libcpputest-3.7.2.a

${CND_DISTDIR}/libcpputest-3.7.2.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}
	${RM} ${CND_DISTDIR}/libcpputest-3.7.2.a
	${AR} -rv ${CND_DISTDIR}/libcpputest-3.7.2.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/libcpputest-3.7.2.a

${OBJECTDIR}/src/CppUTest/CommandLineArguments.o: src/CppUTest/CommandLineArguments.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/CommandLineArguments.o src/CppUTest/CommandLineArguments.cpp

${OBJECTDIR}/src/CppUTest/CommandLineTestRunner.o: src/CppUTest/CommandLineTestRunner.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/CommandLineTestRunner.o src/CppUTest/CommandLineTestRunner.cpp

${OBJECTDIR}/src/CppUTest/JUnitTestOutput.o: src/CppUTest/JUnitTestOutput.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/JUnitTestOutput.o src/CppUTest/JUnitTestOutput.cpp

${OBJECTDIR}/src/CppUTest/MemoryLeakDetector.o: src/CppUTest/MemoryLeakDetector.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/MemoryLeakDetector.o src/CppUTest/MemoryLeakDetector.cpp

${OBJECTDIR}/src/CppUTest/MemoryLeakWarningPlugin.o: src/CppUTest/MemoryLeakWarningPlugin.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/MemoryLeakWarningPlugin.o src/CppUTest/MemoryLeakWarningPlugin.cpp

${OBJECTDIR}/src/CppUTest/SimpleMutex.o: src/CppUTest/SimpleMutex.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/SimpleMutex.o src/CppUTest/SimpleMutex.cpp

${OBJECTDIR}/src/CppUTest/SimpleString.o: src/CppUTest/SimpleString.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/SimpleString.o src/CppUTest/SimpleString.cpp

${OBJECTDIR}/src/CppUTest/TestFailure.o: src/CppUTest/TestFailure.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestFailure.o src/CppUTest/TestFailure.cpp

${OBJECTDIR}/src/CppUTest/TestFilter.o: src/CppUTest/TestFilter.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestFilter.o src/CppUTest/TestFilter.cpp

${OBJECTDIR}/src/CppUTest/TestHarness_c.o: src/CppUTest/TestHarness_c.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestHarness_c.o src/CppUTest/TestHarness_c.cpp

${OBJECTDIR}/src/CppUTest/TestMemoryAllocator.o: src/CppUTest/TestMemoryAllocator.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestMemoryAllocator.o src/CppUTest/TestMemoryAllocator.cpp

${OBJECTDIR}/src/CppUTest/TestOutput.o: src/CppUTest/TestOutput.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestOutput.o src/CppUTest/TestOutput.cpp

${OBJECTDIR}/src/CppUTest/TestPlugin.o: src/CppUTest/TestPlugin.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestPlugin.o src/CppUTest/TestPlugin.cpp

${OBJECTDIR}/src/CppUTest/TestRegistry.o: src/CppUTest/TestRegistry.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestRegistry.o src/CppUTest/TestRegistry.cpp

${OBJECTDIR}/src/CppUTest/TestResult.o: src/CppUTest/TestResult.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/TestResult.o src/CppUTest/TestResult.cpp

${OBJECTDIR}/src/CppUTest/Utest.o: src/CppUTest/Utest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTest
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTest/Utest.o src/CppUTest/Utest.cpp

${OBJECTDIR}/src/CppUTestExt/CodeMemoryReportFormatter.o: src/CppUTestExt/CodeMemoryReportFormatter.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/CodeMemoryReportFormatter.o src/CppUTestExt/CodeMemoryReportFormatter.cpp

${OBJECTDIR}/src/CppUTestExt/MemoryReportAllocator.o: src/CppUTestExt/MemoryReportAllocator.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MemoryReportAllocator.o src/CppUTestExt/MemoryReportAllocator.cpp

${OBJECTDIR}/src/CppUTestExt/MemoryReportFormatter.o: src/CppUTestExt/MemoryReportFormatter.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MemoryReportFormatter.o src/CppUTestExt/MemoryReportFormatter.cpp

${OBJECTDIR}/src/CppUTestExt/MemoryReporterPlugin.o: src/CppUTestExt/MemoryReporterPlugin.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MemoryReporterPlugin.o src/CppUTestExt/MemoryReporterPlugin.cpp

${OBJECTDIR}/src/CppUTestExt/MockActualCall.o: src/CppUTestExt/MockActualCall.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockActualCall.o src/CppUTestExt/MockActualCall.cpp

${OBJECTDIR}/src/CppUTestExt/MockExpectedCall.o: src/CppUTestExt/MockExpectedCall.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockExpectedCall.o src/CppUTestExt/MockExpectedCall.cpp

${OBJECTDIR}/src/CppUTestExt/MockExpectedCallsList.o: src/CppUTestExt/MockExpectedCallsList.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockExpectedCallsList.o src/CppUTestExt/MockExpectedCallsList.cpp

${OBJECTDIR}/src/CppUTestExt/MockFailure.o: src/CppUTestExt/MockFailure.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockFailure.o src/CppUTestExt/MockFailure.cpp

${OBJECTDIR}/src/CppUTestExt/MockNamedValue.o: src/CppUTestExt/MockNamedValue.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockNamedValue.o src/CppUTestExt/MockNamedValue.cpp

${OBJECTDIR}/src/CppUTestExt/MockSupport.o: src/CppUTestExt/MockSupport.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockSupport.o src/CppUTestExt/MockSupport.cpp

${OBJECTDIR}/src/CppUTestExt/MockSupportPlugin.o: src/CppUTestExt/MockSupportPlugin.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockSupportPlugin.o src/CppUTestExt/MockSupportPlugin.cpp

${OBJECTDIR}/src/CppUTestExt/MockSupport_c.o: src/CppUTestExt/MockSupport_c.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/MockSupport_c.o src/CppUTestExt/MockSupport_c.cpp

${OBJECTDIR}/src/CppUTestExt/OrderedTest.o: src/CppUTestExt/OrderedTest.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/CppUTestExt
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/CppUTestExt/OrderedTest.o src/CppUTestExt/OrderedTest.cpp

${OBJECTDIR}/src/Platforms/Gcc/UtestPlatform.o: src/Platforms/Gcc/UtestPlatform.cpp 
	${MKDIR} -p ${OBJECTDIR}/src/Platforms/Gcc
	${RM} "$@.d"
	$(COMPILE.cc) -Iinclude -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Platforms/Gcc/UtestPlatform.o src/Platforms/Gcc/UtestPlatform.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/libcpputest-3.7.2.a

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
