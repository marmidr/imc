# IMC - Inter Module Communication protocol

![PL](pl.png)
Prosty protokół komunikacyjny (ale nie aż TAK prosty) zaprojektowany do wykorzystania w urządzeniach własnej konstrukcji opartych o niewielkie mikrokontrolery.

![EN](gb.png)
Simple communication protocol (but not THAT simple) designed for use in devices of your own design, based on small microcontroller.

***

## Data flow scenarios


    Normal scenario: each Write/Request is followed directly       In this scenario B respond is delayed;
    by ACK and Response.                                           after ACK received, A is free to send another request.


       +------------+                 +-----------+                   +------------+                 +-----------+
       |            |                 |           |                   |            |                 |           |
       |  Device A  |                 | Device B  |                   |  Device A  |                 | Device B  |
       |            |                 |           |                   |            |                 |           |
       +------+-----+                 +-----+-----+                   +------+-----+                 +-----+-----+
              |                             |                                |                             |
              |                             |                                |                             |  B state:
              |                             |                                |                             |
              |  Write [0]                  |                   Waiting      |  Request [0]                |  Put request [0]
              +---------------------------> |                   ACK          +---------------------------> |  on queue
              |                             |                                |                             |
              |                    ACK [0]  |                   Idle :       |                     ACK [0] |
              | <---------------------------+                   free to send | <---------------------------+
              |                             |                                |                             |
              |                             |                                |  Request [1]                |  Put request [1]
              |                             |                                +---------------------------> |  on queue
              |  Write [1]                  |                                |                             |
              +---------------------> X     |                                |                             |
    ACK       |                             |                                |                             |  Device is busy doing
    timeout   |  Write [1]   retry          |                                |    ... after some time:     |  more important things
    +-------> +---------------------------> |                                |                             |
              |                             |                                |                             |
              |                    ACK [1]  |                                |                             |
              | <---------------------------+                                |                Response [0] |  Process queue
              |                             |                                | <---------------------------+  request [0]
              |                             |                                |                             |
              |                             |                                |                             |  Wait for ACK
              |  Request [2]                |                                |  ACK [0]                    |
              +---------------------------> |                                +---------------------------> |  Free to write
              |                             |                                |                             |
              |                    ACK [2]  |                                |                Response [1] |  Process queue
              | <---------------------------+                                | <---------------------------+  request [1]
              |                             |                                |                             |
              |               Response [2]  |                                |                             |  Wait for ACK
              | <---------------------------+                                |  ACK [1]                    |
              |                             |                                +---------------------------> |  Idle
              |  ACK [2]                    |                                |                             |
              +---------------------------> |                                |                             |
              |                             |                                |                             |



![PL](pl.png)
## Główne cechy

* Niezależny od sprzętu. Moduł przyjmuje odebrane dane po jednym bajcie za pomocą jednej funkcji, wykonując przy tym synchronizację (rozpoznawanie początku ramki). Przy wysyłce ramka formowana jest w wewnętrznym buforze i gotowa do wysłania przekazywana w całości do funkcji użytkownika.

* Idealny do zastosowań w niewielkich mikrokontrolerach, jako medium można użyć UART, RS232, RS485, TCP.

* Wszystkie przesyłane dane (poza adresami rozgłoszeniowymi - broadcast) są potwierdzane. Nadawca sprawdza status i gdy zmieni się z SENDING na SENT_ACK ma pewność, że dane zostały dostarczone.

* Automatyczne ponowienie w przypadku braku potwierdzenia. Jeśli upłynie określony w konfiguracji czas przewidziany na odebranie potwierdzenia (ACK) od od sterownika docelowego, wysyłka jest ponawiana. Ilość powtórzeń jest konfigurowalna.
Czas liczony jest ilością cyklicznych wywołań przez program użytkownika automatu stanu IMC.

* Adresowanie max 253 urządzeń, obsługa broadcast i multicast

    * broadcast: dane wysyłane pod adres 0 są odbierane przez wszystkie podpięte urządzenia; potwierdzenia nie są wysyłane

    * multicast: sterownik ma określoną listę adresów alternatywnych - dane odbierają wszystkie urządzenia z tym adresem na wskazanej liście adresów multicast; potwierdzenia nie są wysyłane

* Maksymalnie 4095 bajtów danych w ramce. Zależnie od potrzeb ilość danych, a więc ilość wymaganej do działania pamięci RAM, może być konfigurowana od 0 (wysyłamy tylko 1-bajtowe polecenie) od 4095 bajtów

* Mechanizmy zapobiegające zawieszaniu się w przypadku błędów transmisji. Cyklicznie wywoływany przez program użytkownika automat stanów IMC monitoruje stan pracy (odbieranie/wysyłanie/oczekiwanie na potwierdzenie) i po upłynięciu limitu czasu na odebranie kolejnego bajtu danych lub potwierdzenia od urządzenia Slave proces zostaje anulowany, a IMC znów może odbierać/nadawać

* Kontrola poprawności danych – każda ramka zawiera prostą sumę kontrolną lub CRC16

* Informacja o stanie IMC (wolny, odbiór, wysyłka)

* Status wysłanych danych (wysyłanie, wysłane-potwierdzone, wysłane-brak potwierdzenia)

* Wsparcie dla DMA przy transmisji (IMC formuje kompletną ramkę do transmisji w wewnętrznym buforze i przekazuje dane do zewnętrznej funkcji, implementowanej przez użytkownika)

* Konfigurowalna ilość buforów ramek odbiorczych – żadne dane nie przepadną jeśli sterownik nie może natychmiastowo przetworzyć odebranej ramki

* Statystyki – zliczanie ilości ramek odebranych, wysłanych, wysłanych potwierdzonych, błędów ramki i sumy kontrolnej, przepełnień bufora

* Niewielki rozmiar – można bez problemu wykorzystać w układzie opartym np. na ATMega8 (8kB Flash, 1kB RAM), po skonfigurowanie tylko-do-odbioru rozmiar będzie jeszcze mniejszy

* Każda ramka ma 4-bitowy znacznik TAG, przekazywany podczas nadawania danych lub wewnętrzny, inkrementowany z każdą transmisją. TAG ułatwia sparowanie wysłanej ramki z odpowiedzią odebraną asynchronicznie (ramka odpowiedzi powtarza wartość tag z zapytania/polecenia)

* Podejście obiektowe: biblioteka IMC pracuje na zmiennej T_IMC_Ctx (kontekst), co oznacza że można mieć kilka niezależnie działających instancji IMC obsługujących kilka portów komunikacyjnych.

## Użycie

Pakiet zawiera wiele plików, w tym kod pomocniczy jak `rprintf`, `trace`, a także biblioteka testów jednostkowych `CppUTest` wraz z zestawem testów.
Aby użyć IMC w swoim programie konieczne są:

* imc.c, imc.h - biblioteka

* imc_cfg.h - nagłówek z konfiguracją

* types.h - nagłówki, typy danych, makra

* crc16.c, crc16.h - obliczanie sumy kontrolnej CRC16, jeśli tak został skonfigurowany IMC (domyślnie)


```C

#include "imc.h"

// context IMC - structure holding current state and I/O buffers
T_IMC_Ctx  IMC_Ctx;

// user function sending frames to be transmitted by IMC
void IMC_WriteBuffer(uint8_t *pData, uint16_t DataSize)
{
    // write data to UART
    // UART2_Write(pData, DataSize);

    // inform state machine that data has been sent, so it can wait for acknowledge
    // (if broadcast or multicast address, IMC switch to idle state)
    IMC_OnDataSent();
}

// example timer IRQ
void TIMER1_INT(void)
{
    // mandatory call of state machine
	// interval should be IMC_SM_INTERVAL(baudrate) [ms].
    IMC_ProcessStateMachine();
}

// example UART IRQ
void UART2_INT(void)
{
    // byte received -> push it to IMC
    char rx = 0; //UART2RX;
    bool er = false; //UART2ERFLAG;

    if (er) // frame error detected?
        IMC_OnRxError();
    else
        IMC_OnRxData(rx);
}

// try to get received frame from the buffer
static void ProcessIMC(void)
{
    // helper structure holding all fields
    T_IMC_Io imc_io;

    // check if any frame is in the receiving buffer
    if (!IMC_Read2(&imc_io))
        return;

    if (imc_io.DataType == IMC_DT_TERMINAL) // && (imc_io.Addr == IMC_HMIADDR)
    {
        //printf("Device status:\n%s", (const char*)imc_io.pData);
    }

    IMC_ReadEnd();
}

void main()
{
    // initialize UART, Timers etc.

    // initialize IMC
    IMC_Init(&IMC_Ctx);    // initialize context buffer IMC
    IMC_Select(&IMC_Ctx);  // select this context as current
    IMC_SetAddress(12);    // set device address for this context: 12

    for (;;)
    {
        // process received frame:
        ProcessIMC();

        // if in idle state:
        if (IMC_GetState() <= IMC_STAT_IDLE)
        {
            // send command (request)
            IMC_SetTxSize(0);
            IMC_AddTxString("status?\n");
            T_IMC_WriteRes rc = IMC_Write(IMC_HMIADDR, false, IMC_DT_TERMINAL, NULL, 0, NULL);

            if (rc == IMC_WRITE_OK)
            {
                // WaitMs(500);
            }
        }
    }
}

```

## Przykłady
Przykładowe programy demo_* obrazują sposób użycia.
Program demo_hmi_registers.c to rozbudowany przykład komunikacji ze sterownikiem MR-HMI3 gdzie ma miejsce odczyt/zapis rejestrów oraz sprawdzanie stanu urządzenia.


***

![EN](gb.png)

## Main features

* Hardware independent. Module accepts received data byte-by-byte in call of single function, performing synchronization. During transmission, the frame is formed in internal buffer and ready to send passed to the user function

* Perfect to use in small microcontrollers where as a medium you can use USART, RS0232, RS-485, Bluetooth or even TCP

* Every data frame (except broadcast and multicast addresses) is confirmed. When sending status change from SENDING to SENT_ACK, sender knows that frame has been delivered

* Auto repetition in case of lack of acknowledge. When specified amount of time passed and no ACK frame has been received, transmitter sends the frame again. The number of retransmissions is configurable. During sending and waiting for ACK the transmitter is in BUSY state.

* Addressing of 253 slave devices plus broadcast and multicast addressing modes:

    * broadcast: frames sent to device address '0' are received by all slave devices; the ACK frame is not send

    * multicast: slave may have specified list of max 254 addresses; data is received if the address is on the list; the ACK frame is not send

* Maximum 4095 bytes in data frame. Depending on needs, the amount of data in frame can be changed. This influences required RAM for transmit and receive buffers. If frame is bigger than slave can accept, it is ignored.

* State machine checking receive and acknowledge time outs. It also controls when to transmit data again in case of missing ACK

* Integrity control - each frame contains simple RLC or CRC16 checksum (configurable)

* Current state information (idle, receiving, sending, sent-ack, sent-nack)

* Configurable number of buffers for RX frames - if device cannot process commands immediately, IMC can hold number of received (and confirmed) frames until it will be free

* Statistics: counting of frames received, transmitted, transmitted-acknowledged, frame errors, buffer overrun

* Every frame contains 4-bit Tag, incremented with each transmission. Tag helps to pair request frame with response frame.

* Context-based approach - IMC works on T_IMC_Ctx structure that contains all buffers and state machine fields, so many I/O ports can be handled using number of IMC context.

## Usage

Package contains many files, beside the IMC code also unit test and `CppUTest` library.
What you need is:

* imc.c, imc.h - IMC itself

* imc_cfg.h - IMC configuration

* types.h - data types, macros

* crc16.c, crc16.h - CRC16 routine, required only if this kind of checksum is used
