/*******************************************************************************
 * \file    demo_minimal_master.c
 *
 * \brief   Demonstracja użycia modułu IMC do wymiany danych.
 *          Wersja dla urządzenia MASTER: wysyła tekst polecenia
 *
 * \author  Mariusz Midor
 *          https://bitbucket.org/mmidor/imc
 *
 ******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// załączniki
#include "imc.h"
#include "trace.h"

///////////////////////////////////////////////////////////////////////////////
// definicje preprocesora

// adres sterownika HMI
#define IMC_HMIADDR        10

///////////////////////////////////////////////////////////////////////////////
// typy

///////////////////////////////////////////////////////////////////////////////
// zmienne

/** struktura - kontekst IMC (można użyć dowolnie wielu, po jednej dla każdego portu) */
T_IMC_Ctx  IMC_Ctx;

///////////////////////////////////////////////////////////////////////////////
// implementacja

// przykładowa funkcja przerwania od timera sprzętowego lub programowego
void TIMER1_INT(void)
{
    // wywołanie co 1-10 ms (zalezy od baudrate) - obowiązkowy automat stanów IMC, musi być w przerwaniu
    // inaczej kod 'while (IMC_GetState() > IMC_STAT_IDLE);' nigdy się nie zakończy
    IMC_ProcessStateMachine();
}

// -----------------------------------------------------------------------------

// przykładowa funkcja obsługi przerwania portu UART
void UART2_INT(void)
{
    // odebrano dane z UART -> wrzucamy bajt do IMC
    char rx = 0; //UART2RX;
    bool er = false; //UART2ERFLAG;

    if (er) // wykryto błąd parzystości?
        IMC_OnRxError();
    else
        IMC_OnRxData(rx);
}

// -----------------------------------------------------------------------------

// inicjalizacja urządzenia i komunikacji
static void Initialize(void)
{
    // TIMER1_Setup(1000, TIMER1_INT);
    // TIMER1_Enable();
    // UART1_Setup(115200, 8, 1, NULL);

    IMC_Init(&IMC_Ctx);     // inicjalizacja kontekstu IMC
    IMC_Select(&IMC_Ctx);   // wybieramy jako aktywny - od tej pory to na nim moduł operuje
    IMC_SetAddress(1);      // nasz adres podstawowy: 1 (1 to zwykle komputer)
    // od tej pory można pracować na IMC: sprawdzać status, wysyłać dane, odczytywać
}

// -----------------------------------------------------------------------------

// próba pobrania odebranej ramki z bufora IMC i jej obsługa
static void ProcessIMC(void)
{
    // struktura ułatwiająca pobieranie ramek z bufora odbiorczego IMC
    T_IMC_Rd imc_rd;

    // sprawdź czy w buforze IMC jest jakaś odebrana ramka
    if (!IMC_Read2(&imc_rd))
        return;

    if (imc_rd.DataType == IMC_DT_TERMINAL) // && (imc_rd.Addr == IMC_HMIADDR)
    {
        //printf("Device status:\n%s", (const char*)imc_rd.pData);
    }

    IMC_ReadEnd();
}

// -----------------------------------------------------------------------------

int main(void)
{
    Initialize();

    for (;;)
    {
        // sprawdzenie czy jest kompletna odebrana ramka IMC
        ProcessIMC();

        // jeśli teraz nie trwa wysyłka lub odbieranie:
        if (IMC_GetState() <= IMC_STAT_IDLE)
        {
            // wysłanie polecenia
            IMC_NewTxPacket();
            IMC_AddTxString("status?\n");
            T_IMC_WriteRes rc = IMC_Write(IMC_HMIADDR, false, IMC_DT_TERMINAL, NULL, 0, NULL);
            if (rc == IMC_WRITE_OK)
            {
            // WaitMs(500);
            }
        }
    }
}

// -----------------------------------------------------------------------------

