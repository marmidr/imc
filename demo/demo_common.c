/*******************************************************************************
 * \file    demo_common.c
 *
 * \author  Mariusz Midor
 *          https://bitbucket.org/mmidor/imc
 *
 ******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// załączniki
#include "imc.h"
#include "crc16.h"
#include "trace.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

///////////////////////////////////////////////////////////////////////////////
// definicje preprocesora

///////////////////////////////////////////////////////////////////////////////
// typy

///////////////////////////////////////////////////////////////////////////////
// zmienne

///////////////////////////////////////////////////////////////////////////////
// implementacja

// wyjście makr TRACE_
void TRACE_PutChar(char C)
{
    // wysłanie znaku terminala przez UART
    // UART1_WriteChar(C);
}

// używane tylko gdy teksty mają być zapisywane do pliku; zależy od TRACE_LogLevel
void TRACE_OpenLog(const char *Path)
{}

void TRACE_CloseLog(void)
{}

void TRACE_FlushLog(void)
{}

// -----------------------------------------------------------------------------

// funkcja wysyłająca uformowane ramki IMC
void IMC_WriteBuffer(uint8_t *pData, uint16_t DataSize)
{
    // wysłanie danych przez UART
    // UART2_Write(pData, DataSize);

    // poinformowanie automatu stanów że wysłano i może czekać na potwierdzenie
    // (jesli adres odbiorcy broadcastowy (0) to IMC nie czeka na potwierdzenie
    // i odrazu przechodzi w gotowość)
    IMC_OnDataSent();
}

// używana tylko gdy IMC_CONFIG_USE_EVENTS == true
// __attribute__((weak, used))
void IMC_Event(T_IMC_Events Event, T_IMC_EvFrameType FrameType, uint8_t Src, uint8_t Dst,
    const uint8_t *pData, uint16_t DataSize, uint8_t FrameTag, T_IMC_DataType Dt)
{
}

void IMC_CalcCRC16(uint16_t *pCRC16, const void *pData, uint32_t DataSize)
{
    CRC16_Compute(pCRC16, pData, DataSize);
}

void IMC_AssertFailed(const char *expression, const char *file, unsigned int line)
{
    fprintf(stderr, "Assert failed: %s @ %s:%u\n", expression, file, line);
    IMC_Event(IMC_EVNT_RX_FRAME, IMC_EVNT_FT_DUMMY, 0, 0, NULL, 0, 0, IMC_DT_NONE);
    abort();
}

void IMC_Trace(const char *Fmt, ...)
{
    va_list ap;
    va_start(ap, Fmt);
    vprintf(Fmt, ap);
    va_end(ap);
}

// -----------------------------------------------------------------------------

