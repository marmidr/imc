/*******************************************************************************
 * \file    demo_hmi_registers.c
 *
 * \brief   Demonstracja użycia modułu IMC do wymiany danych.
 *          Wersja dla urządzenia MASTER: pyta o status sterownika
 *          oraz odczytuje/zapisuje rejestry HMI
 *
 * \author  Mariusz Midor
 *          https://bitbucket.org/mmidor/imc
 *
 ******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
// załączniki

#include "imc.h"
#include "trace.h"
#include "gol_hmi_io.h"
#include "hmi_regs.h"

///////////////////////////////////////////////////////////////////////////////
// definicje preprocesora

// adres sterownika HMI
#define IMC_HMIADDR        10

///////////////////////////////////////////////////////////////////////////////
// typy

/** kody błędów sterownika HMI zwrócone w ramce statusu T_DevStatus */
typedef enum
{
    /** brak błędu */
    ERRC_NONE,
    /** wyjątek procesora - zatrzymany */
    ERRC_FATAL_EXCEPTION,
    /** niewłaściwa wersja projektu */
    ERRC_PROJ_INVALID_VERSION,
    /** plik(i) projektu uszkodzone */
    ERRC_PROJ_CORRUPTED,
    /** brak/niekompletny autorun.inf */
    ERRC_AUTORUN_INF_CORRUPTED,
    /** wywołano exit() */
    ERRC_EXIT,
    /** brak wolnej pamięci RAM */
    ERRC_OUT_OF_MEM,
    /** sterownik się zawiesił - brak reakcji przez 5s */
    ERRC_HALTED,
    /** inny błąd */
    ERRC_OTHER = 250,
} T_ErrCode;

/** status urządzenia - o tą wartość pyta sterownik nadrzędny */
typedef struct
{
    union
    {
        uint32_t    __Status;

        struct
        {
            /** kod błędu T_ErrCode */
            uint32_t ErrorCode: 8;
            /** uruchamianie */
            uint32_t Loading: 1;
            /** gotowy do pracy */
            uint32_t Ready: 1;
            /** tryb bezpieczny - serwis */
            uint32_t Safemode: 1;
            /** zmieniła się wartość rejestru HMI */
            uint32_t RegChanged: 1;
            /** polecenie HMI zostało wykonane - detekcja przez porównanie z poprzednią wartością */
            uint32_t RegCmdTag: 4;
            /** projekt został załadowany pomyślnie */
            uint32_t ProjLoaded: 1;
            /** */
    //      uint32 : 15;
        };
    };
    uint16_t    SessionID;
    uint16_t    rsvd;
} T_DevStatus;

/** statystyki sterownika HMI */
typedef struct
{
    /** rozmiar tej struktury */
    uint16_t    Size;
    /** */
    uint16_t    rsvd0;
    /** całkowity przepracowany czas w sec - resetowalny */
    uint32_t    WorkTime;
    /** całkowity przepracowany czas w sec */
    uint32_t    WorkTimeTotal;
    /** czas od włączenia w sec */
    uint32_t    WorkTimePowered;
    /** ilość wszystkich uruchomień - resetowalny */
    uint16_t    BootCount;
    /** ilość wszystkich uruchomień */
    uint16_t    BootCountTotal;
    /** ilość pełnych uruchomień */
    uint16_t    BootCountSuccess;
    /** użycie pamięci */
    struct
    {
        uint32_t    Total;
        uint32_t    Used;
        uint32_t    UsedMax;
    } Mem;
    /** kolejki zdarzeń */
    struct
    {
        uint16_t    InCount;
        uint16_t    InCountMax;
        uint16_t    OutCount;
        uint16_t    OutCountMax;
    } Que;
    /** statystyka obiektów */
    struct
    {
        int32_t Count;
        int32_t CountMax;
        int32_t Created;
        int32_t Mem;
        int32_t MemMax;
    } Obj;
} T_DevStats;

///////////////////////////////////////////////////////////////////////////////
// zmienne

/** struktura ułatwiająca pobieranie ramek z bufora odbiorczego IMC */
T_IMC_Rd    IMC_RdData;
/** struktura - kontext IMC (można użyć dowolnie wielu, po jednej dla każdego portu) */
T_IMC_Ctx   IMC_Ctx;
/** odbieranie tekstowego - wstrzymujemy inną komunikację */
bool        ReceivingTextReg;

///////////////////////////////////////////////////////////////////////////////
// implementacja

// opcjonalna - dzięki niej można monitorować wymianę danych,
// używana tylko gdy IMC_CONFIG_USE_EVENTS == true
#if 0
void IMC_Event(T_IMC_Events Event, T_IMC_EvFrameType FrameType, uint8_t Src, uint8_t Dst,
    const uint8_t *pData, uint16_t DataSize, uint8_t FrameTag, T_IMC_DataType Dt)
{
    const char *event_names[] =
    {
        "RX",
        "TX",
        "RX_INCOMPL",
        "RX_ERROR",
        "RX_CRCERR",
        "TX_RETRANS",
        "TX_NACK",
        "MSG",
        "RX_DROPPED",
    };

    const char *frame_names[] =
    {
        "ECHO ",
        "ACK  ",
        "DATA ",
        "TOKEN",
        "SLAVE"
    };

    const char *dt_names[] =
    {
        "-       ",
        "FIRMWARE",
        "DISK_IO ",
        "FILE_IO ",
        "REG_IO  ",
        "RSVD5   ",
        "TERMINAL",
        "STATUS  ",
        "DEBUG   ",
    };

    const char frame_tags[] = "0123456789ABCDEF";
    static volatile bool lock = false;
    static int8_t last_tag = -1;

    // -----

    if (Event == IMC_EVNT_MSG)
        TRACE_DEBUG_NL("IMC: %s", pData);

    if (lock)
        return;
    lock = true;

    if (last_tag != FrameTag)
    {
        // oddzielamy pakiety po TAGu dla lepszej czytelności w terminalu
        switch (Event)
        {
            case IMC_EVNT_RX_FRAME:
            case IMC_EVNT_TX_FRAME:
                last_tag = FrameTag;
                TRACE_PRINT_NL("");
                break;

            default:
                break;
        }
    }

    switch (Event)
    {
        case IMC_EVNT_RX_FRAME:
        case IMC_EVNT_RX_CHSUM_ERROR:
            TRACE_DEBUG_NL("IMC: %s [%2u->  ] [%c: %s %s] Size=%4u B",
                event_names[Event],
                Src,
                frame_tags[FrameTag],
                frame_names[(FrameType-1) >> 4],
                dt_names[Dt],
                DataSize
            );

            // if (FrameType == IMC_EVNT_FT_DATA && Dt == IMC_DT_TEMINAL)
            //     TRACE_PRINT_NL("'%s'", pData);
            break;

        case IMC_EVNT_TX_FRAME:
        case IMC_EVNT_TX_RETRANSMISSION:
            TRACE_DEBUG_NL("IMC: %s [  ->%-2u] [%c: %s] Size=%4u B",
                event_names[Event],
                Dst,
                frame_tags[FrameTag],
                frame_names[(FrameType-1) >> 4],
                DataSize
            );

            // if (FrameType == IMC_EVNT_FT_DATA && Dt == IMC_DT_TEMINAL)
            //     TRACE(" '%s'", pData);
            break;

        case IMC_EVNT_TX_NACK:
            TRACE_DEBUG_NL("IMC: %s [  ->%-2u] [%c: %s %s]",
                event_names[Event],
                Dst,
                frame_tags[FrameTag],
                frame_names[(FrameType-1) >> 4],
                dt_names[Dt]
            );
            break;

        case IMC_EVNT_RX_DROPPED:
        case IMC_EVNT_RX_ERROR:
            TRACE_DEBUG_NL("IMC: %s  Received=%4u B",
                event_names[Event],
                DataSize
            );
            break;

        default:
            break;
    }

    lock = false;
}
#endif

// -----------------------------------------------------------------------------

// przykładowa funkcja przerwania od timera sprzętowego lub programowego
void TIMER1_INT(void)
{
    // wywołanie co 1ms - obowiązkowy automat stanów IMC, musi być w przerwaniu
    // inaczej kod 'while (IMC_GetState() > IMC_STAT_IDLE);' nigdy się nie zakończy
    IMC_ProcessStateMachine();
}

// -----------------------------------------------------------------------------

// przykładowa funkcja obsługi przerwania portu UART
void UART2_INT(void)
{
    // odebrano dane z UART -> wrzucamy bajt do IMC
    char rx = 0; //UART2RX;
    bool er = false; //UART2ERFLAG;

    if (er) // wykryto błąd parzystości?
        IMC_OnRxError();
    else
        IMC_OnRxData(rx);
}

/******************************************************************************/

// inicjalizacja niskopoziomowa: porty, timery, uart
static void InitLow(void)
{
    // TIMER1_Setup(1000, TIMER1_INT);
    // TIMER1_Enable();
    // UART1_Setup(115200, 8, 1, NULL); // terminal
    // UART2_Setup(460800, 8, 1, UART2_INT); // IMC
}

// -----------------------------------------------------------------------------

// inicjalizacja wyższego poziomu
static void InitHigh(void)
{
    IMC_Init(&IMC_Ctx);         // inicjalizacja kontekstu IMC
    IMC_Select(&IMC_Ctx);       // wybieramy jako aktywny - od tej pory to na nim moduł operuje
    IMC_SetAddress(IMC_HMIADDR);// nasz adres podstawowy: 10
    // od tej pory można pracować na IMC: sprawdzać status, wysyłać dane, odczytywać
}

// -----------------------------------------------------------------------------

// wysyła zapytanie o status sterownika (żeby wiedzieć czy działa,
// czy zmieniły się jakieś rejestry)
static void HMI_GetStatus(void)
{
    IMC_NewTxPacket();

    if (IMC_WRITE_OK != IMC_Write(IMC_HMIADDR, false, IMC_DT_STATUS, NULL, 0, NULL))
        TRACE_ERROR_LOCATION();
}

//------------------------------------------------------------------------------

// czeka na zakończenie transmisji
void IMC_WaitReady()
{
    while (IMC_GetState() > IMC_STAT_IDLE)
        ;
}

//------------------------------------------------------------------------------

// funkcja formująca i wysyłająca ramkę zapytania o rejestr
static void HMI_RegRead(const char *Regname)
{
    T_RegName regname;
    uint16_t regno;

    HMI_REGNAME_ASGN(regname, Regname);

    if (REG_IsNameValid(regname, &regno))
    {
        IMC_NewTxPacket();

        T_RegData rd = { RCMD_READ_REGS, 1 };
        IMC_AddTxData(&rd, sizeof(rd));

        T_RegType rt = REG_GetTypeFromName(regname);
        T_RegID rid = { rt, regno };
        IMC_AddTxData(&rid, sizeof(rid));

        while (IMC_GetState() > IMC_STAT_IDLE)
            ;

        if (IMC_WRITE_OK != IMC_Write(IMC_HMIADDR, false, IMC_DT_REG_IO, NULL, 0, NULL))
            TRACE_ERROR_LOCATION();
    }
    else
    {
        TRACE_ERROR_LOCATION("- register name '%s' is invalid", Regname);
    }
}

//------------------------------------------------------------------------------

// funkcja wysyła pojedynczy rejestr każdego typu; typ i numer są rozpoznawane
// na podstawie podanej nazwy, w przypadku rejestru tekstowego Value jest wkaźnikiem
// na ciąg znaków 'char'
static T_IMC_WriteRes HMI_RegWrite(const char *Regname, ptrdiff_t Value)
{
    T_RegName regname;
    uint16_t regno;

    HMI_REGNAME_ASGN(regname, Regname);

    if (REG_IsNameValid(regname, &regno))
    {
        // skasowanie rozmiaru bufora ramki nadawczej IMC
        IMC_NewTxPacket();

        // dodanie nagłówka danych: polecenie zapisu rejestru
        T_RegData rd = { RCMD_WRITE_REGS, 1 };
        IMC_AddTxData(&rd, sizeof(rd));

        // dodanie do bufora IMC wartości rejestru
        T_RegType rt = REG_GetTypeFromName(regname);
        T_RegVal rv = { rt, regno, 0 };

        switch (rt)
        {
            // liczbowe
            case HMIR_B:
            case HMIR_H:
            case HMIR_R:
            case HMIR_M:
            case HMIR_S:
                rv.Value = Value;
                IMC_AddTxData(&rv, sizeof(rv));
                break;

            // tekstowe
            case HMIR_T:
            case HMIR_U:
                {
                    // wysyłamy krótkie teksty, więc jeden pakiet, flagi FIRST i LAST razem
                    const char *txt = (const char *)Value;
                    // flaga FIRST więc wpisujemy długość tekstu
                    int len = strlen(txt);
                    rv.Value = len;
                    rv.Value |= HMI_REGVAL_STR_FIRST | HMI_REGVAL_STR_LAST;
                    // nagłówek
                    IMC_AddTxData(&rv, sizeof(rv));
                    // tekst, razem z NULL
                    IMC_AddTxData(txt, len+1);
                }
                break;

            // float: Value zawiera wartość float (4 bajty) w zmiennej typu int
            case HMIR_F:
                rv.Value = Value;
                IMC_AddTxData(&rv, sizeof(rv));
                break;

            default:
                break;
        }

        return IMC_Write(IMC_HMIADDR, false, IMC_DT_REG_IO, NULL, 0, NULL);
    }
    TRACE_ERROR_LOCATION("- register name '%s' is invalid", Regname);
    return IMC_WRITE_DATA_ERROR;
}

//------------------------------------------------------------------------------

// zapis wielu rejestrów na raz; wszystkie oprócz tekstowych
static T_IMC_WriteRes HMI_RegWriteMulti(const T_RegVal *pRegs)
{
    // zliczenie rejestrów przez wykrycie wpisu-terminatora
    uint8_t nregs = 0;
    while (pRegs[nregs].RegType != HMIR_INVALID)
        nregs++;

    IMC_NewTxPacket();
    T_RegData rd = { RCMD_WRITE_REGS, nregs };
    IMC_AddTxData(&rd, sizeof(rd));
    IMC_AddTxData(pRegs, sizeof(T_RegVal) * nregs);
    return IMC_Write(IMC_HMIADDR, false, IMC_DT_REG_IO, NULL, 0, NULL);
}

//------------------------------------------------------------------------------

// odczyt wielu rejestrów na raz; wszystkie oprócz tekstowych
static T_IMC_WriteRes HMI_RegReadMulti(const T_RegID *pRegID)
{
    // zliczenie rejestrów przez wykrycie wpisu-terminatora
    uint8_t nregs = 0;
    while (pRegID[nregs].RegType != HMIR_INVALID)
        nregs++;

    IMC_NewTxPacket();
    T_RegData rd = { RCMD_READ_REGS, nregs };
    IMC_AddTxData(&rd, sizeof(rd));
    IMC_AddTxData(pRegID, sizeof(T_RegID) * nregs);
    return IMC_Write(IMC_HMIADDR, false, IMC_DT_REG_IO, NULL, 0, NULL);
}

//------------------------------------------------------------------------------

static T_IMC_WriteRes HMI_RegWriteLongString(const char *String)
{
    const size_t MAX_TEXT_IN_PACKET = 10; // do testów mniej niż limit: HMI_REGVAL_STR_MAXLEN
    const T_RegData rd = { RCMD_WRITE_REGS, 1 };
    T_RegVal rv = { HMIR_T, 21, 0 };
    const char *data = String;
    size_t data_len = strlen(String);

    // pierwsza paczka: tylko długość
    rv.Value = data_len;
    rv.Value |= HMI_REGVAL_STR_FIRST;
    IMC_WaitReady();
    IMC_NewTxPacket();
    IMC_AddTxData(&rd, sizeof(rd));
    IMC_AddTxData(&rv, sizeof(rv));
    T_IMC_WriteRes iwr = IMC_Write(IMC_HMIADDR, false, IMC_DT_REG_IO, NULL, 0, NULL);

    if (IMC_WRITE_OK != iwr)
    {
        TRACE_ERROR_LOCATION();
        return iwr;
    }

    // w kolejnych treść
    while (data_len > 0)
    {
        int tosend = data_len > MAX_TEXT_IN_PACKET ? MAX_TEXT_IN_PACKET : data_len;
        data_len -= tosend;
        rv.Value = tosend;

        if (data_len == 0)
            rv.Value |= HMI_REGVAL_STR_LAST;

        IMC_WaitReady();
        IMC_NewTxPacket();
        IMC_AddTxData(&rd, sizeof(rd));
        IMC_AddTxData(&rv, sizeof(rv));
        IMC_AddTxData(data, tosend);
        IMC_AddTxData("\0", 1);
        iwr = IMC_Write(IMC_HMIADDR, false, IMC_DT_REG_IO, NULL, 0, NULL);
        data += tosend;

        if (IMC_WRITE_OK != iwr)
        {
            TRACE_ERROR_LOCATION();
            return iwr;
        }
    }

    return iwr;
}

//------------------------------------------------------------------------------

// obsługa odebranej ramki z rejestrami
static void ProcessIMC_Reg(void)
{
    static char txreg_value[1000]; // bufor na odbierany rejestr tekstowy (może być wysyłany partiami)
    T_RegData *p_rd = (T_RegData*)IMC_RdData.pData;

    // odpowiedź na RCMD_READ_REGS lub lista zmienionych rejestrów
    if (p_rd->Command == RCMD_WRITE_REGS)
    {
        for (uint16_t i = 0; i < p_rd->Count; i++)
        {
            T_RegVal *p_rv = &p_rd->Regs[i];

            switch (p_rv->RegType)
            {
                case HMIR_B:
                case HMIR_H:
                case HMIR_R:
                case HMIR_M:
                case HMIR_S:
                    // wartość rejestru liczbowego na terminal
                    TRACE_INFO_NL("%%%c%u=%u",
                        HMI_REGNAMES[p_rv->RegType],
                        p_rv->RegNo, p_rv->Value);
                    break;

                case HMIR_T:
                case HMIR_U:
                    // tekst rejestru na terminal
                    if (p_rv->Value & HMI_REGVAL_STR_FIRST)
                    {
                        // informacja o długości
                        TRACE_INFO_NL("%%%c%u=%u bytes",
                            HMI_REGNAMES[p_rv->RegType],
                            p_rv->RegNo, p_rv->Value & HMI_REGVAL_STR_MASK);
                    }

                    // flaga odbierania tekstu jest kasowana odebranym bitem HMI_REGVAL_STR_LAST
                    ReceivingTextReg = (p_rv->Value & HMI_REGVAL_STR_LAST) == 0;

                    // znaczniki HMI_REGVAL_STR_FIRST i HMI_REGVAL_STR_LAST mogą wystąpić razem
                    {
                        if (p_rv->Value & HMI_REGVAL_STR_FIRST)
                            strcpy(txreg_value, p_rv->String);
                        else
                            strcat(txreg_value, p_rv->String);
                    }

                    // ostatnia część - można wypisać bufor na terminal
                    if (p_rv->Value & HMI_REGVAL_STR_LAST)
                        TRACE_INFO_NL("%%%c%u='%s'", txreg_value);
                    break;

                case HMIR_F:
                    {
                        // wartość float przesyłana w 4 bajtach int ->
                        // trzeba ją wyciągnąć za pomocą unii
                        T_Int32 r;
                        r.i32 = p_rv->Value;
                        TRACE_INFO_NL("%%%c%u=%f",
                            HMI_REGNAMES[p_rv->RegType], p_rv->RegNo, r.f32);
                    }
                    break;

                default:
                    break;
            }
        }
    }
}

// -----------------------------------------------------------------------------
// próba pobrania odebranej ramki z bufora IMC i jej obsługa
static void ProcessIMC(void)
{
    // sprawdź czy w buforze IMC jest jakaś odebrana ramka
    if (!IMC_Read2(&IMC_RdData))
        return;

    // sprawdzamy typ danych:
    switch (IMC_RdData.DataType)
    {
        case IMC_DT_REG_IO:
            // obsłużenie rejestrów
            ProcessIMC_Reg();
            // koniec odczytu -> zwalniamy bufor odbiorczy IMC
            IMC_ReadEnd();
            break;

        case IMC_DT_TERMINAL:
            // odebrano polecenie terminala
            TRACE_DEBUG_NL("TERM> %s", (char*)IMC_RdData.pData);
            IMC_ReadEnd();
            break;

        case IMC_DT_STATUS:
            {
                // odebrano informację o statusie sterownika
                T_DevStatus *p_stat = (T_DevStatus*)IMC_RdData.pData;

                // status wskazuje że pewne rejestry monitorowane zmieniły wartość,
                // więc trzeba o nie zapytać:
                if (p_stat->RegChanged)
                {
                    // wysylamy pytanie o listę zmienionych rejestrów
                    T_RegData rd;
                    rd.Command = RCMD_GET_CHANGED_VALUES;
                    rd.Count = 20; // wyślij max 20 w paczce

                    // czekaj aż IMC będzie wolny (może wciąż wysyłać potwierdzenie
                    // odebrania ramki którą przetwarzamy)
                    while (IMC_GetState() > IMC_STAT_IDLE)
                        ;

                    if (IMC_WRITE_OK != IMC_Write(IMC_RdData.Addr, false,
                        IMC_DT_REG_IO, &rd, sizeof(rd), NULL))
                    {
                        TRACE_ERROR_LOCATION();
                    }
                }
                // koniec odczytu - zwalniamy bufor odbiorczy IMC
                IMC_ReadEnd();
            }
            break;

        default:
            IMC_ReadEnd();
            break;
    }
}

// -----------------------------------------------------------------------------

static const char* long_text =
    "Alfa Romeo Automobiles S.p.A. (Italian pronunciation: [ˈalfa roˈmɛːo]) is a car manufacturer, founded by Frenchman Alexandre Darracq as A.L.F.A. ('[Società] Anonima Lombarda Fabbrica Automobili', 'Lombard Automobile Factory Company') on 24 June 1910, in Milan.[3] The brand is known for sporty vehicles and has been involved in car racing since 1911."
    "The company was owned by Italian state holding company Istituto per la Ricostruzione Industriale between 1932 and 1986, when it became a part of the Fiat group.[4] In February 2007, the Alfa Romeo brand became Alfa Romeo Automobiles S.p.A., a subsidiary of Fiat Group Automobiles, now Fiat Chrysler Automobiles Italy."
    "The company that became Alfa Romeo was founded as Società Anonima Italiana Darracq (SAID) in 1906 by the French automobile firm of Alexandre Darracq, with Italian investors. In late 1909, the Italian Darracq cars were selling slowly and the Italian partners of the company hired Giuseppe Merosi to design new cars. On 24 June 1910, a new company was founded named A.L.F.A., initially still in partnership with Darracq. The first non-Darracq car produced by the company was the 1910 24 HP, designed by Merosi. A.L.F.A. ventured into motor racing, with drivers Franchini and Ronzoni competing in the 1911 Targa Florio with two 24-hp models. In August 1915, the company came under the direction of Neapolitan entrepreneur Nicola Romeo, who converted the factory to produce military hardware for the Italian and Allied war efforts. In 1920, the name of the company was changed to Alfa Romeo with the Torpedo 20-30 HP the first car to be so badged."
    "In 1921, the Banca Italiana di Sconto, which backed the Ing. Nicola Romeo & Co, went broke and the government needed to support the industrial companies involved, among which was Alfa Romeo, through the 'Consorzio per Sovvenzioni sui Valori Industriali'. In 1925, the railway activities were separated from the Romeo company, and in 1928, Nicola Romeo left. In 1933, the state ownership was reorganized under the banner of the Istituto per la Ricostruzione Industriale (IRI) by Benito Mussolini's government, which then had effective control. The company struggled to return to profitability after the Second World War, and turned to mass-producing small vehicles rather than hand-building luxury models. In 1954, it developed the Alfa Romeo Twin Cam engine, which would remain in production until 1994. During the 1960s and 1970s, Alfa Romeo produced a number of sporty cars, though the Italian government parent company, Finmeccanica, struggled to make a profit, so it sold the marque to the Fiat Group in 1986."
    "Alfa Romeo has competed successfully in Grand Prix motor racing, Formula One, sportscar racing, touring car racing, and rallies. It has competed both as a constructor and an engine supplier, via works entries (usually under the name Alfa Corse or Autodelta), and private entries. The first racing car was made in 1913, three years after the foundation of the company, and Alfa Romeo won the inaugural world championship for Grand Prix cars in 1925. The number victories in races until the Second World War, against rivals such as Bentley, and the production of luxury cars compete with Bugatti, have made Alfa Romeo a world motorsport legend. The company gained a good name in motorsport, which gave a sporty image to the whole marque. Enzo Ferrari founded the Scuderia Ferrari racing team in 1929 as an Alfa Romeo racing team, before becoming independent in 1939. It holds the world's title of the most wins of any marque in the world.[5]";

int main(void)
{
    InitLow();
    InitHigh();
    TRACE_DEBUG_NL("R E A D Y");

    for (;;)
    {
        // sprawdzenie czy jest kompletna odebrana ramka IMC
        ProcessIMC();

        // dla ReceivingTextReg trzeba jakiś timeout zamplementować, bo bez tego
        // w razie utraty danych zablokuje się komunikacja
        if (!ReceivingTextReg)
        {
            // IMC wolny, nie odbiera a ostatnie dane zostały wysłane;
            // jeśli status IMC jest >= BUSY, to może się on zmienić tylko w przerwaniu
            // przez wywolanie IMC_ProcessEvent() lub IMC_StateMachineProc()

            // zapytanie o status sterownika HMI, np co 100ms
            IMC_WaitReady();
            HMI_GetStatus();


            // zapytanie o wartość rejestru, odpowiedź obsługiwana w ProcessIMC
            IMC_WaitReady();
            HMI_RegRead("%R25");


            // zapis rejestru bitowego
            IMC_WaitReady();
            HMI_RegWrite("%B1", 1);


            // zapis rejestru tekstowego
            IMC_WaitReady();
            HMI_RegWrite("%T2", (ptrdiff_t)"Komunikat");


            // zapis długiego rejestru tekstowego
            IMC_WaitReady();
            HMI_RegWriteLongString(long_text);


            // zapis na raz kilku rejestrów liczbowych
            if (IMC_GetState() <= IMC_STAT_IDLE)
            {
                T_RegVal reg_val[] =
                {
                    { HMIR_B,  0, true  },
                    { HMIR_H,  1, false },
                    { HMIR_R, 10, 1024  },
                    { HMIR_M, 12, 120   },
                    // terminator
                    { HMIR_INVALID, 0, 0 },
                };

                T_IMC_WriteRes iwr = HMI_RegWriteMulti(reg_val);
                if (IMC_WRITE_OK != iwr)
                    TRACE_ERROR_LOCATION("IMC Write: %s", IMC_GetWriteRes_Str(iwr));
            }


            // żądanie odczytu na raz kilku rejestrów liczbowych
            if (IMC_GetState() <= IMC_STAT_IDLE)
            {
                T_RegID reg_id[] =
                {
                    { HMIR_B, 0 },
                    { HMIR_B, 1 },
                    { HMIR_B, 2 },
                    { HMIR_M, 0 },
                    { HMIR_M, 1 },
                    { HMIR_M, 2 },
                    // terminator
                    { HMIR_INVALID, 0 },
                };

                T_IMC_WriteRes iwr = HMI_RegReadMulti(reg_id);
                if (IMC_WRITE_OK != iwr)
                    TRACE_ERROR_LOCATION("IMC Write: %s", IMC_GetWriteRes_Str(iwr));

                // jeśli zapytanie zostalo wysłane, sterownik odpowie i dane zostaną
                // odebrane w ProcessIMC_Reg()
            }
        }
    }
}

// -----------------------------------------------------------------------------

