/*******************************************************************************
 * \file    trace.c
 *
 * \brief   Terminal printing
 *
 * \author  Mariusz Midor
 *          http://egolhmi.eu
 *
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
/// includes

#include <string.h>
#include "trace.h"
#include "types.h"

////////////////////////////////////////////////////////////////////////////////
/// variables, constants

/** bieżący poziom trace, ustawiany przez makra */
int TRACE_Level;
/** minimalny poziom TRACE logowany do pliku (TRACE_LEVEL_...) */
int TRACE_LogLevel;
/** 0 - logowanie właczone, > 0 - wyłączone */
int TRACE_LogLock;
/** ostatnim znakiem był \n */
int TRACE_LastNL;

////////////////////////////////////////////////////////////////////////////////
/// implementation

void TRACE_Print(short Level, const char *Fmt, ...)
{
    if (Level > TRACE_LEVEL_DEBUG)  Level = TRACE_LEVEL_DEBUG;
    TRACE_Level = Level;

    va_list ap;
    va_start(ap, Fmt);
    vrprintf(TRACE_PutChar, Fmt, ap);
    va_end(ap);

    if (INRANGE(Level, TRACE_LEVEL_FATAL, TRACE_LEVEL_ERROR))
        TRACE_FlushLog();

    TRACE_LastNL = 0;
}

//------------------------------------------------------------------------------

void TRACE_PrintLocation(short Level, const char *Function, int Line, const char *Prefix, const char *Fmt, ...)
{
    if (Level > TRACE_LEVEL_DEBUG)  Level = TRACE_LEVEL_DEBUG;
    TRACE_Level = Level;
    rprintf(TRACE_PutChar, "%s%s:%u ", Prefix, Function, Line);

    va_list ap;
    va_start(ap, Fmt);
    vrprintf(TRACE_PutChar, Fmt, ap);
    va_end(ap);

    if (INRANGE(Level, TRACE_LEVEL_FATAL, TRACE_LEVEL_ERROR))
        TRACE_FlushLog();

    TRACE_LastNL = 0;
}

//------------------------------------------------------------------------------
