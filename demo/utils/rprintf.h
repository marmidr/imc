/*******************************************************************************
 * \file    rprintf.h
 *
 * \brief   Printf functionality
 *
 *******************************************************************************/

/*
   Copyright (c) 2002, Alexander Popov (sasho@vip.bg)
   Copyright (c) 2002,2004,2005 Joerg Wunsch
   Copyright (c) 2005, Helmut Wallner
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _RPRINTF_H_
#define _RPRINTF_H_

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////
/// includes
#include <stdarg.h>

////////////////////////////////////////////////////////////////////////////////
/// preprocessor definitions

/**
 * \name Values for PRINTF_LEVEL
 *
 */
//{@
#define PRINTF_MIN      1
#define PRINTF_STD      2
#define PRINTF_FLT      3
//@}

#ifndef PRINTF_LEVEL
# define PRINTF_LEVEL   PRINTF_FLT
#endif

////////////////////////////////////////////////////////////////////////////////
/// exported functions

/*******************************************************************************
 * \brief   Formatuje tekst, wynik wypisuje znak po znaku do wskazanej funkcji
 * \param   stream - wskaźnik do funkcji przyjmującej jeden parametr typu char
 * \param   fmt - tekst formatujący
 * \param   ... - lista argumentów
 */

void rprintf(void (*stream)(char), const char *fmt, ...);

/*******************************************************************************
 * \brief   Formatuje tekst, wynik wypisuje znak po znaku do wskazanej funkcji
 * \param   stream - wskaźnik do funkcji przyjmującej jeden parametr typu char
 * \param   fmt - tekst formatujący
 * \param   ap - lista argumentów z funkcji va_start()
 */

void vrprintf(void (*stream)(char), const char *fmt, va_list ap);

/*******************************************************************************
 * \brief   Formatuje tekst, wynik wypisuje do wskazanego bufora
 * \param   buffer - bufor na tekst wyjściowy
 * \param   buffsize - rozmiar bufora w bajtach
 * \param   fmt - tekst formatujący
 * \param   ... - lista argumentów
 * \return  Ilość znaków wpisanych do bufora
 */

int snrprintf(char *buffer, int buffsize, const char *fmt, ...);

/*******************************************************************************
 * \brief   Formatuje tekst, wynik wypisuje do wskazanego bufora
 * \param   buffer - bufor na tekst wyjściowy
 * \param   buffsize - rozmiar bufora w bajtach
 * \param   fmt - tekst formatujący
 * \param   ap - lista argumentów z funkcji va_start()
 * \return  Ilość znaków wpisanych do bufora
 */

int snvrprintf(char *buffer, int buffsize, const char *fmt, va_list ap);

//------------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

//------------------------------------------------------------------------------

#endif // _RPRINTF_H_
