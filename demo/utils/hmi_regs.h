/*******************************************************************************
 * \file    hmi_regs.h
 *
 * \brief   Obsługa reejstrów HMI
 *
 * \author  Mariusz Midor
 *          http://egolhmi.eu
 *
 ******************************************************************************/

#ifndef _HMI_REGS_H_
#define _HMI_REGS_H_

/////////////////////////////////////////////////////////////////////////////////
// załączniki
#include <string.h>
#include "types.h"
#include "gol_hmi_io.h"

////////////////////////////////////////////////////////////////////////////////
/// definicje preprocesora
#define HMI_REGNAME_LEN    8
#define HMI_REGS_R       500
#define HMI_REGS_M       100
#define HMI_REGS_B      1000
#define HMI_REGS_H       200
#define HMI_REGS_T       200
#define HMI_REGS_U       200
#define HMI_REGS_F       100
#define HMI_REGS_S        50

#define HMI_REGNAME_CLR(reg)        (reg)[0] = '\0'
#define HMI_REGNAME_ISVALID(reg)    ((reg) && ((reg)[0] == '%'))
#define HMI_REGNAME_ISEQ(r1, r2)    (strncmp((r1), (r2), HMI_REGNAME_LEN) == 0)
#define HMI_REGNAME_ASGN(dst, src)  {strncpy((dst), (src), HMI_REGNAME_LEN); dst[HMI_REGNAME_LEN-1] = '\0';}

////////////////////////////////////////////////////////////////////////////////
// typy
typedef char T_RegName[HMI_REGNAME_LEN];

/////////////////////////////////////////////////////////////////////////////////
// zmienne globalne

/////////////////////////////////////////////////////////////////////////////////
// funkcje globalne

bool REG_IsNameValid(const T_RegName RegName, uint16_t *pRegNo);
T_RegType REG_GetTypeFromName(const T_RegName RegName);

//------------------------------------------------------------------------------

#endif // _HMI_REGS_H_

