/*******************************************************************************
 * \file    trace.h
 *
 * \brief   Terminal printing
 *
 * \author  Mariusz Midor
 *          http://egolhmi.eu
 *
 ******************************************************************************/

/* ----------------------------------------------------------------------------
 *         ATMEL Microcontroller Software Support
 * ----------------------------------------------------------------------------
 * Copyright (c) 2008, Atmel Corporation
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * Atmel's name may not be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

//------------------------------------------------------------------------------
/// \unit
///
/// !Purpose
///
/// Standard output methods for reporting debug information, warnings and
/// errors, which can be easily be turned on/off.
///
/// !Usage
/// -# Initialize the DBGU using TRACE_CONFIGURE() if you intend to eventually
///    disable ALL traces; otherwise use DBGU_Configure().
/// -# Uses the TRACE_DEBUG(), TRACE_INFO(), TRACE_WARNING(), TRACE_ERROR()
///    TRACE_FATAL() macros to output traces throughout the program.
/// -# Each type of trace has a level : Debug 5, Info 4, Warning 3, Error 2
///    and Fatal 1. Disable a group of traces by changing the value of
///    TRACE_LEVEL during compilation; traces with a level bigger than TRACE_LEVEL
///    are not generated. To generate no trace, use the reserved value 0.
/// -# Trace disabling can be static or dynamic. If dynamic disabling is selected
///    the trace level can be modified in runtime. If static disabling is selected
///    the disabled traces are not compiled.
///
/// !Trace level description
/// -# TRACE_DEBUG (5): Traces whose only purpose is for debugging the program,
///    and which do not produce meaningful information otherwise.
/// -# TRACE_INFO (4): Informational trace about the program execution. Should
///    enable the user to see the execution flow.
/// -# TRACE_WARNING (3): Indicates that a minor error has happened. In most case
///    it can be discarded safely; it may even be expected.
/// -# TRACE_ERROR (2): Indicates an error which may not stop the program execution,
///    but which indicates there is a problem with the code.
/// -# TRACE_FATAL (1): Indicates a major error which prevents the program from going
///    any further.
///
///
///     2013-01-18
///         - dodane elastyczne makra TRACE_LOCATION_
///
///     2009-09-04
///         - wersja skrócona
///         - współpraca z rprintf()
///
//------------------------------------------------------------------------------

#ifndef TRACE_H
#define TRACE_H

//------------------------------------------------------------------------------
//         Headers
//------------------------------------------------------------------------------

#include "rprintf.h"

//------------------------------------------------------------------------------
//         Global Definitions
//------------------------------------------------------------------------------

#define TRACE_LEVEL_DEBUG      5
#define TRACE_LEVEL_INFO       4
#define TRACE_LEVEL_WARNING    3
#define TRACE_LEVEL_ERROR      2
#define TRACE_LEVEL_FATAL      1
#define TRACE_LEVEL_NO_TRACE   0

#if !defined(TRACE_LEVEL)
# define TRACE_LEVEL        TRACE_LEVEL_DEBUG
#endif

#ifdef __cplusplus
extern "C" void TRACE_PutChar(char C);
extern "C" void TRACE_OpenLog(const char *Path);
extern "C" void TRACE_CloseLog(void);
extern "C" void TRACE_FlushLog(void);
extern "C" void TRACE_FlushImcBuff(void);

extern "C" void TRACE_Print(short Level, const char *Fmt, ...);
extern "C" void TRACE_PrintLocation(short Level, const char *Function, int Line, const char *Fmt, ...);
#else
extern void TRACE_PutChar(char C);
extern void TRACE_OpenLog(const char *Path);
extern void TRACE_CloseLog(void);
extern void TRACE_FlushLog(void);
extern void TRACE_FlushImcBuff(void);

extern void TRACE_Print(short Level, const char *Fmt, ...);
extern void TRACE_PrintLocation(short Level, const char *Function, int Line, const char *Prefix, const char *Fmt, ...);
#endif

/** bieżący poziom trace, ustawiany przez makra */
extern int  TRACE_Level;
/** minimalny poziom TRACE logowany do pliku (TRACE_LEVEL_...) */
extern int  TRACE_LogLevel;
/** 0 - logowanie właczone, > 0 - wyłączone */
extern int  TRACE_LogLock;
/** ostatnim znakiem był \n */
extern int  TRACE_LastNL;

#define TRACE_PUTCHAR                   TRACE_PutChar
#define TRACE_PRINT_NL(...)             TRACE_Print(TRACE_LogLevel, "\n" __VA_ARGS__)
#define TRACE_PRINT(...)                TRACE_Print(TRACE_LogLevel, __VA_ARGS__)

//------------------------------------------------------------------------------
/// Outputs a formatted string using <printf> if the log level is high
/// enough. Can be disabled by defining TRACE_LEVEL=0 during compilation.
/// \param format  Formatted string to output.
/// \param ...  Additional parameters depending on formatted string.
//------------------------------------------------------------------------------

// poniższe dziedziczą Global.TRACE_Level po poprzednich wywołaniach
#define TRACE_NL(...)                   TRACE_Print(TRACE_LogLevel, "\n""    " __VA_ARGS__)
#define TRACE(...)                      TRACE_Print(TRACE_LogLevel, __VA_ARGS__)

/** Te makra zapisują tekst do pliku logów, niezaleznie od TRACE_LEVEL */
#define TRACE_LOG_NL(...)               TRACE_Print(TRACE_LEVEL_FATAL, "\n""    " __VA_ARGS__)
#define TRACE_LOG(...)                  TRACE_Print(TRACE_LEVEL_FATAL, "    " __VA_ARGS__)

/** Trace compilation depends on TRACE_LEVEL value */
#if (TRACE_LEVEL >= TRACE_LEVEL_DEBUG)
    #define TRACE_DEBUG_NL(...)         TRACE_Print(TRACE_LEVEL_DEBUG, "\n""-D- " __VA_ARGS__)
    #define TRACE_DEBUG(...)            TRACE_Print(TRACE_LEVEL_DEBUG, "-D- " __VA_ARGS__)
    #define TRACE_DEBUG_LOCATION(...)   TRACE_PrintLocation(TRACE_LEVEL_DEBUG, __FUNCTION__, __LINE__, "\n""-D- ", "" __VA_ARGS__)
#else
    #define TRACE_DEBUG_NL(...)         { }
    #define TRACE_DEBUG(...)            { }
    #define TRACE_ERROR_LOCATION(...)   { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_INFO)
    #define TRACE_INFO_NL(...)          TRACE_Print(TRACE_LEVEL_INFO, "\n""-I- " __VA_ARGS__)
    #define TRACE_INFO(...)             TRACE_Print(TRACE_LEVEL_INFO, "-I- " __VA_ARGS__)
    #define TRACE_INFO_LOCATION(...)    TRACE_PrintLocation(TRACE_LEVEL_INFO, __FUNCTION__, __LINE__, "\n""-I- ", "" __VA_ARGS__)
#else
    #define TRACE_INFO_NL(...)          { }
    #define TRACE_INFO(...)             { }
    #define TRACE_INFO_LOCATION(...)    { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_WARNING)
    #define TRACE_WARNING_NL(...)       TRACE_Print(TRACE_LEVEL_WARNING, "\n""-W- " __VA_ARGS__)
    #define TRACE_WARNING(...)          TRACE_Print(TRACE_LEVEL_WARNING, "-W- " __VA_ARGS__)
    #define TRACE_WARNING_LOCATION(...) TRACE_PrintLocation(TRACE_LEVEL_WARNING, __FUNCTION__, __LINE__, "\n""-W- ", "" __VA_ARGS__)
#else
    #define TRACE_WARNING_NL(...)       { }
    #define TRACE_WARNING(...)          { }
    #define TRACE_WARNING_LOCATION(...) { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_ERROR)
    #define TRACE_ERROR_NL(...)         TRACE_Print(TRACE_LEVEL_ERROR, "\n""-E- " __VA_ARGS__)
    #define TRACE_ERROR(...)            TRACE_Print(TRACE_LEVEL_ERROR, "-E- " __VA_ARGS__)
    #define TRACE_ERROR_LOCATION(...)   TRACE_PrintLocation(TRACE_LEVEL_ERROR, __FUNCTION__, __LINE__, "\n""-E- ", "" __VA_ARGS__)
#else
    #define TRACE_ERROR_NL(...)         { }
    #define TRACE_ERROR(...)            { }
    #define TRACE_ERROR_LOCATION(...)   { }
#endif

#define TRACE_FATAL_NL(...)             TRACE_Print(TRACE_LEVEL_FATAL, "\n""-F- " __VA_ARGS__)
#define TRACE_FATAL(...)                TRACE_Print(TRACE_LEVEL_FATAL, "-F- " __VA_ARGS__)
#define TRACE_FATAL_LOCATION(...)       TRACE_PrintLocation(TRACE_LEVEL_FATAL, __FUNCTION__, __LINE__, "\n""-F- ", "" __VA_ARGS__)

//------------------------------------------------------------------------------

// tak się nie da, ponieważ __FUNCTION__ nie jest makrem tylko tablicą char []
//#define TRACE_ERROR_LOCATION(...)     TRACE_ERROR_NL("" __FUNCTION__ ":" STRINGIFY(__LINE__) __VA_ARGS__)
// http://stackoverflow.com/questions/4384765/whats-the-difference-between-pretty-function-function-func
// http://gcc.gnu.org/onlinedocs/gcc-4.5.1/gcc/Function-Names.html#Function-Names

//------------------------------------------------------------------------------

#endif //#ifndef TRACE_H

