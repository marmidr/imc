/* Copyright (c) 2002,2004,2005 Joerg Wunsch
   Copyright (c) 2008  Dmitry Xmelkov
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.

 * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
 */

/* $Id$ */

/*
    pobrano z:
    http://svn.savannah.nongnu.org/viewvc/trunk/avr-libc/libc/stdio/vfscanf.c?root=avr-libc&view=markup

    2012-04-20 Mariusz Midor:
    - przeróbka by używał stringa jako danych wejściowych
    - obsługa typów short i long (był tylko int 16-bitowy)

*/

////////////////////////////////////////////////////////////////////////////////
/// załączniki

#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include "rscanf.h"

////////////////////////////////////////////////////////////////////////////////
/// definicje preprocesora, makra

#if !defined (SCANF_LEVEL)
#ifndef SCANF_WWIDTH        /* use word for width variable  */
#define SCANF_WWIDTH    0
#endif
#ifndef SCANF_BRACKET       /* use '%[' conversion  */
#define SCANF_BRACKET   0
#endif
#ifndef SCANF_FLOAT         /* use float point conversion   */
#define SCANF_FLOAT     0
#endif
#elif   SCANF_LEVEL == SCANF_MIN
#define SCANF_WWIDTH    0
#define SCANF_BRACKET   0
#define SCANF_FLOAT     0
#elif   SCANF_LEVEL == SCANF_STD
#define SCANF_WWIDTH    0
#define SCANF_BRACKET   1
#define SCANF_FLOAT     0
#elif   SCANF_LEVEL == SCANF_FLT
#define SCANF_WWIDTH    1
#define SCANF_BRACKET   1
#define SCANF_FLOAT     1
#else
#error   "Not a known scanf level."
#endif

/* ATTENTION: check FL_CHAR first, not FL_LONG. The last is set
   simultaneously.  */
#define FL_STAR     0x01    /* '*': skip assignment     */
#define FL_WIDTH    0x02    /* width is present         */
#define FL_LONG     0x04    /* 'long' type modifier     */
#define FL_CHAR     0x08    /* 'char' type modifier     */
#define FL_OCT      0x10    /* octal number             */
#define FL_DEC      0x20    /* decimal number           */
#define FL_HEX      0x40    /* hexidecimal number       */
#define FL_MINUS    0x80    /* minus flag (field or value) */
#define FL_SHORT    0x100   /* 'short' type modifier    */
#define FL_LONGLONG 0x200   /* 'long long' type modifier */

////////////////////////////////////////////////////////////////////////////////
/// typy

#if SCANF_WWIDTH
typedef unsigned int width_t;
#else
typedef uint8_t width_t;
#endif


typedef struct
{
    const char *src;
    const char *srcend;
    const char *ptr;
} stream_t;


////////////////////////////////////////////////////////////////////////////////
/// implementacja

/*
__attribute__((always_inline))
*/
static int stream_getc(stream_t *p_stream)
{
    if (p_stream->ptr >= p_stream->srcend)
        return -1;

    int c = *p_stream->ptr;
    p_stream->ptr++;
    return c;
}

// -----------------------------------------------------------------------------

/*
__attribute__((always_inline))
*/
static int stream_ungetc(int c, stream_t *p_stream)
{
    if (p_stream->ptr > p_stream->src)
    {
        p_stream->ptr--;
        return c;
    }

    return -1;
}

// -----------------------------------------------------------------------------

/* Add noinline attribute to avoid GCC 4.2 optimization.    */

__attribute__((noinline))
static void putval(void *addr, long val, uint16_t flags)
{
    if (!(flags & FL_STAR))
    {
        if (flags & FL_CHAR)
            *(char *) addr = (char)val;
        else if (flags & FL_SHORT)
            *(short *) addr = (short)val;
        else if (flags & FL_LONG)
            *(long *) addr = (long)val;
        else
            *(int *) addr = (int)val;
    }
}

// -----------------------------------------------------------------------------

__attribute__((noinline))
static uint32_t mulacc(uint32_t val, uint16_t flags, uint8_t c)
{
    uint8_t cnt;

    if (flags & FL_OCT)
    {
        cnt = 3;
    }
    else if (flags & FL_HEX)
    {
        cnt = 4;
    }
    else
    {
        val += (val << 2);
        cnt = 1;
    }

    do
    {
        val <<= 1;
    } while (--cnt);

    return val + c;
}

// -----------------------------------------------------------------------------

__attribute__((noinline))
static uint8_t conv_int(stream_t *p_stream, width_t width, void *addr, uint16_t flags)
{
    int i;
    uint32_t val = 0;

    i = stream_getc(p_stream);          /* after ungetc()   */

    switch ((uint8_t) i)
    {
        case '-':
            flags |= FL_MINUS;
            /* FALLTHROUGH */
        case '+':
            if (!--width || (i = stream_getc(p_stream)) < 0)
                goto err;
    }

    flags &= ~FL_WIDTH;

    if (!(flags & (FL_DEC | FL_OCT)) && (uint8_t) i == '0')
    {
        if (!--width || (i = stream_getc(p_stream)) < 0)
            goto putval;

        flags |= FL_WIDTH;

        if ((uint8_t) (i) == 'x' || (uint8_t) (i) == 'X')
        {
            flags |= FL_HEX;

            if (!--width || (i = stream_getc(p_stream)) < 0)
                goto putval;
        }
        else
        {
            if (!(flags & FL_HEX))
                flags |= FL_OCT;
        }
    }

    /* This fact is used below to parse hexidecimal digit.  */
    #if ('A' - '0') != (('a' - '0') & ~('A' ^ 'a'))
    #error
    #endif


    do
    {
        uint8_t c = i;
        c -= '0';

        if (c > 7)
        {
            if (flags & FL_OCT)
                goto unget;

            if (c > 9)
            {
                if (!(flags & FL_HEX))
                    goto unget;

                c &= ~('A' ^ 'a');
                c += '0' - 'A';

                if (c > 5)
                {
unget:
                    stream_ungetc(i, p_stream);
                    break;
                }
                c += 10;
            }
        }

        val = mulacc(val, flags, c);
        flags |= FL_WIDTH;

        if (!--width)
            goto putval;
    }
    while ((i = stream_getc(p_stream)) >= 0);

    if (!(flags & FL_WIDTH))
        goto err;

putval:
    if (flags & FL_MINUS)
        val = -val;

    putval(addr, val, flags);
    return 1;

err:
    return 0;
}

// -----------------------------------------------------------------------------

#if  SCANF_BRACKET

__attribute__((noinline))
static const char * conv_brk(stream_t *p_stream, width_t width, char *addr, const char *fmt)
{
    uint8_t msk[32];
    uint8_t fnegate;
    uint8_t frange;
    uint8_t cabove;
    int i;

    memset(msk, 0, sizeof(msk));
    fnegate = 0;
    frange = 0;
    cabove = 0;         /* init to avoid compiler warning   */

    for (i = 0; ; i++)
    {
        uint8_t c = *fmt++;

        if (c == 0)
        {
            return 0;
        }
        else if (c == '^' && !i)
        {
            fnegate = 1;
            continue;
        }
        else if (i > fnegate)
        {
            if (c == ']')
                break;

            if (c == '-' && !frange)
            {
                frange = 1;
                continue;
            }
        }

        if (!frange)
            cabove = c;

        for (; ; )
        {
            msk[c >> 3] |= 1 << (c & 7);

            if (c == cabove)
                break;

            if (c < cabove)
                c++;
            else
                c--;
        }

        frange = 0;
    }

    if (frange)
        msk['-' / 8] |= 1 << ('-' & 7);

    if (fnegate)
    {
        uint8_t *p = msk;

        do
        {
            uint8_t c = *p;
            *p++ = ~c;
        } while (p != msk + sizeof (msk));
    }

    /* And now it is a flag of fault.   */
    fnegate = 1;

    /* NUL ('\0') is consided as normal character. This is match to Glibc.
       Note, there is no method to include NUL into symbol list.    */
    do
    {
        i = stream_getc(p_stream);

        if (i < 0)
            break;

        if (!((msk[(uint8_t) i >> 3] >> (i & 7)) & 1))
        {
            stream_ungetc(i, p_stream);
            break;
        }

        if (addr)
            *addr++ = i;

        fnegate = 0;
    } while (--width);

    if (fnegate)
    {
        return 0;
    }
    else
    {
        if (addr)
            *addr = 0;

        return fmt;
    }
}

#endif  /* SCANF_BRACKET */

// -----------------------------------------------------------------------------

#if  SCANF_FLOAT

static const float pwr_p10 [6] = {
    1e+1, 1e+2, 1e+4, 1e+8, 1e+16, 1e+32
};
static const float pwr_m10 [6] = {
    1e-1, 1e-2, 1e-4, 1e-8, 1e-16, 1e-32
};

static const char pstr_nfinity[] = "nfinity";
static const char pstr_an[] = "an";

__attribute__((noinline))
static uint8_t conv_flt(stream_t *p_stream, width_t width, float *addr)
{
    union
    {
        uint32_t u32;
        float flt;
    } x;

    int i;
    const char *p;
    int expon;

    uint8_t flag;

    #define FL_MINUS    0x80    /* number is negative   */
    #define FL_ANY      0x02    /* any digit was readed */
    #define FL_OVFL     0x04    /* overflow was     */
    #define FL_DOT      0x08    /* decimal '.' was  */
    #define FL_MEXP     0x10    /* exponent 'e' is neg. */

    i = stream_getc(p_stream);  /* after ungetc()   */
    flag = 0;

    switch (i)
    {
        case '-':
            flag = FL_MINUS;
            /* FALLTHROUGH */

        case '+':
            if (!--width || (i = stream_getc(p_stream)) < 0)
                goto err;
    }

    switch (tolower(i))
    {
        case 'n':
            p = pstr_an;
            goto operate_pstr;

        case 'i':
            p = pstr_nfinity;

operate_pstr:
            {
                uint8_t c;

                while ((c = *p++) != 0)
                {
                    if (!--width
                        || (i = stream_getc(p_stream)) < 0
                        || ((uint8_t) tolower(i) != c
                            && (stream_ungetc(i, p_stream), 1))
                        )
                    {
                        if (p == pstr_nfinity + 3)
                            break;

                        goto err;
                    }
                }
            }

            x.flt = (p == pstr_an + 3) ? NAN : INFINITY;
            break;

        default:
            expon = 0;
            x.u32 = 0;

            do
            {
                uint8_t c = i - '0';

                if (c <= 9)
                {
                    flag |= FL_ANY;

                    if (flag & FL_OVFL)
                    {
                        if (!(flag & FL_DOT))
                            expon += 1;
                    }
                    else
                    {
                        if (flag & FL_DOT)
                            expon -= 1;

                        x.u32 = mulacc(x.u32, FL_DEC, c);

                        if (x.u32 >= (ULONG_MAX - 9) / 10)
                            flag |= FL_OVFL;
                    }

                }
                else if (c == (('.' - '0') & 0xff) && !(flag & FL_DOT))
                {
                    flag |= FL_DOT;
                }
                else
                {
                    break;
                }
            } while (--width && (i = stream_getc(p_stream)) >= 0);

            if (!(flag & FL_ANY))
                goto err;

            if ((uint8_t) i == 'e' || (uint8_t) i == 'E')
            {
                int expacc;

                if (!--width || (i = stream_getc(p_stream)) < 0)
                    goto err;

                switch ((uint8_t) i)
                {
                    case '-':
                        flag |= FL_MEXP;
                        /* FALLTHROUGH */

                    case '+':
                        if (!--width)
                            goto err;

                        i = stream_getc(p_stream);      /* test EOF will below  */
                        break;
                }

                if (!isdigit(i))
                    goto err;

                expacc = 0;

                do
                {
                    expacc = mulacc(expacc, FL_DEC, i - '0');
                } while (--width && isdigit(i = stream_getc(p_stream)));

                if (flag & FL_MEXP)
                    expacc = -expacc;

                expon += expacc;
            }

            if (width && i >= 0)
                stream_ungetc(i, p_stream);

            float _f = (float)x.u32;
            x.flt = _f;

            const float *pfloat;

            if (expon < 0)
            {
                pfloat = &pwr_m10[5];
                expon = -expon;
            }
            else
            {
                pfloat = (pwr_p10 + 5);
            }

            for (width = 32; width; width >>= 1)
            {
                for (; (unsigned)expon >= width; expon -= width)
                    x.flt *= (*pfloat);

                pfloat--;
            }

            break;
    } /* switch */

    if (flag & FL_MINUS)
        x.flt = -x.flt;

    if (addr)
        *addr = x.flt;

    return 1;

err:
    return 0;
}

#endif  /* SCANF_FLOAT  */

// -----------------------------------------------------------------------------

__attribute__((noinline))
static int skip_spaces(stream_t *stream)
{
    int i;

    do
    {
        if ((i = stream_getc(stream)) < 0)
            return i;
    } while (isspace(i));

    stream_ungetc(i, stream);
    return i;
}

// -----------------------------------------------------------------------------

int vrsscanf (const char *input, const char *fmt, va_list ap)
{
    uint8_t nconvs;
    uint8_t c;
    width_t width;
    void *addr;
    uint16_t flags;
    int i;
    stream_t input_stream;
    stream_t *stream = &input_stream;

    input_stream.ptr = input;
    input_stream.src = input;
    input_stream.srcend = input + strlen(input);
    nconvs = 0;

    /* Initialization of stream_flags at each pass simplifies the register
       allocation with GCC 3.3 - 4.2.  Only the GCC 4.3 is good to move it
       to the begin.    */

    while ((c = *fmt++) != 0)
    {
        if (isspace(c))
        {
            skip_spaces(stream);
        }
        else if (c != '%' || (c = *fmt++) == '%')
        {
            /* Ordinary character.  */
            if ((i = stream_getc(stream)) < 0)
                goto eof;

            if ((uint8_t) i != c)
            {
                stream_ungetc(i, stream);
                break;
            }
        }
        else
        {
            flags = 0;

            if (c == '*')
            {
                flags = FL_STAR;
                c = *fmt++;
            }

            width = 0;

            while ((c -= '0') < 10)
            {
                flags |= FL_WIDTH;
                width = mulacc(width, FL_DEC, c);
                c = *fmt++;
            }

            c += '0';

            if (flags & FL_WIDTH)
            {
                /* C99 says that width must be greater than zero.
                   To simplify program do treat 0 as error in format.   */
                if (!width)
                    break;
            }
            else
            {
                width = ~0;
            }

            /* ATTENTION: with FL_CHAR the FL_LONG is set also. */

            switch (c)
            {
                case 'h':
                    c = *fmt++;

                    if (c == 'h')
                    {
                        flags |= FL_CHAR;
                        c = *fmt++;
                    }
                    else
                    {
                        flags |= FL_SHORT;
                    }
                    break;
                    /* FALLTHROUGH */

                case 'l':
/*
                    if ((c = *fmt++) == 'l')
 *                  {
                        flags |= FL_LONGONG;
 *                      c = *fmt++;
 *                  }
                    else
*/
                    flags |= FL_LONG;
                    c = *fmt++;
                    break;
            }

            #define CNV_BASE    "cdinopsuxX"

            #if SCANF_BRACKET
            #define CNV_BRACKET "["
            #else
            #define CNV_BRACKET ""
            #endif

            #if SCANF_FLOAT
            #define CNV_FLOAT   "efgEFG"
            #else
            #define CNV_FLOAT   ""
            #endif

            #define CNV_LIST    CNV_BASE CNV_BRACKET CNV_FLOAT

            if (!c || !strchr((CNV_LIST), c))
                break;

            addr = (flags & FL_STAR) ? 0 : va_arg(ap, void *);

            if (c == 'n')
            {
                putval(addr, stream->ptr - stream->src, flags);
                continue;
            }

            if (c == 'c')
            {
                if (!(flags & FL_WIDTH))
                    width = 1;

                do
                {
                    if ((i = stream_getc(stream)) < 0)
                        goto eof;

                    if (addr)
                    {
                        *(char*)addr = i;
                        addr = (void*)(((int)addr) + 1);
                    }
                } while (--width);

                c = 1;          /* no matter with smart GCC */

#if  SCANF_BRACKET
            }
            else if (c == '[')
            {
                fmt = conv_brk(stream, width, (char*)addr, fmt);
                c = (fmt != 0);
#endif
            }
            else
            {
                if (skip_spaces(stream) < 0)
                    goto eof;

                switch (c)
                {
                    case 's':
                        /* Now we have 1 nospace symbol. */
                        do
                        {
                            if ((i = stream_getc(stream)) < 0)
                                break;

                            if (isspace(i))
                            {
                                stream_ungetc(i, stream);
                                break;
                            }

                            if (addr)
                            {
//                              *(char *)addr++ = i; // pointer arith. warning
                                *(char*)addr = i;
                                addr = (void*)(((int)addr) + 1);
                            }
                        } while (--width);

                        if (addr)
                            *(char *)addr = 0;

                        c = 1;      /* no matter with smart GCC */
                        break;

#if  SCANF_FLOAT
                    case 'p':
                    case 'x':
                    case 'X':
                        flags |= FL_HEX;
                        goto conv_int;

                    case 'd':
                    case 'u':
                        flags |= FL_DEC;
                        goto conv_int;

                    case 'o':
                        flags |= FL_OCT;
                        /* FALLTHROUGH */
                    case 'i':
conv_int :
                        c = conv_int(stream, width, addr, flags);
                        break;

                    default:        /* e,E,f,F,g,G  */
                        c = conv_flt(stream, width, addr);
#else
                    case 'd':
                    case 'u':
                        flags |= FL_DEC;
                        goto conv_int;

                    case 'o':
                        flags |= FL_OCT;
                        /* FALLTHROUGH */
                    case 'i':
                        goto conv_int;

                    default:            /* p,x,X    */
                        flags |= FL_HEX;
conv_int:
                        c = conv_int(stream, width, addr, flags);
#endif
                }
            } /* else */

            if (!c)
            {
                if (stream->ptr >= stream->srcend)
                    goto eof;

                break;
            }

            if (!(flags & FL_STAR))
                nconvs += 1;
        } /* else */
    } /* while */
    return nconvs;

eof:
    return nconvs ? nconvs : -1;
}

// -----------------------------------------------------------------------------

int rsscanf(const char *input, const char *fmt, ...)
{
    va_list ap;
    va_start (ap, fmt);
    int nscans = vrsscanf(input, fmt, ap);
    va_end(ap);

    return nscans;
}

// -----------------------------------------------------------------------------
