/*******************************************************************************
 * \file    gol_hmi_io.h
 *
 * \brief   Definicje typów do wymiany danych HMI między urządzeniami
 *
 * autor:   Mariusz Midor
 *          http://egolhmi.eu
 *
 ******************************************************************************/

#ifndef _GOL_HMI_IO_H_
#define _GOL_HMI_IO_H_

/////////////////////////////////////////////////////////////////////////////////
// załączniki

#include "types.h"

////////////////////////////////////////////////////////////////////////////////
/// definicje preprocesora
/** nazwy rejstrów, string indeksowany przez T_RegType */
#define HMI_REGNAMES            "BHRMTUFS\0\0\0"
/** maksymalna długość tekstu w jednej paczce IMC */
#define HMI_REGVAL_STR_MAXLEN   600
/** flagi przy przesyłaniu rej. tekstowego
 * FIRST - to jest pierwszy pakiet, po użyciu maski
 *          HMI_REGVAL_STR_MASK uzyskamy długość tekstu
 * LAST - ostatnia część przesyłanego tekstu
 */
#define HMI_REGVAL_STR_FIRST    BIT(31)
#define HMI_REGVAL_STR_LAST     BIT(30)
#define HMI_REGVAL_STR_MASK     0x00FFFFFF

////////////////////////////////////////////////////////////////////////////////
// typy

typedef enum
{
    HMIR_B,
    HMIR_H,
    HMIR_R,
    HMIR_M,
    HMIR_T,
    HMIR_U,
    HMIR_F,
    HMIR_S,
    //
    HMIR_INVALID
} ATTR_PACKED T_RegType;


/** identyfikator rejestru (inna postać nazwy tekstowej) */
typedef struct
{
    /** T_RegType */
    uint16_t    RegType : 4;
    /** 0..2047 */
    uint16_t    RegNo   : 12;
} ATTR_PACKED T_RegID;


/** 6 bajtowa wartość rejestru */
typedef struct
{
    /** T_RegType */
    uint16_t    RegType : 4;
    /** 0..2047 */
    uint16_t    RegNo   : 12;
    /** wartość liczbowego,
     * długość (w bajtach) tekstowego UTF8 przy zapisywaniu,
     * offset (w bajtach) tekstowego UTF8 przy odczycie */
    int32_t Value;
    /** treść rejestru tekstowego, max HMI_REGVAL_STRLEN */
    char    String[];
} ATTR_PACKED T_RegVal;


/** polecenia rejestrów */
typedef enum
{
    /** */
    RCMD_NONE,
    /** wyślij wskazane rejestry */
    RCMD_READ_REGS,
    /** zapisz wskazane rejestry*/
    RCMD_WRITE_REGS,
    /** wyślij listę zmienionych rejestrów obserwowanych razem z wartościami */
    RCMD_GET_CHANGED_VALUES,
    /** wyślij nazwy zmienionych rejestrów obserwowanych */
    RCMD_GET_CHANGED_NAMES,
    /** lista zmienionych rejestrów obserwowanych wraz z wartościami */
    RCMD_CHANGED_NAMES_DATA,
    /** wyczyść listę zmienionych rejestrów obserwowanych */
    RCMD_CLEAR_CHANGED_LIST,
    /** */
    RCMD_TERMINATOR
} ATTR_PACKED T_RegCmd;


/** wymiana informacji z urz. zewnętrznym */
typedef struct
{
    /** polecenie */
    T_RegCmd    Command;
    /** ilość przesłanych rejestrów */
    uint8_t     Count;
    /** tablica rejestrów liczbowych */
    union
    {
        /** RCMD_WRITE_REGS */
        T_RegVal    Regs[0];
        /** RCMD_READ_REGS */
        T_RegID     IDs[0];
    };
} ATTR_PACKED T_RegData;

/////////////////////////////////////////////////////////////////////////////////
// zmienne globalne

/////////////////////////////////////////////////////////////////////////////////
// funkcje globalne

//------------------------------------------------------------------------------

#endif // _GOL_HMI_IO_H_

