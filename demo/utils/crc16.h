/*******************************************************************************
 * \file    crc16.h
 *
 * \brief   Fast CRC16
 *
 * \author  Mariusz Midor
 *          https://bitbucket.org/mmidor/imc
 *
 ******************************************************************************/

#ifndef _CRC16_H_
#define _CRC16_H_

////////////////////////////////////////////////////////////////////////////////
// includes

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////////////////
/// preprocessor definitions

/**
 *  #define CRC16_CONFIG_FAST
 *  use precalculated values from table, which significantly increase speed
 *  of the calculation. on some targets (like AVR, PIC) may occupy 512 bytes of RAM,
 *  in that case specifing this constant is not recommended
*/
#define CRC16_CONFIG_FAST   1

////////////////////////////////////////////////////////////////////////////////
/// exported functions

void CRC16_Compute(uint16_t *pCRC16, const void *pData, uint32_t DataSize);

////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

//------------------------------------------------------------------------------

#endif // _CRC16_H_
