/*******************************************************************************
 * \file    types.h
 *
 * \brief   Types and macros used in program
 *
 *  \author Mariusz Midor
 *          http://egolhmi.eu
 *          https://bitbucket.org/mmidor/imc
 *
 * \date    2016-05-09
 *
 ******************************************************************************/

#ifndef _TYPES_H_
#define _TYPES_H_

////////////////////////////////////////////////////////////////////////////////
/// includes

#include <stdint.h>
#include <stddef.h>
#include <stdarg.h>
#ifndef __cplusplus
# include <stdbool.h>
#endif

////////////////////////////////////////////////////////////////////////////////
/// preprocessor definitions

#ifndef NULL
# define NULL 0
#endif

#if defined __BORLANDC__
    // win32
#elif defined __MINGW32__
    // win32
#elif defined __GNUC__
    //
#else
    #error Unknown compiler
#endif

////////////////////////////////////////////////////////////////////////////////

#ifndef MIN
# define MIN(x, y)              (((x) < (y)) ? (x) : (y))
#endif

#ifndef MAX
# define MAX(x, y)              (((x) > (y)) ? (x) : (y))
#endif

#ifndef ABS
# define ABS(a)                 ((a) >= 0 ? (a) : (-(a)))
#endif

#define BIT(n)                  (1U << (n))

#define INRANGE(val, min, max)  (((val) >= (min)) && ((val) <= (max)))

#define IS_POWEROF_2(n)         ((n-1) == ((n-1) & (~n)))

#if (defined __cplusplus) && (__cplusplus >= 201103)
# define STATIC_ASSERT(expr, errmsg)    static_assert(expr, errmsg)
#else
# define STATIC_ASSERT(expr, errmsg)    typedef char __statassert[(expr) ? 1 : -1]
#endif

#ifdef __GNUC__
# define ATTR_WARNUNUSEDRESULT  __attribute__ ((warn_unused_result))
# define ATTR_PACKED            __attribute__ ((packed))
# define ATTR_NOINLINE          __attribute__ ((noinline))
#else
# define ATTR_WARNUNUSEDRESULT
# define ATTR_PACKED
# define ATTR_NOINLINE
#endif

#if defined WIN32 || defined __BORLANDC__ || defined __MINGW32__
# ifndef __WIN32__
#  define __WIN32__
# endif
#endif

/** create "x" from x */
#define __STRINGIFY(x)          #x
#define STRINGIFY(x)            __STRINGIFY(x)

////////////////////////////////////////////////////////////////////////////////
// types

/** helpful unions */
typedef union
{
    uint32_t u32;
     int32_t i32;
    uint16_t u16[2];
     int16_t i16[2];
    uint8_t  u8[4];
     int8_t  i8[4];
    float    f32;
} T_Int32;

typedef union
{
    uint64_t u64;
     int64_t i64;
    uint32_t u32[2];
     int32_t i32[2];
    uint16_t u16[4];
     int16_t i16[4];
    uint8_t  u8[8];
     int8_t  i8[8];
    double   f64;
    float    f32[2];
} T_Int64;

//------------------------------------------------------------------------------

#endif // _TYPES_H_
