/*******************************************************************************
 * \file    hmi_regs.c
 *
 * \author  Mariusz Midor
 *          http://egolhmi.eu
 *
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
/// załączniki

#include "hmi_regs.h"
// #include "trace.h"

#include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////
/// definicje preprocesora, makra

////////////////////////////////////////////////////////////////////////////////
/// prototypy

////////////////////////////////////////////////////////////////////////////////
/// stałe, zmienne

////////////////////////////////////////////////////////////////////////////////
/// implementacja

bool REG_IsNameValid(const T_RegName RegName, uint16_t *pRegNo)
{
    if (!RegName)
        return false;

    if (RegName[0] != '%')
        return false;

    char t[2] = { RegName[1], '\0' };
    if (!strpbrk(t, "BHRMTUFS"))
        return false;

    uint16_t regno = atoi(&RegName[2]);

    switch (RegName[1])
    {
        case 'B':
            if (regno >= HMI_REGS_B)
                return false;
            break;

        case 'H':
            if (regno >= HMI_REGS_H)
                return false;
            break;

        case 'R':
            if (regno >= HMI_REGS_R)
                return false;
            break;

        case 'M':
            if (regno >= HMI_REGS_M)
                return false;
            break;

        case 'T':
            if (regno >= HMI_REGS_T)
                return false;
            break;

        case 'U':
            if (regno >= HMI_REGS_U)
                return false;
            break;

        case 'F':
            if (regno >= HMI_REGS_F)
                return false;
            break;

        case 'S':
            if (regno >= HMI_REGS_S)
                return false;
            break;

        default:
            return false;
    }

    if (pRegNo)
        *pRegNo = regno;

    return true;
}

//------------------------------------------------------------------------------

T_RegType REG_GetTypeFromName(const T_RegName RegName)
{
    if (!HMI_REGNAME_ISVALID(RegName))
    {
        // TRACE_ERROR_LOCATION("- register name '%s' is invalid", RegName);
        return HMIR_INVALID;
    }

    const char *regnames = HMI_REGNAMES;
    const char *p = regnames;
    char rn = RegName[1];

    while (*p)
    {
        if (*p == rn)
        {
            T_RegType rt = (T_RegType)(p-regnames);
            return rt;
        }
        p++;
    }

    return HMIR_INVALID;
}

//------------------------------------------------------------------------------
