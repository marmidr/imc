# list of folder names to keep in report
filters="cpputest src tests"

cd build/
# clean output file
echo "" > alltests_filtered.info

out=1
for f in $filters; do
	echo Filtering $f
	# http://www.awk.of.pl/
	re=/SF:.*$f/,/end_of_record/
	awk $re alltests.info > $out.info
	# concatenate filtered with summary output
	cat $out.info >> alltests_filtered.info
	echo "" >> alltests_filtered.info
	out=$((out+1))
done

